﻿using Microsoft.AspNetCore.SignalR;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Template.Services.DebugLogger
{
	public class DebugLoggerHub : Hub<IDebugLoggerClient>
	{
		public string GetConnectionId()
		{
			return Context.ConnectionId;
		}
	}
}
