﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.Configuration;

namespace WebApp.Spa.Template.Services.FolderPath
{
	public class FolderPathService : IFolderPathService
	{
		private readonly Config config;
		private readonly IHostingEnvironment hostingEnvironment;
		private readonly IHttpContextAccessor httpContextAccessor;

		public FolderPathService(IOptions<Config> configOptions, IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
		{
			this.config = configOptions.Value;
			this.hostingEnvironment = hostingEnvironment;
			this.httpContextAccessor = httpContextAccessor;
		}

		public string WebAppPath
		{
			get
			{
				var directory = new DirectoryInfo(AppContext.BaseDirectory);

				while (directory.Name != this.config.WebAppName)
				{
					directory = directory.Parent;

					if (directory == null)
						throw new Exception($"Unable to compute root path for web application {this.config.WebAppName}.");
				}

				return directory.FullName;
			}
		}

		public string WebRootPath => this.hostingEnvironment.WebRootPath;

		public string AppSettingsPath => Path.Combine(AppContext.BaseDirectory, "appsettings.json");

		public string WebConfigPath => Path.Combine(AppContext.BaseDirectory, "web.config");

		public string UpdatesFolderPath => Path.Combine(WebAppPath, "App_Data", "Updates");

		public string SessionsFolderPath => Path.Combine(Path.GetTempPath(), "Twenty57 Stadium 6", "sessions", $"{this.config.WebAppId}_sessions");

		public string SessionIdFolderPath => Path.Combine(SessionsFolderPath, this.httpContextAccessor.HttpContext.Session.Id);
	}
}
