﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Spa.Administration;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Template.Services.Administration
{
	public class RoleAdministration : AAdministration, IRoleAdministration
	{
		private readonly RoleManager<Role> roleManager;
		private readonly AdministrationContext administrationContext;

		public RoleAdministration(RoleManager<Role> roleManager, AdministrationContext administrationContext)
		{
			this.roleManager = roleManager;
			this.administrationContext = administrationContext;
		}

		public async Task<IEnumerable<RoleInfo>> GetRolesAsync()
		{
			var roles = await this.roleManager.Roles
									.OrderBy(r => r.Name)
									.ToArrayAsync();

			IDictionary<string, string> pageNamesById = await this.administrationContext.Pages
																	.Include(p => p.PageRoles)
																	.ToDictionaryAsync(p => p.Id, p => p.Name);

			return roles.Select(r => new RoleInfo
			{
				Role = r,
				Pages = r.PageRoles.Select(pr => pageNamesById[pr.PageId])
			});
		}

		public async Task AddRoleAsync(string name, IEnumerable<string> pages)
		{
			var createRoleIdentityResult = await this.roleManager.CreateAsync(new Role(name));
			AssertSuccess(createRoleIdentityResult);

			var newRole = await this.roleManager.FindByNameAsync(name);

			IDictionary<string, string> pageIdsByName = await this.administrationContext.Pages
																	.Include(p => p.PageRoles)
																	.ToDictionaryAsync(p => p.Name, p => p.Id);

			var pageIds = pages.Select(n => pageIdsByName[n]);

			foreach (string pageId in pageIds)
			{
				newRole.PageRoles.Add(new PageRole
				{
					PageId = pageId,
					RoleId = newRole.Id
				});
			}

			await this.administrationContext.SaveChangesAsync();
		}

		public async Task EditRoleAsync(string id, string name, IEnumerable<string> pages)
		{
			var role = await this.roleManager.FindByIdAsync(id);

			role.Name = name;

			var updateRoleIdentityResult = await this.roleManager.UpdateAsync(role);
			AssertSuccess(updateRoleIdentityResult);

			IDictionary<string, string> pageIdsByName = await this.administrationContext.Pages
																	.Include(p => p.PageRoles)
																	.ToDictionaryAsync(p => p.Name, p => p.Id);

			var newPageRoleIds = pages.Select(p => pageIdsByName[p]);
			var pageRolesToRemove = role.PageRoles.Where(pr => !newPageRoleIds.Contains(pr.PageId)).ToArray();
			foreach (var pageRole in pageRolesToRemove)
			{
				role.PageRoles.Remove(pageRole);
			}

			var existingPageIds = role.PageRoles.Select(pr => pr.PageId).ToHashSet();
			var pageRoleIdsToAdd = newPageRoleIds.Where(i => !existingPageIds.Contains(i));
			foreach (string pageId in pageRoleIdsToAdd)
			{
				role.PageRoles.Add(new PageRole
				{
					PageId = pageId,
					RoleId = role.Id
				});
			}

			this.administrationContext.Attach(role);
			this.administrationContext.Update(role);
			await this.administrationContext.SaveChangesAsync();
		}

		public async Task DeleteRoleAsync(string id)
		{
			var role = await this.roleManager.FindByIdAsync(id);

			var deleteRoleIdentityResult = await this.roleManager.DeleteAsync(role);
			AssertSuccess(deleteRoleIdentityResult);
		}
	}
}
