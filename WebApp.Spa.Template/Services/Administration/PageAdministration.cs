﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Spa.Administration;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Template.Services.Administration
{
	public class PageAdministration : AAdministration, IPageAdministration
	{
		private readonly RoleManager<Role> roleManager;
		private readonly AdministrationContext administrationContext;

		public PageAdministration(RoleManager<Role> roleManager, AdministrationContext administrationContext)
		{
			this.roleManager = roleManager;
			this.administrationContext = administrationContext;
		}

		public async Task<IEnumerable<PageInfo>> GetPagesAsync()
		{
			var pages = await this.administrationContext.Pages
										.Include(p => p.PageRoles)
										.Include(p => p.AccessLookupEntries)
										.ToArrayAsync();

			var roleNamesById = await this.roleManager.Roles.ToDictionaryAsync(r => r.Id, r => r.Name);

			return pages.Select(p => new PageInfo
			{
				Page = p,
				Roles = p.PageRoles.Select(pr => roleNamesById[pr.RoleId])
			});
		}

		public async Task EditPageAsync(string id, IEnumerable<string> roles)
		{
			var page = await this.administrationContext.Pages
									.Include(p => p.PageRoles)
									.SingleOrDefaultAsync(p => p.Id == id);

			var roleIdsByName = await this.roleManager.Roles.ToDictionaryAsync(r => r.Name, r => r.Id);

			var newPageRoleIds = roles.Select(r => roleIdsByName[r]).ToHashSet();
			var pageRolesToRemove = page.PageRoles.Where(pr => !newPageRoleIds.Contains(pr.RoleId)).ToArray();
			foreach (var pageRole in pageRolesToRemove)
			{
				page.PageRoles.Remove(pageRole);
			}

			var existingRoleIds = page.PageRoles.Select(pr => pr.RoleId).ToHashSet();
			var pageRoleIdsToAdd = newPageRoleIds.Where(i => !existingRoleIds.Contains(i));
			foreach (string roleId in pageRoleIdsToAdd)
			{
				page.PageRoles.Add(new PageRole
				{
					PageId = page.Id,
					RoleId = roleId
				});
			}

			this.administrationContext.Attach(page);
			this.administrationContext.Update(page);
			await this.administrationContext.SaveChangesAsync();
		}
	}
}
