﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace WebApp.Spa.Template.Services.HttpClient
{
	public static class HttpClientServiceCollectionExtensions
	{
		public static IServiceCollection AddApplicationManagerClient(this IServiceCollection services, string applicationManagerUrl)
		{
			services.AddHttpClient(HttpClients.ApplicationManager, c => c.BaseAddress = new Uri(applicationManagerUrl));

			return services;
		}
	}
}
