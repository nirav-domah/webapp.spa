﻿namespace WebApp.Spa.Template.Services.Authentication
{
	public class AntiforgeryConstants
	{
		public const string AntiforgeryTokenFieldName = "CSRF-TOKEN";

		public const string AntiforgeryTokenHeaderName = "X-CSRF-TOKEN";
	}
}
