﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using WebApp.Spa.Core.Enums;

namespace WebApp.Spa.Template.Services.Authentication.Anonymous
{
	public static class AnonymousAuthenticationServiceCollectionExtensions
	{
		public static IServiceCollection AddAnonymousAuthentication(this IServiceCollection services)
		{
			services.Configure<AuthenticationOptions>(o =>
			{
				string anonymousAuthenticationScheme = AuthenticationSchemes.GetDefaultAuthenticationScheme(AuthenticationType.Anonymous);
				o.AddScheme<AnonymousAuthenticationHandler>(anonymousAuthenticationScheme, anonymousAuthenticationScheme);
			});

			return services;
		}
	}
}
