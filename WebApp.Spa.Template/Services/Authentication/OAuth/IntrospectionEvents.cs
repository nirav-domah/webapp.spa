﻿using IdentityModel.AspNetCore.OAuth2Introspection;
using IdentityModel.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.OAuth.ProviderSettings;

namespace WebApp.Spa.Template.Services.Authentication.OAuth
{
	public class IntrospectionEvents : OAuth2IntrospectionEvents
	{
		private readonly IUserAdministration userAdministration;
		private readonly IHttpContextAccessor httpContextAccessor;
		private readonly IHttpClientFactory httpClientFactory;
		private readonly IMemoryCache memoryCache;
		private readonly GenericProviderSettings oidcSettings;

		public IntrospectionEvents(IUserAdministration userAdministration,
			IHttpContextAccessor httpContextAccessor,
			IHttpClientFactory httpClientFactory,
			IMemoryCache memoryCache,
			OidcSettingsProvider oidcSettingsProvider)
		{
			this.userAdministration = userAdministration;
			this.httpContextAccessor = httpContextAccessor;
			this.httpClientFactory = httpClientFactory;
			this.memoryCache = memoryCache;
			this.oidcSettings = oidcSettingsProvider.Settings;
		}

		public override Task CreatingTicket(ClaimsPrincipal principal)
		{
			var userInfo = GetUserInfo(principal);
			if (userInfo.UniqueId == null || userInfo.Email == null)
			{
				var httpRequest = this.httpContextAccessor.HttpContext.Request;
				string referenceToken = GetReferenceToken(httpRequest);

				userInfo = GetUserInfoAsync(referenceToken).Result;
			}

			var claims = ClaimsHelper.BuildClaims(userInfo, this.userAdministration);
			principal.AddIdentity(new ClaimsIdentity(claims, authenticationType: "OAuth"));

			return base.CreatingTicket(principal);
		}

		private UserInfo GetUserInfo(ClaimsPrincipal claims)
		{
			return new UserInfo
			{
				UniqueId = claims.FindFirstValue("sub"),
				Email = claims.FindFirstValue("email"),
				Name = claims.FindFirstValue("name")
			};
		}

		private async Task<UserInfo> GetUserInfoAsync(string token)
		{
			var userInfo = await this.memoryCache.GetOrCreateAsync(token, async cacheEntry =>
			{
				cacheEntry.SlidingExpiration = TimeSpan.FromMinutes(5);
				var httpClient = this.httpClientFactory.CreateClient();

				string userInfoEndpoint = await GetUserInfoEndpointAsync();
				return await GetUserInfoAsync(httpClient, token, userInfoEndpoint);
			});

			var userInfoEntity = new UserInfo
			{
				UniqueId = userInfo.Value<string>("sub"),
				Email = userInfo.Value<string>("email"),
				Name = userInfo.Value<string>("name")
			};

			if (this.oidcSettings.RoleClaimName != null && userInfo.TryGetValue(this.oidcSettings.RoleClaimName, out var roles))
				userInfoEntity.Roles = roles.ToObject<List<string>>();

			return userInfoEntity;
		}

		private static async Task<JObject> GetUserInfoAsync(System.Net.Http.HttpClient httpClient, string token, string userInfoEndpoint)
		{
			var httpRequest = new HttpRequestMessage(HttpMethod.Get, userInfoEndpoint);
			httpRequest.Headers.Add("Authorization", $"Bearer {token}");

			var response = await httpClient.SendAsync(httpRequest);
			string content = await response.Content.ReadAsStringAsync();
			return (JObject)JsonConvert.DeserializeObject(content);
		}

		private string GetReferenceToken(HttpRequest request, string scheme = "Bearer")
		{
			string authorization = request.Headers["Authorization"].FirstOrDefault();

			if (string.IsNullOrEmpty(authorization))
				return null;

			if (authorization.StartsWith(scheme + " ", StringComparison.OrdinalIgnoreCase))
				return authorization.Substring(scheme.Length + 1).Trim();

			return null;
		}

		private async Task<string> GetUserInfoEndpointAsync()
		{
			var client = this.httpClientFactory.CreateClient("IdentityModel.AspNetCore.OAuth2Introspection.BackChannelHttpClientName");

			var discoveryResponse = await client.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
			{
				Address = this.oidcSettings.Authority.ToLower(),
				Policy = new DiscoveryPolicy()
			});

			return discoveryResponse.UserInfoEndpoint;
		}
	}
}
