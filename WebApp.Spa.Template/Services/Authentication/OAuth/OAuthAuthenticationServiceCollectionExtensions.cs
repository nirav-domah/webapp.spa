﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApp.Spa.Core.Enums;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.Configuration;
using WebApp.Spa.Core.Services.OAuth.ProviderSettings;

namespace WebApp.Spa.Template.Services.Authentication.OAuth
{
	public static class OAuthAuthenticationServiceCollectionExtensions
	{
		private const string TokenIntrospectionScheme = "introspection";

		public static IServiceCollection AddOAuthAuthentication(this IServiceCollection services)
		{
			services.AddMemoryCache();
			services.AddScoped(p => ActivatorUtilities.CreateInstance<OidcSettingsProvider>(p));

			services.AddOptions<JwtBearerOptions>(JwtBearerDefaults.AuthenticationScheme)
					.Configure<IOptionsMonitor<Config>>((o, optionsMonitor) =>
					{
						var oidcSettings = optionsMonitor.CurrentValue.OAuth;
						o.Authority = oidcSettings.Authority;
						o.Audience = oidcSettings.Audience;
						o.TokenValidationParameters.ValidateAudience = !string.IsNullOrEmpty(oidcSettings.Audience);
						o.ForwardDefaultSelector = Selector.ForwardReferenceToken(TokenIntrospectionScheme);

						optionsMonitor.OnChange(config =>
						{
							var newOidcSettings = config.OAuth;
							o.Authority = newOidcSettings.Authority;
							o.Audience = newOidcSettings.Audience;
							o.TokenValidationParameters.ValidateAudience = !string.IsNullOrEmpty(newOidcSettings.Audience);
						});

						o.Events = new JwtBearerEvents()
						{
							OnTokenValidated = async c =>
							{
								var bearerToken = (JwtSecurityToken)c.SecurityToken;
								var serviceProvider = c.HttpContext.RequestServices;

								var userAdministration = serviceProvider.GetRequiredService<IUserAdministration>();
								var userInfo = await GetUserInfoAsync(bearerToken, serviceProvider, o.ConfigurationManager);

								var claims = ClaimsHelper.BuildClaims(userInfo, userAdministration);

								c.Principal.AddIdentity(new ClaimsIdentity(claims, authenticationType: "OAuth"));
							}
						};
					});

			services.AddTransient<IntrospectionEvents>();
			services.AddOptions<OAuth2IntrospectionOptions>(TokenIntrospectionScheme)
					.Configure<IOptionsMonitor<Config>>((o, optionsMonitor) =>
					{
						o.EventsType = typeof(IntrospectionEvents);
						o.EnableCaching = true;

						var oidcSettings = optionsMonitor.CurrentValue.OAuth;
						o.Authority = oidcSettings.Authority.ToLower();
						o.ClientId = oidcSettings.ApiResourceName;
						o.ClientSecret = oidcSettings.ApiResourceSecret;

						optionsMonitor.OnChange(config =>
						{
							var newOidcSettings = config.OAuth;
							o.Authority = newOidcSettings.Authority.ToLower();
							o.ClientId = newOidcSettings.ApiResourceName;
							o.ClientSecret = newOidcSettings.ApiResourceSecret;
						});
					});

			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
					.AddJwtBearer()
					.AddOAuth2Introspection(TokenIntrospectionScheme);

			return services;
		}

		private static async Task<UserInfo> GetUserInfoAsync(JwtSecurityToken bearerToken, IServiceProvider serviceProvider, IConfigurationManager<OpenIdConnectConfiguration> configurationManager)
		{
			var oidcSettings = serviceProvider.GetRequiredService<OidcSettingsProvider>()
												.Settings;

			var userInfoEntity = new UserInfo();

			if (oidcSettings.OidcProvider == OidcProvider.Google || oidcSettings.OidcProvider == OidcProvider.AzureAD)
			{
				userInfoEntity.UniqueId = bearerToken.Subject;
				userInfoEntity.Email = (string)bearerToken.Payload["email"];
				userInfoEntity.Name = (string)bearerToken.Payload["name"];

				if (oidcSettings.RoleClaimName != null && bearerToken.Payload.TryGetValue(oidcSettings.RoleClaimName, out object roles))
					userInfoEntity.Roles = ((JArray)roles).ToObject<List<string>>();
			}
			else
			{
				var memoryCache = serviceProvider.GetService<IMemoryCache>();
				var userInfo = await memoryCache.GetOrCreateAsync(bearerToken.RawData, async cacheEntry =>
				{
					cacheEntry.SlidingExpiration = TimeSpan.FromMinutes(30);

					var oidcConfig = await configurationManager.GetConfigurationAsync(cancel: default);
					var httpClient = serviceProvider.GetService<IHttpClientFactory>().CreateClient();

					return await GetUserInfoAsync(httpClient, bearerToken, oidcConfig);
				});

				userInfoEntity.UniqueId = userInfo.Value<string>("sub");
				userInfoEntity.Email = userInfo.Value<string>("email");
				userInfoEntity.Name = userInfo.Value<string>("name");

				if (oidcSettings.RoleClaimName != null && userInfo.TryGetValue(oidcSettings.RoleClaimName, out var roles))
					userInfoEntity.Roles = roles.ToObject<List<string>>();
			}

			return userInfoEntity;
		}

		private static async Task<JObject> GetUserInfoAsync(System.Net.Http.HttpClient httpClient, JwtSecurityToken bearerToken, OpenIdConnectConfiguration oidcConfig)
		{
			var httpRequest = new HttpRequestMessage(HttpMethod.Get, oidcConfig.UserInfoEndpoint);
			httpRequest.Headers.Add("Authorization", $"Bearer {bearerToken.RawData}");

			var response = await httpClient.SendAsync(httpRequest);
			string content = await response.Content.ReadAsStringAsync();
			return (JObject)JsonConvert.DeserializeObject(content);
		}
	}
}
