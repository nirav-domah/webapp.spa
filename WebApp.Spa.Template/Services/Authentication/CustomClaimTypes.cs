﻿namespace WebApp.Spa.Template.Services.Authentication
{
	public static class CustomClaimTypes
	{
		public const string IsAdministrator = "IsAdministrator";
		public const string UniqueId = "UniqueId";
	}
}
