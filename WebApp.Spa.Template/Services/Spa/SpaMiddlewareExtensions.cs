﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.FileProviders;
using System.IO;
using VueCliMiddleware;
using WebApp.Spa.Core.Services.Configuration;

namespace WebApp.Spa.Template.Services.Spa
{
	public static class SpaMiddlewareExtensions
	{
		public static IApplicationBuilder UseSpa(this IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseWhen(
					httpContext => true,
					appBuilder => UseSpa("ClientApp/dist/debug", env, appBuilder)
				);
			}
			else
			{
				app.UseWhen(
					httpContext => IsDebug(),
					appBuilder => UseSpa("ClientApp/dist/debug", env, appBuilder)
				);

				app.UseWhen(
					httpContext => !IsDebug(),
					appBuilder => UseSpa("ClientApp/dist/production", env, appBuilder)
				);
			}

			return app;
		}

		private static void UseSpa(string spaRootPath, IHostingEnvironment env, IApplicationBuilder app)
		{
			StaticFileOptions staticFileOptions = null;
			if (!env.IsDevelopment())
			{
				staticFileOptions = new StaticFileOptions
				{
					FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, spaRootPath))
				};
				app.UseStaticFiles(staticFileOptions);
			}

			app.UseSpa(spa =>
			{
				spa.Options.SourcePath = "ClientApp";
				spa.Options.DefaultPage = "/index.html";

				if (env.IsDevelopment())
				{
					spa.UseVueCli(
						npmScript: "dev",
						port: 8081,
						regex: "dev server running at");

					//using (var scope = app.ApplicationServices.CreateScope())
					//{
					//	var config = scope.ServiceProvider
					//						.GetRequiredService<IOptionsSnapshot<Config>>()
					//						.Value;

					//	spa.UseProxyToSpaDevelopmentServer(config.SpaDevHostUrl);
					//}
				}
				else
				{

					spa.Options.DefaultPageStaticFileOptions = staticFileOptions;
				}
			});
		}

		private static bool IsDebug()
		{
			return AppSettingsHelper.GetInstance()
									.GetAppSettings()
									.Config
									.Debug;
		}
	}
}
