﻿namespace WebApp.Spa.Template.Services.Authorization
{
	public static class Policies
	{
		public const string AdministratorAccess = "AdministratorAccess";
		public const string RolesAccess = "RolesAccess";
	}
}
