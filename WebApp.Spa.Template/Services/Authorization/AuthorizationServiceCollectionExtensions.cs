﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using WebApp.Spa.Template.Services.Authorization.Requirements;

namespace WebApp.Spa.Template.Services.Authorization
{
	public static class AuthorizationServiceCollectionExtensions
	{
		public static IServiceCollection AddCustomAuthorization(this IServiceCollection services)
		{
			services.AddAuthorization(o =>
			{
				o.AddPolicy(
					Policies.AdministratorAccess,
					p => p.Requirements.Add(new AdministratorAccessRequirement()));

				o.AddPolicy(
					Policies.RolesAccess,
					p => p.Requirements.Add(new RolesAccessRequirement()));
			});

			services.AddTransient<IAuthorizationHandler, AdministratorAccessHandler>()
					.AddTransient<IAuthorizationHandler, RolesAccessHandler>();

			return services;
		}
	}
}
