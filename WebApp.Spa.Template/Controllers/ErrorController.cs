﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using WebApp.Spa.Core.Extensions;

namespace WebApp.Spa.Template.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class ErrorController : ControllerBase
	{
		public IActionResult Error([FromServices] IHostingEnvironment _webHostEnvironment)
		{
			var feature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
			var exception = feature?.Error;

			var problemDetails = new ProblemDetails
			{
				Status = (int)HttpStatusCode.InternalServerError,
				Instance = feature?.Path,
				Title = exception?.Message,
				Detail = UnrollExceptionMessage(exception.InnerException)
			};

			return StatusCode(problemDetails.Status.Value, problemDetails);
		}

		private string UnrollExceptionMessage(Exception innerException)
		{
			if (innerException == null)
				return null;

			return innerException.GetDisplayMessage(": ");
		}
	}
}
