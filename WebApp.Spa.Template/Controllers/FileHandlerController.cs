﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Template.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class FileHandlerController : ControllerBase
	{
		private readonly IFilesService filesService;

		public FileHandlerController(IFilesService filesService)
		{
			this.filesService = filesService;
		}

		// GET: api/FileHandler?content=content
		[HttpGet]
		public IActionResult GetFile(string content)
		{
			try
			{
				if (this.filesService.TryParseFileToken(content, out var fileToken))
				{
					var stream = this.filesService.GetInputStream(fileToken);
					return new FileStreamResult(stream, GetContentTypeForFile(fileToken.GetFullPath()));
				}
				else
				{
					byte[] contentData = Encoding.UTF8.GetBytes(content);
					return new FileContentResult(contentData, "application/octet-stream");
				}
			}
			catch (Exception exception)
			{
				throw new Exception($"Error reading file: {exception.Message}");
			}
		}

		// GET: api/FileHandler/Download?content=content&fileName=fileName
		[HttpGet]
		[Route("download")]
		public IActionResult DownloadFile(string content, string fileName)
		{
			try
			{
				string fileContentType = new FileExtensionContentTypeProvider().TryGetContentType(fileName, out string contentType)
											? contentType
											: "application/octet-stream";

				if (this.filesService.TryParseFileToken(content, out var fileToken))
				{
					var stream = this.filesService.GetInputStream(fileToken);
					return new FileStreamResult(stream, fileContentType);
				}

				byte[] contentData;
				if (!HasTextFileExtension(fileName) && content.StartsWith("["))
				{
					try
					{
						contentData = JsonConvert.DeserializeObject<byte[]>(content);
						return new FileContentResult(contentData, fileContentType);
					}
					catch (JsonReaderException)
					{ }
				}

				contentData = Encoding.UTF8.GetBytes(content);
				return new FileContentResult(contentData, fileContentType);
			}
			catch (Exception exception)
			{
				throw new Exception($"Error opening file: {exception.Message}");
			}
		}

		private bool HasTextFileExtension(string fileName)
		{
			string fileExtension = Path.GetExtension(fileName).ToLower();
			return (fileExtension == ".txt") || (fileExtension == ".json");
		}

		private string GetContentTypeForFile(string filePath = "")
		{
			if (!string.IsNullOrEmpty(filePath) && Path.GetExtension(filePath).ToLower() == ".svg")
				return "image/svg+xml";

			return "application/octet-stream";
		}
	}
}
