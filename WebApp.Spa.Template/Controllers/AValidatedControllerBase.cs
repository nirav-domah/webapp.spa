﻿using Microsoft.AspNetCore.Mvc;

namespace WebApp.Spa.Template.Controllers
{
	public abstract class AValidatedControllerBase : ControllerBase
	{
		protected IActionResult GetValidationResult(string message)
		{
			return ValidationProblem(new ValidationProblemDetails
			{
				Detail = message
			});
		}
	}
}
