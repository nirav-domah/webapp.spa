﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace WebApp.Spa.Template.Helpers
{
	public static class EnumHelper
	{
		public static bool TryParseDescription<T>(string value, out T result)
			where T : struct
		{
			if (string.IsNullOrEmpty(value))
			{
				result = default(T);
				return false;
			}

			var fieldInfos = typeof(T).GetFields();
			foreach (var fieldInfo in fieldInfos)
			{
				string description = fieldInfo.GetCustomAttribute<DescriptionAttribute>()?.Description;
				if (value == description)
					return Enum.TryParse(fieldInfo.Name, out result);
			}

			return Enum.TryParse(value, out result);
		}

		public static string GetValueDescription(object enumValue)
		{
			var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());
			string description = fieldInfo?.GetCustomAttribute<DescriptionAttribute>()?.Description;
			return description ?? enumValue.ToString();
		}
	}
}
