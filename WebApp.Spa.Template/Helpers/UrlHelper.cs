﻿using Microsoft.AspNetCore.WebUtilities;
using System.Collections.Generic;
using System.Linq;

namespace WebApp.Spa.Template.Helpers
{
	public static class UrlHelper
	{
		public static string Combine(params string[] parts)
		{
			return string.Join("/", parts
				.Select(part => part?.Trim().Trim('/'))
				.Where(part => !string.IsNullOrWhiteSpace(part)));
		}

		public static string AddQueryString(string uri, IDictionary<string, string> queryString)
		{
			return QueryHelpers.AddQueryString(uri, queryString);
		}

		public static string EnsureHttpProtocol(string url, bool secure = false)
		{
			return !url.StartsWith("http://") && !url.StartsWith("https://")
					? (secure ? "https://" + url : "http://" + url)
					: url;
		}
	}
}
