export class Person {
	FirstName = undefined;
	Pet = undefined;

	getFieldTypeName(fieldName) {
		let customTypes = {
			Pet: 'Dog'
		};

		return customTypes[fieldName];
	}
}

export class Dog {
	Colour = undefined;
}

export class Cat {
	Colour = undefined;
}

export class _Company_Employees_List extends Array {
	getItemTypeName() {
		return 'Person';
	}
}

export class Company {
	Employees = undefined;

	getFieldTypeName(fieldName) {
		let customTypes = {
			Employees: '_Company_Employees_List'
		};

		return customTypes[fieldName];
	}
}
