import { createRouter, createWebHistory } from 'vue-router';
import { trimStart } from 'lodash';
import { useAuthenticationStore } from '@/stores/authentication.js';
import routes from '@/router/routes.js';
import oidcHelper from '@/utils/oidc-helper.js';

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: routes
});

router.beforeEach(async (to, _from, next) => {
	const authenticationStore = useAuthenticationStore();
	let pageName = to.name ? to.name : trimStart(to.path, '/');

	let isAnonymousAuthentication = authenticationStore.isAnonymousAuthentication;
	if (isAnonymousAuthentication) {
		let isAdminPage = to.matched.some(record => record.meta.requiresAdministrator);
		let isLoginPage = to.name === 'Login';
		if (isLoginPage || isAdminPage) {
			next({ name: 'NotFound', params: [to.path] });
			return;
		}

		document.title = getPageTitle(pageName, to);
		next();
		return;
	}

	let isCookieAuthentication = authenticationStore.isCookieAuthentication;
	if (isCookieAuthentication) {
		let isAnonymousPage = to.matched.some(record => record.meta.allowAnonymous);
		if (!isAnonymousPage) {
			let isAuthenticated = authenticationStore.isAuthenticated;
			if (!isAuthenticated) {
				document.title = getPageTitle('Login');
				next({
					name: 'Login',
					params: { pageRoute: to.path }
				});
				return;
			}

			if (!isAccessAllowed(to, authenticationStore)) {
				document.title = getPageTitle('Forbidden');
				next({
					name: 'Forbidden',
					params: { pageName: pageName }
				});
				return;
			}
		}
	}

	let isOAuthAuthentication = authenticationStore.isOAuthAuthentication;
	if (isOAuthAuthentication) {
		let isAnonymousPage = to.matched.some(record => record.meta.allowAnonymous);
		if (!isAnonymousPage) {
			let isAuthenticated = authenticationStore.isAuthenticated;
			if (!isAuthenticated) {
				oidcHelper.redirectToLoginAsync();
				return;
			}

			if (!isAccessAllowed(to, authenticationStore)) {
				document.title = getPageTitle('Forbidden');
				next({
					name: 'Forbidden',
					params: { pageName: pageName }
				});
				return;
			}
		}
	}

	document.title = getPageTitle(pageName, to);
	next();
});

function getPageTitle(pageName, routeObject) {
	let title = routeObject?.matched.find(record => record.meta.title)?.meta.title;
	return `${title ?? pageName} - ${import.meta.env.VITE_APP_TITLE}`;
}

function isAccessAllowed(to, authenticationStore) {
	let isAdminPage = to.matched.some(record => record.meta.requiresAdministrator);
	if (isAdminPage) {
		let isUserAdministrator = authenticationStore.isAdministrator;
		return isUserAdministrator;
	}

	let pageName = trimStart(to.path, '/');
	let isAccessiblePage = authenticationStore.accessiblePagesSet.has(pageName);
	return isAccessiblePage;
}

export default router;
