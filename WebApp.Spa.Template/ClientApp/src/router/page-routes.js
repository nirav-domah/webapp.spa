import StartPage from '@/views/StartPage.vue';
import Page1 from '@/views/Page1.vue';
import LabelPage from '@/views/LabelPage.vue';
import TextBoxPage from '@/views/TextBoxPage.vue';
import DataGridPage from '@/views/DataGridPage.vue';
import LinkPage from '@/views/LinkPage.vue';
import PanelPage from '@/views/PanelPage.vue';
import LayoutGridPage from '@/views/LayoutGridPage.vue';
import LayoutTablePage from '@/views/LayoutTablePage.vue';
import NotificationPage from '@/views/NotificationPage.vue';
import ListControlPage from '@/views/ListControlPage.vue';
import TypesPage from '@/views/TypesPage.vue';
import RadioButtonListPage from '@/views/RadioButtonListPage.vue';

const pageRoutes = [
	{
		path: '/',
		redirect: '/StartPage'
	},
	{
		path: '/StartPage',
		component: StartPage
	},
	{
		path: '/Page1',
		component: Page1
	},
	{
		path: '/LabelPage',
		component: LabelPage
	},
	{
		path: '/TextBoxPage',
		component: TextBoxPage
	},
	{
		path: '/DataGridPage',
		component: DataGridPage
	},
	{
		path: '/LinkPage',
		component: LinkPage
	},
	{
		path: '/PanelPage',
		component: PanelPage
	},
	{
		path: '/LayoutGridPage',
		component: LayoutGridPage
	},
	{
		path: '/LayoutTablePage',
		component: LayoutTablePage
	},
	{
		path: '/NotificationPage',
		component: NotificationPage
	},
	{
		path: '/ListControlPage',
		component: ListControlPage
	},
	{
		path: '/TypesPage',
		component: TypesPage
	},
	{
		path: '/RadioButtonListPage',
		component: RadioButtonListPage
	}
];

export default pageRoutes;
