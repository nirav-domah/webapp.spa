import * as signalR from '@aspnet/signalr';
import navigation from '@/utils/navigation.js';

const connection = new signalR.HubConnectionBuilder()
	.withUrl(`${navigation.getWebAppUrl()}hubs/debugLoggerHub`)
	.configureLogging(signalR.LogLevel.Error)
	.build();

connection.on('Log', message => console.log(message));

connection.on('GroupCollapsed', groupName => console.groupCollapsed(groupName));

connection.on('GroupEnd', () => console.groupEnd());

connection.on('Error', message => console.error(message));

export default connection;
