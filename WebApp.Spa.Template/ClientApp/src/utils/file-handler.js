﻿import http from '@/setup/http.js';

function isFileToken(value) {
	return value?.['$type']?.startsWith('WebApp.Spa.Core.Entities.FileToken');
}

function getFileAsync(fileToken) {
	return http.get('api/FileHandler', {
		params: { content: fileToken }
	});
}

function downloadFileAsync(content, fileName) {
	return http.get('api/FileHandler/Download', {
		params: { content: content, fileName: fileName },
		responseType: 'blob'
	});
}

export default {
	isFileToken,
	getFileAsync,
	downloadFileAsync
};
