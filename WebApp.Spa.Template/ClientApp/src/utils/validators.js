function requiredFieldValidator(value, validationMessage) {
	if (Array.isArray(value)) {
		if (value.length > 0) {
			return { isValid: true };
		}
	} else if (value != null && value !== '') {
		return { isValid: true };
	}

	return {
		isValid: false,
		validationMessage: validationMessage ?? 'Please enter some text'
	};
}

function amountValidator(value) {
	var pattern = /^[+-]?((0\.|[1-9]\d*\.)\d{2}|[1-9]\d*|0)$/;
	var validationMessage = 'Please enter a valid amount';

	return regularExpressionValidator(value, pattern, validationMessage);
}

function emailValidator(value) {
	var pattern = /^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]{1,64}@[a-zA-Z0-9-]{1,64}\.[.a-zA-Z0-9-]{1,64}$/;
	var validationMessage = 'Please enter a valid email address';

	return regularExpressionValidator(value, pattern, validationMessage);
}

function numberValidator(value) {
	var pattern = /^[+-]?[0-9]\d*$/;
	var validationMessage = 'Please enter numbers only';

	return regularExpressionValidator(value, pattern, validationMessage);
}

function urlValidator(value) {
	var pattern = /^(\w{2,}:\/\/)?[\w.\-@]+(:\d{1,5})?(\/[\w\-.~?#[\]!$&'()*+,;=]+)*\/?$/;
	var validationMessage = 'Please enter a valid URL';

	return regularExpressionValidator(value, pattern, validationMessage);
}

function regularExpressionValidator(value, pattern, validationMessage) {
	if (value == null || value === '' || pattern.test(value)) {
		return { isValid: true };
	}

	return {
		isValid: false,
		validationMessage: validationMessage
	};
}

export default {
	requiredFieldValidator,
	amountValidator,
	emailValidator,
	numberValidator,
	urlValidator
};
