import OidcClient from 'oidc-client';
import { useAuthenticationStore } from '@/stores/authentication.js';

async function redirectToLoginAsync() {
	let userManager = new OidcClient.UserManager(getUserManagerSettings());
	await userManager.signinRedirect({ state: window.location.href });
}

async function requestTokensAsync() {
	let userManager = new OidcClient.UserManager(getUserManagerSettings());
	return userManager.signinRedirectCallback();
}

function redirectToLogoutAsync() {
	let userManagerSettings = getUserManagerSettings();

	const authenticationStore = useAuthenticationStore();
	let oidcConfig = authenticationStore.oidcConfig;
	if (oidcConfig.oidcProvider === 'Google' || oidcConfig.oidcProvider === 'Auth0') {
		localStorage.clear();
		userManagerSettings.metadata = {
			end_session_endpoint: oidcConfig.endSessionEndpoint
		};
	}

	let userManager = new OidcClient.UserManager(userManagerSettings);
	return userManager.signoutRedirect();
}

function getUserManagerSettings() {
	const authenticationStore = useAuthenticationStore();
	let oidcConfig = authenticationStore.oidcConfig;

	let userManagerSettings = {
		authority: oidcConfig.authority,
		client_id: oidcConfig.clientId,
		redirect_uri: oidcConfig.redirectUrl,
		post_logout_redirect_uri: oidcConfig.logoutRedirectUrl,
		response_type: oidcConfig.responseType,
		scope: oidcConfig.scope
	};

	if (oidcConfig.oidcProvider === 'Auth0') {
		userManagerSettings.extraQueryParams = {
			audience: oidcConfig.audience
		};
	}

	return userManagerSettings;
}

export default {
	redirectToLoginAsync,
	requestTokensAsync,
	redirectToLogoutAsync
};
