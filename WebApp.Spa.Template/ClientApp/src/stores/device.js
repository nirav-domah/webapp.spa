import { defineStore } from 'pinia';

export const useDeviceStore = defineStore({
	id: 'device',
	state: () => ({
		isMobile: false
	}),
	actions: {
		clearDeviceState() {
			this.$reset();
		}
	}
});
