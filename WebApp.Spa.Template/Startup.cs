﻿using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using WebApp.Spa.Administration;
using WebApp.Spa.Administration.Interfaces;
using WebApp.Spa.Administration.Stores;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.JsonConverters;
using WebApp.Spa.Core.Services.CodeInterpreter;
using WebApp.Spa.Core.Services.Configuration;
using WebApp.Spa.Core.Services.Database;
using WebApp.Spa.Core.Services.DataGridFilter;
using WebApp.Spa.Core.Services.ValueResolver;
using WebApp.Spa.Core.Services.WebService;
using WebApp.Spa.Template.Services.Administration;
using WebApp.Spa.Template.Services.Authentication;
using WebApp.Spa.Template.Services.Authentication.Anonymous;
using WebApp.Spa.Template.Services.Authentication.Cookies;
using WebApp.Spa.Template.Services.Authentication.OAuth;
using WebApp.Spa.Template.Services.Authorization;
using WebApp.Spa.Template.Services.Connectors;
using WebApp.Spa.Template.Services.ControlPropertyValueProvider;
using WebApp.Spa.Template.Services.DebugLogger;
using WebApp.Spa.Template.Services.Files;
using WebApp.Spa.Template.Services.FolderPath;
using WebApp.Spa.Template.Services.HttpClient;
using WebApp.Spa.Template.Services.SessionFiles;
using WebApp.Spa.Template.Services.SessionVariables;
using WebApp.Spa.Template.Services.Spa;
using WebApp.Spa.Template.Services.User;

namespace WebApp.Spa.Template
{
	public class Startup
	{
		private readonly IConfiguration configuration;

		public Startup(IConfiguration configuration)
		{
			this.configuration = configuration;
		}

		public void ConfigureServices(IServiceCollection services)
		{
			var configSection = this.configuration.GetSection(Config.ConfigSectionName);
			var config = configSection.Get<Config>();
			var httpContextAccessor = new HttpContextAccessor();

			services.Configure<Config>(configSection);

			services.AddSingleton<IHttpContextAccessor>(httpContextAccessor);

			services.AddTransient<IFolderPathService, FolderPathService>();

			services.AddApplicationManagerClient(config.ApplicationManagerUrl);

			services.AddDbContext<AdministrationContext>(o => o.UseSqlite(GetAdministrationDbConnectionString(services)))
					.AddScoped<IUserStore, UserStore>()
					.AddScoped<IRoleStore, RoleStore>()
					.AddScoped<IPageStore, PageStore>()
					.AddScoped<ISettingStore, SettingStore>()
					.AddScoped<IAuthenticationSettingStore, AuthenticationSettingStore>()
					.AddScoped<IConnectionStore, ConnectionStore>()
					.AddScoped<IUserAdministration, UserAdministration>()
					.AddScoped<IRoleAdministration, RoleAdministration>()
					.AddScoped<IPageAdministration, PageAdministration>();

			services.AddCookieAuthentication()
					.AddAnonymousAuthentication()
					.AddOAuthAuthentication();

			services.AddDynamicAuthentication();

			services.AddCustomAuthorization();

			services.AddTransient<IUserService, UserService>();

			services.AddSessionVariables();
			services.AddSingleton<ISessionFilesJanitor, SessionFilesJanitor>();

			services.AddSignalR();

			services.AddTransient<IFilesService, FilesService>();

			services.AddTransient<ICodeInterpreter, CodeInterpreter>();

			services.AddTransient<IControlPropertyValueProvider, ControlPropertyValueProvider>();

			services.AddMvc(o => o.Filters.Add(new AuthorizeFilter()))
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
				.AddJsonOptions(o =>
				{
					o.SerializerSettings.ContractResolver = new DefaultContractResolver();  // prevents automatic camelCasing of property names
					o.SerializerSettings.Converters.Add(new StringEnumConverter());
					o.SerializerSettings.Converters.Add(new DateTimeConverter());
					o.SerializerSettings.Converters.Add(new TimeSpanConverter());
					o.SerializerSettings.Converters.Add(new ByteArrayConverter(services.BuildServiceProvider().GetRequiredService<IFilesService>()));
					JsonConvert.DefaultSettings = () => o.SerializerSettings;
				});

			services.Configure<AntiforgeryOptions>(o => o.HeaderName = AntiforgeryConstants.AntiforgeryTokenHeaderName);

			services.Configure<KestrelServerOptions>(o => o.Limits.MaxRequestBodySize = null);
			services.Configure<FormOptions>(o =>
			{
				o.ValueLengthLimit = int.MaxValue;
				o.MultipartBodyLengthLimit = int.MaxValue;
			});

			services.AddDataGridFilter();

			services.AddTransient<IValueResolver, ValueResolver>();
			services.AddTransient<IConnectorsService, ConnectorsService>();
			services.AddTransient<IDatabase, Database>();
			services.AddTransient<IWebService, WebService>();
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			app.UseExceptionHandler("/api/Error");
			//if (env.IsDevelopment())
			//{
			//	//app.UseDeveloperExceptionPage();
			//}
			//else
			//{
			//	app.UseHsts();
			//	app.UseHttpsRedirection();
			//}
			//app.UseHsts();
			//app.UseHttpsRedirection();

			app.UseStaticFiles();

			app.UseAuthentication();

			app.UseSession()
				.UseMiddleware<InitialiseSessionMiddleware>()
				.UseMiddleware<SessionFilesJanitorMiddleware>();

			app.UseSignalR(o => o.MapHub<DebugLoggerHub>("/hubs/debugLoggerHub"));

			app.UseMvc();

			app.UseSpa(env);
		}

		private string GetAdministrationDbConnectionString(IServiceCollection services)
		{
			var folderPathService = services.BuildServiceProvider()
											.GetRequiredService<IFolderPathService>();

			return this.configuration.GetConnectionString(AdministrationContext.ConnectionStringName)
										.Replace("{AppDir}", folderPathService.WebAppPath);
		}
	}
}
