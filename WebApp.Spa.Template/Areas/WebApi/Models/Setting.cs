﻿namespace WebApp.Spa.Template.Areas.WebApi.Models
{
	public class Setting
	{
		public string ID { get; set; }

		public string Name { get; set; }

		public string Value { get; set; }

		public string DefaultValue { get; set; }
	}
}
