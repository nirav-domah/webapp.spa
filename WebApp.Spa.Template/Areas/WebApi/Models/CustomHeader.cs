﻿namespace WebApp.Spa.Template.Areas.WebApi.Models
{
	public class CustomHeader
	{
		public string Name { get; set; }

		public string Value { get; set; }
	}
}
