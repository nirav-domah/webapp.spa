﻿using System;

namespace WebApp.Spa.Template.Areas.WebApi.Models
{
	public class ApplicationUpdate
	{
		public string Id { get; set; }

		public DateTime DateTime { get; set; }

		public string UserId { get; set; }

		public string Username { get; set; }

		public string DesignerVersion { get; set; }

		public bool FileExists { get; set; }
	}
}
