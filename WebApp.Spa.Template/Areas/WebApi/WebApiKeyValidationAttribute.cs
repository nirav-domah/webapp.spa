﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using WebApp.Spa.Core;

namespace WebApp.Spa.Template.Areas.WebApi
{
	public class WebApiKeyValidationAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			string webApiKey = RegistryKeys.Instance.ApiKey;
			string receivedWebApiKey = context.ActionArguments["key"]?.ToString();

			if (receivedWebApiKey == null || receivedWebApiKey != webApiKey)
				throw new Exception("Invalid key");

			base.OnActionExecuting(context);
		}
	}
}
