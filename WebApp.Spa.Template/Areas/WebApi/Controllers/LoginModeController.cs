﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Enums;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.Configuration;
using WebApp.Spa.Core.Services.OAuth.ProviderSettings;
using WebApp.Spa.Template.Areas.WebApi.Models;

namespace WebApp.Spa.Template.Areas.WebApi.Controllers
{
	[ApiController]
	[Area("WebApi")]
	[Route("api/login-mode")]
	[AllowAnonymous]
	[WebApiKeyValidation]
	public class LoginModeController : ControllerBase
	{
		private readonly Config config;
		private readonly IUserAdministration userAdministration;
		private readonly IAuthenticationSettingStore authenticationSettingStore;

		public LoginModeController(
			IOptionsSnapshot<Config> configOptions,
			IUserAdministration userAdministration,
			IAuthenticationSettingStore authenticationSettingStore)
		{
			this.config = configOptions.Value;
			this.userAdministration = userAdministration;
			this.authenticationSettingStore = authenticationSettingStore;
		}

		// GET: api/login-mode?key=key
		[HttpGet]
		public IActionResult GetLoginMode(string key)
		{
			return Ok((int)this.config.AuthenticationType);
		}

		// GET: api/login-mode/email-exists?key=key&email=email
		[HttpGet]
		[Route("email-exists")]
		public async Task<IActionResult> EmailExists(string key, string email)
		{
			return Ok(await this.userAdministration.EmailExistsAsync(email));
		}

		// GET: api/login-mode/authentication-settings?key=key
		[HttpGet]
		[Route("authentication-settings")]
		public IActionResult AuthenticationSettings(string key)
		{
			return Ok(this.authenticationSettingStore.AuthenticationSettings.ToArray());
		}

		// PUT: api/login-mode?key=key
		[HttpPut]
		public async Task<IActionResult> EditLoginMode(string key, LoginModeInfo loginModeInfo)
		{
			switch (loginModeInfo.NewSecurityMode)
			{
				case AuthenticationType.Anonymous: SetAuthenticationType(loginModeInfo.NewSecurityMode); break;
				case AuthenticationType.Cookie:
					{
						if (string.IsNullOrEmpty(loginModeInfo.Email) && !string.IsNullOrEmpty(loginModeInfo.Password))
							throw new Exception("The Email field is required.");

						bool emailExists = !string.IsNullOrEmpty(loginModeInfo.Email) && await this.userAdministration.EmailExistsAsync(loginModeInfo.Email);

						if (!emailExists && !string.IsNullOrEmpty(loginModeInfo.Email) && string.IsNullOrEmpty(loginModeInfo.Password))
							throw new Exception("The Password field is required.");

						SetAuthenticationType(loginModeInfo.NewSecurityMode);

						if (emailExists)
						{
							var userInfo = await this.userAdministration.GetUserByEmailAsync(loginModeInfo.Email);
							await this.userAdministration.EditUserAsync(
								userInfo.User.Id,
								userInfo.User.Email,
								loginModeInfo.Name,
								isAdministrator: true,
								loginModeInfo.Password
							);
						}
						else if (loginModeInfo.Email != null && loginModeInfo.Password != null)
						{
							await this.userAdministration.AddUserAsync(
								loginModeInfo.Email,
								loginModeInfo.Name,
								loginModeInfo.Password,
								isAdministrator: true,
								new[] { Role.DefaultRoleName }
							);
						}

						break;
					}
				case AuthenticationType.OAuth:
					{
						await this.authenticationSettingStore.UpdateAllAsync(
							new[]
							{
								new AuthenticationSetting(AuthenticationSetting.RedirectUrl, loginModeInfo.RedirectUrl),
								new AuthenticationSetting(AuthenticationSetting.LogoutRedirectUrl, loginModeInfo.LogoutRedirectUrl),
								new AuthenticationSetting(AuthenticationSetting.OidcProvider, loginModeInfo.OidcProvider.ToString()),
								new AuthenticationSetting(AuthenticationSetting.AuthenticationAuthorityUri, loginModeInfo.AuthenticationAuthorityUri),
								new AuthenticationSetting(AuthenticationSetting.Tenant, loginModeInfo.Tenant),
								new AuthenticationSetting(AuthenticationSetting.ClientID, loginModeInfo.ClientID),
								new AuthenticationSetting(AuthenticationSetting.ApiResourceName, loginModeInfo.ApiResourceName),
								new AuthenticationSetting(AuthenticationSetting.ApiResourceSecret, loginModeInfo.ApiResourceSecret),
								new AuthenticationSetting(AuthenticationSetting.RoleClaimName, loginModeInfo.RoleClaimName),
								new AuthenticationSetting(AuthenticationSetting.Audience, loginModeInfo.Audience),
								new AuthenticationSetting(AuthenticationSetting.Scopes, loginModeInfo.Scopes)
							}
						);

						var oidcSettings = OidcSettingsProvider.CreateInstance(this.authenticationSettingStore.AuthenticationSettings.ToDictionary(s => s.Name, s => s.Value))
																.Settings;
						AppSettingsHelper.ExecuteAndSave(s =>
						{
							s.Config.OAuth.Authority = oidcSettings.Authority;
							s.Config.OAuth.Audience = oidcSettings.Audience;
							s.Config.OAuth.ClientId = oidcSettings.ClientId;
							s.Config.OAuth.ApiResourceName = oidcSettings.ApiResourceName;
							s.Config.OAuth.ApiResourceSecret = oidcSettings.ApiResourceSecret;
							s.Config.OAuth.Scopes = oidcSettings.Scopes;
						});
						SetAuthenticationType(loginModeInfo.NewSecurityMode);

						bool hasUniqueId = !string.IsNullOrEmpty(loginModeInfo.UniqueId);
						bool hasEmail = !string.IsNullOrEmpty(loginModeInfo.Email);

						if ((hasUniqueId && this.userAdministration.TryGetUserByUniqueId(loginModeInfo.UniqueId, out var userInfo))
							|| (hasEmail && this.userAdministration.TryGetUserByEmail(loginModeInfo.Email, out userInfo)))
						{
							if (!userInfo.User.IsAdministrator)
								await this.userAdministration.EditUserAsync(userInfo.User.Id, isAdministrator: true);

							if (hasEmail && (userInfo.User.Email != loginModeInfo.Email))
								await this.userAdministration.EditUserAsync(userInfo.User.Id, email: loginModeInfo.Email);

							if (hasUniqueId && (userInfo.User.UniqueId != loginModeInfo.UniqueId))
								await this.userAdministration.EditUserAsync(userInfo.User.Id, uniqueId: loginModeInfo.UniqueId);

							if (userInfo.User.Name != loginModeInfo.Name)
								await this.userAdministration.EditUserAsync(userInfo.User.Id, name: loginModeInfo.Name);

						}
						else if (hasUniqueId || hasEmail)
						{
							await this.userAdministration.AddUserAsync(
									loginModeInfo.Email,
									loginModeInfo.Name,
									password: null,
									isAdministrator: true,
									roles: new[] { Role.DefaultRoleName },
									loginModeInfo.UniqueId
								);
						}

						break;
					}
			}

			return Ok();
		}

		private void SetAuthenticationType(AuthenticationType authenticationType)
		{
			AppSettingsHelper.ExecuteAndSave(s => s.Config.AuthenticationType = authenticationType);
		}
	}
}
