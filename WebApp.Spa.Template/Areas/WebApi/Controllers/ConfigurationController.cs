﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using WebApp.Spa.Core.Services.Configuration;
using WebApp.Spa.Template.Areas.WebApi.Models;

namespace WebApp.Spa.Template.Areas.WebApi.Controllers
{
	[ApiController]
	[Area("WebApi")]
	[Route("api/[controller]")]
	[AllowAnonymous]
	[WebApiKeyValidation]
	public class ConfigurationController : ControllerBase
	{
		// GET: api/configuration/all?key=key
		[HttpGet]
		[Route("all")]
		public IActionResult Get(string key)
		{
			var webConfigHelper = WebConfigHelper.GetInstance();

			uint maxAllowedContentLengthInBytes = webConfigHelper.GetMaxAllowedContentLength();
			int maxAllowedContentLengthInKB = (int)(maxAllowedContentLengthInBytes / 1024);

			return Ok(new
			{
				MaxRequestLength = maxAllowedContentLengthInKB,
				CustomHeaders = webConfigHelper.GetCustomHeaders()
												.Select(h => new CustomHeader()
												{
													Name = h.Key,
													Value = h.Value
												})
			});
		}

		// PUT: api/configuration/max-request-length?key=key
		[HttpPut]
		[Route("max-request-length")]
		public IActionResult EditMaxRequestLength(string key, [FromBody] int maxRequestLengthInKB)
		{
			WebConfigHelper.ExecuteAndSave(
				w => w.SetMaxAllowedContentLength(maxRequestLengthInKB)
			);

			return Ok();
		}

		// POST: api/configuration/custom-header?key=key
		[HttpPost]
		[Route("custom-header")]
		public IActionResult AddCustomHeader(string key, CustomHeader customHeader)
		{
			WebConfigHelper.ExecuteAndSave(
				w => w.AddCustomHeader(customHeader.Name, customHeader.Value)
			);

			return Ok();
		}

		// PUT: api/configuration/custom-header?key=key&name=customHeaderName
		[HttpPut]
		[Route("custom-header")]
		public IActionResult EditCustomHeader(string key, string name, CustomHeader customHeader)
		{
			WebConfigHelper.ExecuteAndSave(
				w => w.SetCustomHeader(name, customHeader.Name, customHeader.Value)
			);

			return Ok();
		}

		// DELETE: api/configuration/custom-header?key=key&name=customHeaderName
		[HttpDelete]
		[Route("custom-header")]
		public IActionResult DeleteCustomHeader(string key, string name)
		{
			WebConfigHelper.ExecuteAndSave(
				w => w.RemoveCustomHeader(name)
			);

			return Ok();
		}
	}
}
