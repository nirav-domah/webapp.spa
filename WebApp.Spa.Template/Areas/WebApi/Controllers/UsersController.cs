﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Template.Areas.WebApi.Models;

namespace WebApp.Spa.Template.Areas.WebApi.Controllers
{
	[ApiController]
	[Area("WebApi")]
	[Route("api/[controller]")]
	[AllowAnonymous]
	[AuthenticationTypeValidation]
	[UserApiKeyValidation]
	public class UsersController : ControllerBase
	{
		private readonly IUserAdministration userAdministration;

		public UsersController(IUserAdministration userAdministration)
		{
			this.userAdministration = userAdministration;
		}

		// GET: api/users?key=key
		[HttpGet]
		public async Task<IActionResult> All(string key)
		{
			var users = new List<User>();

			foreach (var userInfo in await this.userAdministration.GetUsersAsync())
			{
				users.Add(
					new User
					{
						Id = userInfo.User.Id,
						Email = userInfo.User.Email,
						Name = userInfo.User.Name,
						Username = userInfo.User.UserName,
						IsAdministrator = userInfo.User.IsAdministrator,
						Roles = userInfo.Roles.ToArray()
					}
				);
			}

			return Ok(users);
		}

		// GET: api/users/count?key=key
		[HttpGet]
		[Route("count")]
		public async Task<IActionResult> Count(string key)
		{
			int count = (await this.userAdministration.GetUsersAsync()).Count();

			return Ok(count);
		}

		// GET: api/users/<id>?key=key
		[HttpGet]
		[Route("{id}")]
		public async Task<IActionResult> GetUserById(string id, string key)
		{
			var userInfo = await this.userAdministration.GetUserByIdAsync(id);

			return Ok(new User
			{
				Id = userInfo.User.Id,
				Email = userInfo.User.Email,
				Name = userInfo.User.Name,
				Username = userInfo.User.UserName,
				IsAdministrator = userInfo.User.IsAdministrator,
				Roles = userInfo.Roles.ToArray()
			});
		}

		// GET: api/users/find?key=key&email=email
		[HttpGet]
		[Route("find")]
		public async Task<IActionResult> GetUserByEmail(string key, string email)
		{
			var userInfo = await this.userAdministration.GetUserByEmailAsync(email);

			return Ok(new User
			{
				Id = userInfo.User.Id,
				Email = userInfo.User.Email,
				Name = userInfo.User.Name,
				Username = userInfo.User.UserName,
				IsAdministrator = userInfo.User.IsAdministrator,
				Roles = userInfo.Roles.ToArray()
			});
		}

		// POST: api/users?key=key
		[HttpPost]
		public async Task<IActionResult> Add(string key, UserInfo userToAdd)
		{
			await this.userAdministration.AddUserAsync(
				userToAdd.Email,
				userToAdd.Name,
				userToAdd.Password,
				userToAdd.IsAdministrator ?? false,
				userToAdd.Roles
			);

			var userInfo = await this.userAdministration.GetUserByEmailAsync(userToAdd.Email);

			return Ok(new
			{
				userID = userInfo.User.Id
			});
		}

		// PUT: api/users/<id>?key=key
		[HttpPut]
		[Route("{id}")]
		public async Task<IActionResult> Edit(string id, string key, UserInfo userModel)
		{
			await this.userAdministration.EditUserAsync(
				id,
				userModel.Email,
				userModel.Name,
				userModel.IsAdministrator ?? false,
				userModel.Password,
				userModel.Roles
			);

			return Ok(new
			{
				Message = "User updated successfully"
			});
		}

		// DELETE: api/users/<id>?key=key
		[HttpDelete]
		[Route("{id}")]
		public async Task<IActionResult> Delete(string id, string key)
		{
			await this.userAdministration.DeleteUserAsync(id);

			return Ok(new
			{
				Message = "User deleted successfully"
			});
		}
	}
}
