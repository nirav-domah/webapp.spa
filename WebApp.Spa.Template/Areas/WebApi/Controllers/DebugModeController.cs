﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApp.Spa.Core.Services.Configuration;

namespace WebApp.Spa.Template.Areas.WebApi.Controllers
{
	[ApiController]
	[Area("WebApi")]
	[Route("api/debug-mode")]
	[AllowAnonymous]
	[WebApiKeyValidation]
	public class DebugModeController : ControllerBase
	{
		private readonly Config config;

		public DebugModeController(IOptionsSnapshot<Config> configOptions)
		{
			this.config = configOptions.Value;
		}

		// GET: api/debug-mode?key=key
		[HttpGet]
		public IActionResult GetDebugMode(string key)
		{
			return Ok(this.config.Debug);
		}

		// PUT: api/debug-mode?key=key
		[HttpPut]
		public IActionResult SetDebugMode(string key, [FromBody] bool debugMode)
		{
			AppSettingsHelper.ExecuteAndSave(s => s.Config.Debug = debugMode);

			return Ok();
		}
	}
}
