﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Services.Configuration;
using WebApp.Spa.Template.Areas.Authentication.Models.Password;
using WebApp.Spa.Template.Controllers;
using WebApp.Spa.Template.Helpers;
using WebApp.Spa.Template.Services.HttpClient;

namespace WebApp.Spa.Template.Areas.Authentication.Controllers
{
	[ApiController]
	[Area("Authentication")]
	[Route("api/[area]/[controller]/[action]")]
	public class PasswordController : AValidatedControllerBase
	{
		private readonly UserManager<User> userManager;
		private readonly IHttpClientFactory httpClientFactory;
		private readonly Config config;

		public PasswordController(
			UserManager<User> userManager,
			IHttpClientFactory httpClientFactory,
			IOptionsSnapshot<Config> configOptions)
		{
			this.userManager = userManager;
			this.httpClientFactory = httpClientFactory;
			this.config = configOptions.Value;
		}

		// POST: api/Authentication/Password/ChangePassword
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ChangePassword(ChangePasswordInfo changePasswordInfo)
		{
			var user = await this.userManager.GetUserAsync(User);

			var changePasswordIdentityResult = await this.userManager.ChangePasswordAsync(user, changePasswordInfo.OldPassword, changePasswordInfo.NewPassword);
			if (!changePasswordIdentityResult.Succeeded)
				return GetValidationResult(string.Join(", ", changePasswordIdentityResult.Errors.Select(e => e.Description)));

			return NoContent();
		}

		// GET: api/Authentication/Password/CanResetPassword
		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> CanResetPassword()
		{
			var applicationManagerClient = this.httpClientFactory.CreateClient(HttpClients.ApplicationManager);
			string apiUrl = UrlHelper.Combine(this.config.ApplicationManagerUrl, "api/forgot-password/can-send-password-reset-email");

			var response = await applicationManagerClient.GetAsync(apiUrl);

			if (!response.IsSuccessStatusCode)
				return GetValidationResult("The Application Manager could not be contacted");

			bool canResetPassword = bool.Parse(await response.Content.ReadAsStringAsync());
			if (!canResetPassword)
				return GetValidationResult("Please contact the Administrator for this application");

			return Ok(canResetPassword);
		}

		// POST: api/Authentication/Password/SendPasswordResetEmail
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> SendPasswordResetEmail(PasswordResetInfo passwordResetInfo)
		{
			var user = await this.userManager.FindByEmailAsync(passwordResetInfo.Email);
			if (user == null)
				return GetValidationResult("Email not found");

			string passwordResetToken = await this.userManager.GeneratePasswordResetTokenAsync(user);

			string resetPasswordUrl = UrlHelper.AddQueryString(
				passwordResetInfo.PasswordResetUrl,
				new Dictionary<string, string>
				{
					{ "userId", user.Id },
					{ "passwordResetToken", passwordResetToken }
				});

			var applicationManagerClient = this.httpClientFactory.CreateClient(HttpClients.ApplicationManager);
			string forgotPasswordApiUrl = UrlHelper.Combine(this.config.ApplicationManagerUrl, "api/forgot-password/send-password-reset-email");

			var content = GetStringContent(new
			{
				email = user.Email,
				resetUrl = resetPasswordUrl,
				appName = this.config.WebAppName
			});
			var response = await applicationManagerClient.PostAsync(forgotPasswordApiUrl, content);

			if (!response.IsSuccessStatusCode)
				return GetValidationResult("Could not send password reset email");

			return NoContent();
		}

		// POST: api/Authentication/Password/UpdatePassword
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> UpdatePassword(UpdatePasswordInfo updatePasswordInfo)
		{
			var user = await this.userManager.FindByIdAsync(updatePasswordInfo.UserId);
			if (user == null)
				return GetValidationResult("User not found");

			var resetPasswordIdentityResult = await this.userManager.ResetPasswordAsync(user, updatePasswordInfo.PasswordResetToken, updatePasswordInfo.NewPassword);
			if (!resetPasswordIdentityResult.Succeeded)
				return GetValidationResult(string.Join(", ", resetPasswordIdentityResult.Errors.Select(e => e.Description)));

			return NoContent();
		}

		private StringContent GetStringContent(object value)
		{
			string payload = JsonConvert.SerializeObject(value);
			var content = new StringContent(payload);
			content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

			return content;
		}
	}
}
