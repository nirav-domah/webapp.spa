﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Spa.Template.Areas.Authentication.Models.Password
{
	public class ChangePasswordInfo
	{
		[Required]
		public string OldPassword { get; set; }

		[Required]
		public string NewPassword { get; set; }
	}
}
