﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Enums;

namespace WebApp.Spa.Template.Areas.Authentication.Models
{
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class AuthenticationState
	{
		public AuthenticationType AuthenticationType { get; set; }

		public UserClaims UserClaims { get; set; }

		public AuthenticationSettings AuthenticationSettings { get; set; }
	}
}
