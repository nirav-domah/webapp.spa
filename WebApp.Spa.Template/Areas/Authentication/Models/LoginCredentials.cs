﻿using System.ComponentModel.DataAnnotations;
using WebApp.Spa.Template.Models.Validation;

namespace WebApp.Spa.Template.Areas.Authentication.Models
{
	public class LoginCredentials
	{
		[Required]
		[Email]
		public string Email { get; set; }

		[Required]
		public string Password { get; set; }
	}
}
