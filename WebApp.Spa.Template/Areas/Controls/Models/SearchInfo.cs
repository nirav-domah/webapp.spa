﻿using System.Collections.Generic;

namespace WebApp.Spa.Template.Areas.Controls.Models
{
	public class SearchInfo
	{
		public string SearchQuery { get; set; }

		public IDictionary<string, string> ColumnNameHeaderMap { get; set; }
	}
}
