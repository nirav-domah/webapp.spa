﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.JsonConverters;
using WebApp.Spa.Core.Services.Configuration;
using WebApp.Spa.Template.Areas.Controls.Models;

namespace WebApp.Spa.Template.Areas.Controls.Controllers
{
	[ApiController]
	[Area("Controls")]
	[Route("api/[area]/[controller]/[action]")]
	public class DataGridController : ControllerBase
	{
		private readonly IDataGridFilter dataGridFilter;
		private readonly Config config;

		public DataGridController(IDataGridFilter dataGridFilter, IOptionsSnapshot<Config> configOptions)
		{
			this.dataGridFilter = dataGridFilter;
			this.config = configOptions.Value;
		}

		// POST: api/Controls/DataGrid/SetData?controlId=[controlId]&isChunkStart=[isChunkStart]
		[HttpPost]
		public IActionResult SetData(string controlId, bool isChunkStart, [FromBody] string jsonData)
		{
			if (isChunkStart)
			{
				HttpContext.Session.SetString(controlId, jsonData);
			}
			else
			{
				string existingJsonData = HttpContext.Session.GetString(controlId);
				var existingData = JsonConvert.DeserializeObject<DataTable>(existingJsonData);

				var newData = JsonConvert.DeserializeObject<DataTable>(jsonData);
				existingData.Merge(newData, false, MissingSchemaAction.Add);

				string mergedJsonData = JsonConvert.SerializeObject(existingData);
				HttpContext.Session.SetString(controlId, mergedJsonData);
			}

			return NoContent();
		}

		// POST: api/Controls/DataGrid/Search?controlId=[controlId]
		[HttpPost]
		public IActionResult Search(string controlId, SearchInfo searchInfo)
		{
			string jsonData = HttpContext.Session.GetString(controlId);

			var data = JArray.Parse(jsonData);
			if (string.IsNullOrEmpty(searchInfo.SearchQuery))
				return Ok(data);

			this.dataGridFilter.Initialise(
				searchInfo.SearchQuery,
				searchInfo.ColumnNameHeaderMap,
				(rowIndex, columnName) => ((JObject)data[rowIndex]).GetValue(columnName).ToObject<object>());

			var filteredData = new List<JToken>();
			for (int i = 0; i < data.Count; i++)
			{
				if (this.dataGridFilter.MeetsFilterCriteria(i))
					filteredData.Add(data[i]);
			}

			return Ok(filteredData);
		}

		// GET: api/Controls/DataGrid/Export?controlId=[controlId]
		[HttpGet]
		public IActionResult Export(string controlId)
		{
			string jsonData = HttpContext.Session.GetString(controlId);

			var data = JsonConvert.DeserializeObject<DataTable>(jsonData, new IsoDateTimeConverter { DateTimeFormat = JsonSerializerFormats.DateTime });

			var memoryStream = new MemoryStream();

			using (var excelPackage = new ExcelPackage(memoryStream))
			{
				var worksheet = excelPackage.Workbook.Worksheets.Add("Worksheet 1");
				worksheet.Cells["A1"].LoadFromDataTable(data, true);
				SetColumnFormats(data, worksheet);
				excelPackage.Save();
			}
			memoryStream.Position = 0;

			return File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		}

		private void SetColumnFormats(DataTable dataTable, ExcelWorksheet worksheet)
		{
			if (dataTable.Rows.Count != 0)
			{
				var firstRow = dataTable.Rows[0];
				for (int columnIndex = 0; columnIndex < firstRow.ItemArray.Length; columnIndex++)
				{
					if (firstRow[columnIndex] is DateTime)
						worksheet.Column(columnIndex + 1).Style.Numberformat.Format = this.config.DateFormat;
				}
			}
		}
	}
}
