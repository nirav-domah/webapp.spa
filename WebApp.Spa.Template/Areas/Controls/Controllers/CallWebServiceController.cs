﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace WebApp.Spa.Template.Areas.Controls.Controllers
{
	[ApiController]
	[Area("Controls")]
	[Route("api/[area]/[controller]/[action]")]
	public class CallWebServiceController : ControllerBase
	{
		// POST: api/Controls/CallWebService/Execute
		[HttpPost]
		public IActionResult Execute(object parameters)
		{
			var headers = new List<KeyValuePair<string, string>>();
			headers.AddRange(((JObject)parameters).Value<JArray>("headers")
			.Select(i =>
			{
				string name = ((JObject)i).Value<string>("name");
				string value = ((JObject)i).Value<string>("value");

				return new KeyValuePair<string, string>(name, value);
			}));

			return new NoContentResult();
		}

		public class Token
		{
			public string name { get; set; }
			public string value { get; set; }
		}
	}
}
