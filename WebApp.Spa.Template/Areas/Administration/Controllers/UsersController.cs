﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Template.Areas.Administration.Models.Users;
using WebApp.Spa.Template.Services.Authorization;

namespace WebApp.Spa.Template.Areas.Administration.Controllers
{
	[ApiController]
	[Authorize(Policies.AdministratorAccess)]
	[Area("Administration")]
	[Route("api/[area]/[controller]/[action]")]
	public class UsersController : ControllerBase
	{
		private readonly IUserAdministration userAdministration;
		private readonly SignInManager<User> signInManager;

		public UsersController(IUserAdministration userAdministration, SignInManager<User> signInManager)
		{
			this.userAdministration = userAdministration;
			this.signInManager = signInManager;
		}

		// GET: api/Administration/Users/All
		[HttpGet]
		public async Task<IActionResult> All()
		{
			var userInfos = await this.userAdministration.GetUsersAsync();
			int administratorCount = userInfos.Select(u => u.User.IsAdministrator).Count();

			string loggedInUserId = GetLoggedInUserId();

			var userRecords = userInfos.Select(u => new UserRecord
			{
				Id = u.User.Id,
				Email = u.User.Email,
				Username = u.User.UserName,
				Name = u.User.Name,
				IsAdministrator = u.User.IsAdministrator,
				IsLastAdministrator = u.User.IsAdministrator && administratorCount <= 1,
				IsLoggedIn = u.User.Id == loggedInUserId,
				Roles = u.Roles
			}).OrderBy(u => u.Username);

			return Ok(userRecords);
		}

		// POST: api/Administration/Users/Add
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Add(UserToAdd userModel)
		{
			await this.userAdministration.AddUserAsync(
				userModel.Email,
				userModel.Name,
				userModel.Password,
				userModel.IsAdministrator,
				userModel.Roles
			);

			return NoContent();
		}

		// PUT: api/Administration/Users/Edit
		[HttpPut]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(UserToEdit userModel)
		{
			await this.userAdministration.EditUserAsync(
				userModel.Id,
				userModel.Email,
				userModel.Name ?? string.Empty,
				userModel.Id == GetLoggedInUserId() ? (bool?)null : userModel.IsAdministrator,
				userModel.Password,
				userModel.Roles
			);

			var userInfo = await this.userAdministration.GetUserByIdAsync(userModel.Id);

			if (userModel.Id == GetLoggedInUserId())
				await this.signInManager.RefreshSignInAsync(userInfo.User);

			return NoContent();
		}

		// PUT: api/Administration/Users/EditIsAdministrator
		[HttpPut]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EditIsAdministrator(IsAdministratorToEdit userModel)
		{
			await this.userAdministration.EditIsAdministrator(userModel.Id, userModel.IsAdministrator);

			return NoContent();
		}

		// DELETE: api/Administration/Users/Delete?id=[id]
		[HttpDelete]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Delete([FromQuery] UserToDelete userModel)
		{
			await this.userAdministration.DeleteUserAsync(userModel.Id);

			return NoContent();
		}

		private string GetLoggedInUserId()
		{
			return User.FindFirstValue(ClaimTypes.NameIdentifier);
		}
	}
}
