﻿using System.ComponentModel.DataAnnotations;
using WebApp.Spa.Template.Areas.Administration.Models.Roles.Validation;

namespace WebApp.Spa.Template.Areas.Administration.Models.Roles
{
	public class RoleToDelete
	{
		[Required]
		[Exists]
		public string Id { get; set; }
	}
}
