﻿using System.ComponentModel.DataAnnotations;
using WebApp.Spa.Template.Areas.Administration.Models.Users.Validation;

namespace WebApp.Spa.Template.Areas.Administration.Models.Users
{
	public class UserToEdit : UserBase, IIsAdministratorToBeEdited
	{
		[Required]
		[Exists]
		[IsNotLastAdministrator]
		public string Id { get; set; }
	}
}
