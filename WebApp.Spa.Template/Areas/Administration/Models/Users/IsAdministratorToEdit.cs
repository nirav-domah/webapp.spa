﻿using System.ComponentModel.DataAnnotations;
using WebApp.Spa.Template.Areas.Administration.Models.Users.Validation;

namespace WebApp.Spa.Template.Areas.Administration.Models.Users
{
	public class IsAdministratorToEdit : IIsAdministratorToBeEdited
	{
		[Required]
		[Exists]
		[IsNotLoggedIn(ErrorMessage = "The currently logged-in user cannot be removed as administrator")]
		[IsNotLastAdministrator]
		public string Id { get; set; }

		public bool IsAdministrator { get; set; }
	}
}
