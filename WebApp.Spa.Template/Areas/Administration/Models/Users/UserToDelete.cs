﻿using System.ComponentModel.DataAnnotations;
using WebApp.Spa.Template.Areas.Administration.Models.Users.Validation;

namespace WebApp.Spa.Template.Areas.Administration.Models.Users
{
	public class UserToDelete
	{
		[Required]
		[Exists]
		[IsNotLoggedIn(ErrorMessage = "The currently logged-in user cannot be deleted")]
		[IsNotLastAdministrator]
		public string Id { get; set; }
	}
}
