﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Spa.Core.Extensions;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.Configuration;
using WebApp.Spa.Core.Services.WebService;
using WebApp.Spa.Template.Helpers;
using WebApp.Spa.Template.Services.Authorization;
using WebApp.Spa.Template.Services.DebugLogger;

namespace WebApp.Spa.Template.Areas.Documents.Controllers.Pages
{
	[ApiController]
	[Authorize(Policies.RolesAccess)]
	[Area("Documents")]
	[Route("api/[area]/Pages/[controller]/[action]")]
	public class DataGridPageController : ControllerBase
	{
		private readonly Config config;
		private readonly IValueResolver valueResolver;
		private readonly IDatabase database;
		private readonly IWebService webService;
		private readonly IHubContext<DebugLoggerHub, IDebugLoggerClient> debugLoggerHubContext;

		public DataGridPageController(
			IOptionsSnapshot<Config> configOptions,
			IValueResolver valueResolver,
			IDatabase database,
			IWebService webService,
			IHubContext<DebugLoggerHub, IDebugLoggerClient> debugLoggerHubContext
		)
		{
			this.config = configOptions.Value;
			this.valueResolver = valueResolver;
			this.database = database;
			this.webService = webService;
			this.debugLoggerHubContext = debugLoggerHubContext;
		}

		// POST: api/Pages/DataGridPage/DataGridPage_Load_WebService1_Operation1
		[HttpPost]
		public async Task<IActionResult> DataGridPage_Load_WebService1_Operation1(object parameters)
		{
			IDebugLoggerClient debugLogger = null;
			if (AppSettingsHelper.GetInstance().GetAppSettings().Config.Debug)
			{
				string connectionId = this.valueResolver.ResolveParameter<string>((JObject)parameters, "connectionId");
				if (connectionId != null)
					debugLogger = this.debugLoggerHubContext.Clients.Client(connectionId);
			}

			object resultsContainer;

			try
			{
				int timeout = 90;
				string baseUrl = string.Empty;
				string username = null, password = null, keyLocation = null, keyName = null, keyValue = null;
				var authentication = WebService.Authentication.Anonymous;

				string connectionString = @"{""URL"":""https://randomuser.me/api/"",""Auth"":""Anonymous"",""Timeout"":90}";
				var connection = (JObject)JsonConvert.DeserializeObject(connectionString);

				if (connection != null)
				{
					if (connection.TryGetValue("URL", out var parsedUrl)) baseUrl = (string)parsedUrl;
					if (connection.TryGetValue("Timeout", out var parsedTimeout)) timeout = (int)parsedTimeout;
					if (connection.TryGetValue("Username", out var parsedUsername)) username = (string)parsedUsername;
					if (connection.TryGetValue("Password", out var parsedPassword)) password = (string)parsedPassword;
					if (connection.TryGetValue("KeyName", out var parsedKeyName)) keyName = (string)parsedKeyName;
					if (connection.TryGetValue("KeyValue", out var parsedKeyValue)) keyValue = (string)parsedKeyValue;
					if (connection.TryGetValue("KeyLocation", out var parsedKeyLocation)) keyLocation = (string)parsedKeyLocation;
					if (connection.TryGetValue("Auth", out var parsedAuth) && Enum.TryParse<WebService.Authentication>((string)parsedAuth, out var parsedAuthentication))
						authentication = parsedAuthentication;
				}

				var queryStrings = new List<KeyValuePair<string, string>>();
				var headers = new List<KeyValuePair<string, string>>();
				var cookies = new List<KeyValuePair<string, string>>();
				var multipartParameters = new List<WebService.MultipartParameter>();

				if (authentication == WebService.Authentication.ApiKey)
				{
					switch (keyLocation)
					{
						case "QueryString": queryStrings.Add(new KeyValuePair<string, string>(keyName, keyValue)); break;
						case "Header": headers.Add(new KeyValuePair<string, string>(keyName, keyValue)); break;
						case "Cookie": cookies.Add(new KeyValuePair<string, string>(keyName, keyValue)); break;
					}
				}

				string path = $@"";
				string url = string.IsNullOrEmpty(path) ? baseUrl : UrlHelper.Combine(baseUrl, path);
				url = UrlHelper.EnsureHttpProtocol(url);

				if (debugLogger != null)
					this.webService.EnableDebugLogger(debugLogger);

				object webServiceFunctionResult = await this.webService.Execute(
											url,
											WebService.HttpMethod.GET,
											queryStrings,
											headers,
											multipartParameters,
											null,
											null,
											cookies,
											WebService.ResponseContentType.Text,
											TimeSpan.FromSeconds(timeout),
											authentication,
											username,
											password
										);

				resultsContainer = new
				{
					results = webServiceFunctionResult
				};
			}
			catch (Exception exception)
			{
				if (debugLogger != null)
					await debugLogger.Error($"Error: {exception.GetDisplayMessage(": ")}");

				throw;
			}

			return Ok(resultsContainer);
		}

		// POST: api/Pages/DataGridPage/DataGridPage_Load_Database1_Query1
		[HttpPost]
		public async Task<IActionResult> DataGridPage_Load_Database1_Query1(object parameters)
		{
			IDebugLoggerClient debugLogger = null;
			if (AppSettingsHelper.GetInstance().GetAppSettings().Config.Debug)
			{
				string connectionId = this.valueResolver.ResolveParameter<string>((JObject)parameters, "connectionId");
				if (connectionId != null)
					debugLogger = this.debugLoggerHubContext.Clients.Client(connectionId);
			}

			object resultsContainer;

			try
			{
				resultsContainer = new[]
				{
					new Dictionary<string, object>
					{
						{ "Column1", "Row01Column1Value" },
						{ "Column2", "Row01Column2Value" },
						{ "Column3", "Row01Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row02Column1Value" },
						{ "Column2", "Row02Column2Value" },
						{ "Column3", "Row02Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row03Column1Value" },
						{ "Column2", "Row03Column2Value" },
						{ "Column3", "Row03Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row04Column1Value" },
						{ "Column2", "Row04Column2Value" },
						{ "Column3", "Row04Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row05Column1Value" },
						{ "Column2", "Row05Column2Value" },
						{ "Column3", "Row05Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row06Column1Value" },
						{ "Column2", "Row06Column2Value" },
						{ "Column3", "Row06Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row07Column1Value" },
						{ "Column2", "Row07Column2Value" },
						{ "Column3", "Row07Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row08Column1Value" },
						{ "Column2", "Row08Column2Value" },
						{ "Column3", "Row08Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row09Column1Value" },
						{ "Column2", "Row09Column2Value" },
						{ "Column3", "Row09Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row10Column1Value" },
						{ "Column2", "Row10Column2Value" },
						{ "Column3", "Row10Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row11Column1Value" },
						{ "Column2", "Row11Column2Value" },
						{ "Column3", "Row11Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row12Column1Value" },
						{ "Column2", "Row12Column2Value" },
						{ "Column3", "Row12Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row13Column1Value" },
						{ "Column2", "Row13Column2Value" },
						{ "Column3", "Row13Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row14Column1Value" },
						{ "Column2", "Row14Column2Value" },
						{ "Column3", "Row14Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row15Column1Value" },
						{ "Column2", "Row15Column2Value" },
						{ "Column3", "Row15Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row16Column1Value" },
						{ "Column2", "Row16Column2Value" },
						{ "Column3", "Row16Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row17Column1Value" },
						{ "Column2", "Row17Column2Value" },
						{ "Column3", "Row17Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row18Column1Value" },
						{ "Column2", "Row18Column2Value" },
						{ "Column3", "Row18Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row19Column1Value" },
						{ "Column2", "Row19Column2Value" },
						{ "Column3", "Row19Column3Value" }
					},
					new Dictionary<string, object>
					{
						{ "Column1", "Row20Column1Value" },
						{ "Column2", "Row20Column2Value" },
						{ "Column3", "Row20Column3Value" }
					}
				};
			}
			catch (Exception exception)
			{
				if (debugLogger != null)
					await debugLogger.Error($"Error: {exception.GetDisplayMessage(": ")}");

				throw;
			}

			return Ok(resultsContainer);
		}

		// POST: api/Pages/DataGridPage/DataGridPage_Load_Database1_Query2
		[HttpPost]
		public IActionResult DataGridPage_Load_Database1_Query2(object parameters)
		{
			return Ok(new[]
				{
					new Dictionary<string, object>
					{
						{ "BooleanColumn", true },
						{ "NumberColumn", 1000.123456789 },
						{ "DateColumn", DateTime.Now.Date },
						{ "TimeColumn", DateTime.Now.TimeOfDay },
						{ "DateTimeColumn", DateTime.Now },
						{ "ArrayColumn", new[] { 1, 2, 3 } },
						{ "NullColumn", null }
					},
					new Dictionary<string, object>
					{
						{ "BooleanColumn", false },
						{ "NumberColumn", 1000000 },
						{ "DateColumn", DateTime.Now.Date },
						{ "TimeColumn", TimeSpan.FromSeconds(2.550) },
						{ "DateTimeColumn", DateTime.Now },
						{ "ArrayColumn", new[] { 1, 2, 3 } },
						{ "NullColumn", null }
					},
					new Dictionary<string, object>
					{
						{ "BooleanColumn", true },
						{ "NumberColumn", 0 },
						{ "DateColumn", DateTime.Now.Date },
						{ "TimeColumn", TimeSpan.FromSeconds(2) },
						{ "DateTimeColumn", DateTime.Now },
						{ "ArrayColumn", new[] { 1, 2, 3 } },
						{ "NullColumn", null }
					}
				});
		}

		// POST: api/Pages/DataGridPage/DataGridPage_Load_Database1_Query3
		[HttpPost]
		public async Task<IActionResult> DataGridPage_Load_Database1_Query3(object parameters)
		{
			IDebugLoggerClient debugLogger = null;
			if (AppSettingsHelper.GetInstance().GetAppSettings().Config.Debug)
			{
				string connectionId = this.valueResolver.ResolveParameter<string>((JObject)parameters, "connectionId");
				if (connectionId != null)
					debugLogger = this.debugLoggerHubContext.Clients.Client(connectionId);
			}

			object resultsContainer;

			try
			{
				var providerFactory = DbProviderFactories.GetFactory("System.Data.SqlClient");

				string commandText = @"IF OBJECT_ID('tempdb..#MyTable') IS NOT NULL DROP Table #MyTable

CREATE TABLE #MyTable(
 ID int,
 Column1 nvarchar(100),
 Column2 nvarchar(100),
 DateTime datetime)

 DECLARE @Id int
 SET @Id = 1

WHILE @Id <= 100
BEGIN 
   INSERT INTO #MyTable
	SELECT @Id,
			CONCAT('Column 1: ', @Id),
			CONCAT('Column 2: ', @Id),
			CURRENT_TIMESTAMP
   SET @Id = @Id + 1
END


SELECT * FROM #MyTable ORDER BY ID";

				int commandTimeout = 0;

				if (debugLogger != null)
					this.database.EnableDebugLogger(debugLogger);

				IQueryResult queryResult;

				try
				{
					queryResult = await this.database.ExecuteQuery(
										providerFactory.CreateConnection(),
										"Data Source=localhost;Initial Catalog=TestStadium5Solution;Integrated Security=False;User ID=ved;Password=10ataigiD",
										commandText,
										CommandType.Text,
										commandTimeout,
										new DbParameter[] { }
									);
				}
				catch (Exception exception)
				{
					throw new Exception("The query caused an error", exception);
				}

				resultsContainer = queryResult.ResultSet.Select(row => new Dictionary<string, object>
				{
					{ "ID", row[0] },
					{ "Column1", row[1] },
					{ "Column2", row[2] },
					{ "DateTime", row[3] }
				}).ToArray();
			}
			catch (Exception exception)
			{
				if (debugLogger != null)
					await debugLogger.Error($"Error: {exception.GetDisplayMessage(": ")}");

				throw;
			}

			return Ok(resultsContainer);
		}
	}
}
