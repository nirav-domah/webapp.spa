﻿using System;

namespace WebApp.Spa.Template.Areas.Documents
{
	[AttributeUsage(AttributeTargets.Class)]
	internal class TemplateAttribute : Attribute
	{
		public TemplateAttribute(params string[] pages)
		{
			Pages = pages;
		}

		public string[] Pages { get; }
	}
}
