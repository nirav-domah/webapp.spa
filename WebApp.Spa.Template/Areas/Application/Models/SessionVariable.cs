﻿namespace WebApp.Spa.Template.Areas.Application.Models
{
	public class SessionVariable
	{
		public string Name { get; set; }

		public string SerializedValue { get; set; }
	}
}
