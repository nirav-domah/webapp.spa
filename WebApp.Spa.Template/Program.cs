﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using System;
using WebApp.Spa.Administration;

namespace WebApp.Spa.Template
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var webHost = CreateWebHostBuilder(args).Build();

#if DEBUG
			// Uncomment to create administration.db file
			CreateAdministrationDbIfNotExists(webHost);

			IdentityModelEventSource.ShowPII = true;
#endif

			webHost.Run();
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args)
		{
			return WebHost.CreateDefaultBuilder(args)
							.ConfigureLogging(logging =>
							{
								logging.ClearProviders();
								logging.AddEventLog();
							})
							.UseStartup<Startup>()
							.UseKestrel(o => o.Limits.MaxRequestBodySize = null);
		}

		private static void CreateAdministrationDbIfNotExists(IWebHost webHost)
		{
			using (var scope = webHost.Services.CreateScope())
			{
				var services = scope.ServiceProvider;
				try
				{
					var administrationContext = services.GetRequiredService<AdministrationContext>();
					administrationContext.Database.EnsureCreated();
				}
				catch (Exception exception)
				{
					var logger = services.GetRequiredService<ILogger<Program>>();
					logger.LogError(exception, "An error occured when creating the administration.db.");
				}
			}
		}
	}
}
