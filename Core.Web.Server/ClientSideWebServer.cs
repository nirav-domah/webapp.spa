﻿using System.Threading;

namespace Core.Web.Server
{
	public class ClientSideWebServer : AWebServer
	{
		private Timer errorOutputTimer;
		private int port = 8081;

		protected override string ServerName => "Client-Side";

		protected override string ProcessFileName => "cmd";

		protected override string ProcessArguments => $"/c npm run serve -- --port {this.port}";

		protected override void SetUpServerProperties(string webAppPath)
		{
			this.port = TcpHelper.GetAvailablePort(preferredPort: 8081);
			HostedUrl = $"http://localhost:{this.port}/";
		}

		protected override void AttachAdditionalWatchers()
		{
			this.errorOutputTimer = new Timer(
				(_) => this.serverReadyWaitHandle?.Set(),
				null,
				Timeout.Infinite,
				Timeout.Infinite
			);
		}

		protected override void DisposeWatchers()
		{
			this.errorOutputTimer?.Dispose();
			this.errorOutputTimer = null;
		}

		protected override void HandleConsoleOutput(string line)
		{
			if (line.TrimStart().StartsWith("DONE  Compiled successfully"))
			{
				this.hasError = false;
				this.serverReadyWaitHandle?.Set();
			}

			if (line.Trim().StartsWith("ERROR"))
			{
				this.hasError = true;
			}

			this.errorOutputTimer.Change(3000, Timeout.Infinite);
		}
	}
}
