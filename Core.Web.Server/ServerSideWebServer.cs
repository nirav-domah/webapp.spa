﻿using Newtonsoft.Json;
using System.IO;

namespace Core.Web.Server
{
	public class ServerSideWebServer : AWebServer
	{
		protected override string ServerName => "Server-Side";

		protected override string ProcessFileName => "dotnet";

		protected override string ProcessArguments => "watch run";

		protected override void SetUpServerProperties(string webAppPath)
		{
			SetUpLaunchSettings(webAppPath);
		}

		protected override void HandleConsoleOutput(string line)
		{
			if (line.TrimStart().StartsWith("Application started"))
			{
				this.hasError = false;
				this.serverReadyWaitHandle?.Set();
			}

			if (line.Trim().StartsWith("watch : Waiting for a file to change before restarting dotnet"))
			{
				this.hasError = true;
				this.serverReadyWaitHandle?.Set();
			}
		}

		private void SetUpLaunchSettings(string webAppFolderPath)
		{
			string launchSettingsPath = Path.Combine(webAppFolderPath, "Properties", "launchSettings.json");
			string launchSettingsContent = File.ReadAllText(launchSettingsPath);
			dynamic launchSettings = JsonConvert.DeserializeObject(launchSettingsContent);

			int hostedPort = TcpHelper.GetAvailablePort();
			HostedUrl = $"http://localhost:{hostedPort}/";

			launchSettings.profiles.kestrel.applicationUrl = HostedUrl;

			string newLaunchSettingsContent = JsonConvert.SerializeObject(launchSettings);
			File.WriteAllText(launchSettingsPath, newLaunchSettingsContent);
		}
	}
}
