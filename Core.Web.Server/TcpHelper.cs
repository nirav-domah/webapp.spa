﻿using System.Net;
using System.Net.Sockets;

namespace Core.Web.Server
{
	public static class TcpHelper
	{
		public static int GetAvailablePort(int? preferredPort = null)
		{
			int ResolvePort(int port = 0)
			{
				var listener = new TcpListener(IPAddress.Loopback, port);
				listener.Start();
				int resolvedPort = ((IPEndPoint)listener.LocalEndpoint).Port;
				listener.Stop();
				return resolvedPort;
			}

			if (preferredPort.HasValue)
			{
				try
				{
					return ResolvePort(preferredPort.Value);
				}
				catch (SocketException) { }
			}

			return ResolvePort();
		}
	}
}
