﻿namespace Core.Web.Server
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var serverSideWebServer = new ServerSideWebServer();
			var clientSideWebServer = new ClientSideWebServer();

			try
			{
				try
				{
					clientSideWebServer.Start(@"C:\git\webapp.spa\WebApp.Spa.Template\ClientApp");
					string spaDevHostUrl = clientSideWebServer.HostedUrl;

					serverSideWebServer.Start(@"C:\git\webapp.spa\WebApp.Spa.Template");
					string hostUrl = serverSideWebServer.HostedUrl;

					WebHelper.Display(hostUrl);
				}
				finally
				{
					clientSideWebServer.Stop();
					serverSideWebServer.Stop();
				}
			}
			catch
			{ }
		}
	}
}
