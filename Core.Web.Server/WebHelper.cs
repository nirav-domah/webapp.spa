﻿using System;
using System.Diagnostics;

namespace Core.Web.Server
{
	public static class WebHelper
	{
		public static void Display(string uriString)
		{
			uriString = Uri.EscapeUriString(uriString);
			if (!Uri.IsWellFormedUriString(uriString, UriKind.Absolute))
				return;

			if (UacHelper.IsUacEnabled && UacHelper.IsProcessElevated)
			{
				var processStartInfo = new ProcessStartInfo("explorer.exe", $"\"{uriString}\"");
				processStartInfo.UseShellExecute = true;
				processStartInfo.Verb = "runas";
				Process.Start(processStartInfo);
			}
			else
			{
				Process.Start(uriString);
			}
		}
	}
}
