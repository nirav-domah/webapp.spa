﻿using System;
using System.Diagnostics;

namespace Core.Web.Server
{
	internal static class ProcessExtensions
	{
		public static void KillTree(this Process process)
		{
			var taskKillProcess = Process.Start(new ProcessStartInfo
			{
				FileName = "taskkill",
				Arguments = $"/T /F /PID {process.Id}",
				CreateNoWindow = true,
				UseShellExecute = false
			});

			int processExitTimeout = (int)TimeSpan.FromSeconds(5).TotalMilliseconds;

			if (!taskKillProcess.WaitForExit(processExitTimeout))
				taskKillProcess.Kill();

			if (!process.WaitForExit(processExitTimeout))
				process.Kill();
		}
	}
}
