﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Core.Web.Server
{
	public abstract class AWebServer
	{
		protected EventWaitHandle serverReadyWaitHandle;
		protected bool hasError;

		private string activeWebAppPath;
		private Process serverProcess;

		private readonly IList<string> outputLogs = new List<string>();
		private readonly IList<string> errorLogs = new List<string>();

		public bool IsRunning => this.activeWebAppPath != null;

		public string HostedUrl { get; protected set; }

		protected abstract string ServerName { get; }

		protected abstract string ProcessFileName { get; }

		protected abstract string ProcessArguments { get; }

		public void Start(string webAppPath)
		{
			this.hasError = false;

			if (webAppPath == this.activeWebAppPath)
				return;

			Stop();

			SetUpServerProperties(webAppPath);

			this.serverProcess = new Process
			{
				StartInfo = new ProcessStartInfo
				{
					FileName = ProcessFileName,
					Arguments = ProcessArguments,
					UseShellExecute = false,
					CreateNoWindow = true,
					WorkingDirectory = webAppPath,
					RedirectStandardOutput = true,
					RedirectStandardError = true
				}
			};

			this.serverProcess.OutputDataReceived += Process_OutputDataReceived;
			this.serverProcess.ErrorDataReceived += Process_ErrorDataReceived;

			this.serverReadyWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);

			this.serverProcess.Start();
			this.serverProcess.BeginOutputReadLine();

			AttachAdditionalWatchers();

			WaitUntilRunning();
			if (this.hasError)
			{
				foreach (string log in this.outputLogs.SkipWhile(l => !l.ToLower().Contains("error")))
				{
					this.errorLogs.Add(log);
				}

				throw new Exception($"Error starting {ServerName} server.\nDetails: {string.Join("\n", this.errorLogs)}");
			}

			this.activeWebAppPath = webAppPath;
		}

		public void Stop()
		{
			this.hasError = false;

			if (this.serverProcess != null)
			{
				this.serverProcess.OutputDataReceived -= Process_OutputDataReceived;
				this.serverProcess.ErrorDataReceived -= Process_ErrorDataReceived;
			}

			this.serverReadyWaitHandle?.Dispose();
			this.serverReadyWaitHandle = null;

			DisposeWatchers();

			if (IsRunning || this.serverProcess != null)
			{
				if (this.serverProcess != null && !this.serverProcess.HasExited)
				{
					this.serverProcess.KillTree();
					this.serverProcess = null;
				}

				this.activeWebAppPath = null;
			}
		}

		public void WaitUntilRunning()
		{
			if (!this.serverReadyWaitHandle.WaitOne(TimeSpan.FromSeconds(10)))
			{
				foreach (string log in this.outputLogs.SkipWhile(l => !l.ToLower().Contains("error")))
					this.errorLogs.Add(log);

				throw new Exception($"Timed out waiting for {ServerName} server.\nDetails: {string.Join("\n", this.errorLogs)}");
			}
		}

		protected abstract void SetUpServerProperties(string webAppPath);

		protected abstract void HandleConsoleOutput(string line);

		protected virtual void AttachAdditionalWatchers()
		{ }

		protected virtual void DisposeWatchers()
		{ }

		private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
		{
			if (!string.IsNullOrEmpty(e.Data))
			{
				this.outputLogs.Add(e.Data);

				HandleConsoleOutput(e.Data);
			}
		}

		private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
		{
			this.errorLogs.Add(e.Data);
		}
	}
}
