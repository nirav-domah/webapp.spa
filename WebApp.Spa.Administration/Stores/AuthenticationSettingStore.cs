﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Administration.Stores
{
	public class AuthenticationSettingStore : IAuthenticationSettingStore
	{
		private bool disposed;
		private readonly AdministrationContext administrationContext;

		public AuthenticationSettingStore(AdministrationContext administrationContext)
		{
			this.administrationContext = administrationContext;
		}

		public IQueryable<AuthenticationSetting> AuthenticationSettings => this.administrationContext.AuthenticationSettings;

		public void Dispose()
		{
			this.disposed = true;
		}

		public Task<AuthenticationSetting> FindByIdAsync(string authenticationSettingId, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.AuthenticationSettings
					.SingleOrDefaultAsync(s => s.Id == authenticationSettingId, cancellationToken);
		}

		public Task<AuthenticationSetting> FindByNameAsync(string authenticationSettingName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.AuthenticationSettings
					.SingleOrDefaultAsync(s => s.Name == authenticationSettingName, cancellationToken);
		}

		public async Task CreateAsync(AuthenticationSetting authenticationSetting, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (authenticationSetting == null)
				throw new ArgumentNullException(nameof(authenticationSetting));

			await this.administrationContext.AuthenticationSettings.AddAsync(authenticationSetting, cancellationToken);
			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task UpdateAsync(AuthenticationSetting authenticationSetting, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (authenticationSetting == null)
				throw new ArgumentNullException(nameof(authenticationSetting));

			this.administrationContext.Attach(authenticationSetting);
			this.administrationContext.Update(authenticationSetting);

			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task UpdateAllAsync(IEnumerable<AuthenticationSetting> authenticationSettings, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (authenticationSettings == null)
				throw new ArgumentNullException(nameof(authenticationSettings));

			this.administrationContext.AuthenticationSettings.RemoveRange(this.administrationContext.AuthenticationSettings);
			await this.administrationContext.AddRangeAsync(authenticationSettings, cancellationToken);

			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task RemoveRangeAsync(IEnumerable<AuthenticationSetting> authenticationSettings, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (authenticationSettings == null)
				throw new ArgumentNullException(nameof(authenticationSettings));

			this.administrationContext.AuthenticationSettings.RemoveRange(authenticationSettings);

			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		private void ThrowIfDisposed()
		{
			if (this.disposed)
				throw new ObjectDisposedException(GetType().Name);
		}
	}
}
