﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Administration.Interfaces;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Stores
{
	public class SettingStore : ISettingStore
	{
		private bool disposed;
		private readonly AdministrationContext administrationContext;

		public SettingStore(AdministrationContext administrationContext)
		{
			this.administrationContext = administrationContext;
		}

		public IQueryable<Setting> Settings => this.administrationContext.Settings;

		public void Dispose()
		{
			this.disposed = true;
		}

		public Task<Setting> FindByIdAsync(string settingId, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Settings
					.SingleOrDefaultAsync(s => s.Id == settingId, cancellationToken);
		}

		public Task<Setting> FindByNameAsync(string settingName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Settings
					.SingleOrDefaultAsync(s => s.Name == settingName, cancellationToken);
		}

		public async Task CreateAsync(Setting setting, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (setting == null)
				throw new ArgumentNullException(nameof(setting));

			await this.administrationContext.Settings.AddAsync(setting, cancellationToken);
			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task UpdateAsync(Setting setting, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (setting == null)
				throw new ArgumentNullException(nameof(setting));

			this.administrationContext.Attach(setting);
			this.administrationContext.Update(setting);

			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task RemoveRangeAsync(IEnumerable<Setting> settings, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (settings == null)
				throw new ArgumentNullException(nameof(settings));

			this.administrationContext.Settings.RemoveRange(settings);

			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		private void ThrowIfDisposed()
		{
			if (this.disposed)
				throw new ObjectDisposedException(GetType().Name);
		}
	}
}
