﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Administration.Interfaces;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Stores
{
	// Custom RoleStore implementation based upon:
	// https://github.com/dotnet/aspnetcore/blob/master/src/Identity/Extensions.Stores/src/RoleStoreBase.cs
	// https://github.com/dotnet/aspnetcore/blob/master/src/Identity/EntityFrameworkCore/src/RoleStore.cs
	public class RoleStore : IRoleStore
	{
		private bool disposed;
		private readonly AdministrationContext administrationContext;

		public RoleStore(AdministrationContext administrationContext)
		{
			this.administrationContext = administrationContext;
		}

		public IQueryable<Role> Roles => this.administrationContext.Roles
												.Include(r => r.UserRoles)
												.Include(r => r.PageRoles);

		public void Dispose()
		{
			this.disposed = true;
		}

		public async Task<IdentityResult> CreateAsync(Role role, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (role == null)
				throw new ArgumentNullException(nameof(role));

			await this.administrationContext.Roles.AddAsync(role, cancellationToken);
			await this.administrationContext.SaveChangesAsync(cancellationToken);

			return IdentityResult.Success;
		}

		public async Task<IdentityResult> UpdateAsync(Role role, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (role == null)
				throw new ArgumentNullException(nameof(role));

			this.administrationContext.Attach(role);
			this.administrationContext.Update(role);

			await this.administrationContext.SaveChangesAsync(cancellationToken);

			return IdentityResult.Success;
		}

		public async Task<IdentityResult> DeleteAsync(Role role, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (role == null)
				throw new ArgumentNullException(nameof(role));

			this.administrationContext.Roles.Remove(role);
			await this.administrationContext.SaveChangesAsync(cancellationToken);

			return IdentityResult.Success;
		}

		public Task<Role> FindByIdAsync(string roleId, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Roles.SingleOrDefaultAsync(r => r.Id == roleId, cancellationToken);
		}

		public Task<Role> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Roles.SingleOrDefaultAsync(r => r.NormalizedName == normalizedRoleName, cancellationToken);
		}

		public Task<string> GetRoleIdAsync(Role role, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (role == null)
				throw new ArgumentNullException(nameof(role));

			return Task.FromResult(role.Id);
		}

		public Task<string> GetRoleNameAsync(Role role, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (role == null)
				throw new ArgumentNullException(nameof(role));

			return Task.FromResult(role.Name);
		}

		public Task<string> GetNormalizedRoleNameAsync(Role role, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (role == null)
				throw new ArgumentNullException(nameof(role));

			return Task.FromResult(role.NormalizedName);
		}

		public Task SetRoleNameAsync(Role role, string roleName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (role == null)
				throw new ArgumentNullException(nameof(role));

			role.Name = roleName;
			return Task.CompletedTask;
		}

		public Task SetNormalizedRoleNameAsync(Role role, string normalizedRoleName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (role == null)
				throw new ArgumentNullException(nameof(role));

			role.NormalizedName = normalizedRoleName;
			return Task.CompletedTask;
		}

		private void ThrowIfDisposed()
		{
			if (this.disposed)
				throw new ObjectDisposedException(GetType().Name);
		}
	}
}
