﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Administration.Interfaces;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Stores
{
	// Custom UserStore implementation based upon:
	// https://github.com/dotnet/aspnetcore/blob/master/src/Identity/Extensions.Stores/src/UserStoreBase.cs
	// https://github.com/dotnet/aspnetcore/blob/master/src/Identity/EntityFrameworkCore/src/UserStore.cs
	public class UserStore : IUserStore
	{
		private bool disposed;
		private readonly AdministrationContext administrationContext;

		public UserStore(AdministrationContext administrationContext)
		{
			this.administrationContext = administrationContext;
		}

		public IQueryable<User> Users => this.administrationContext.Users.Include(u => u.UserRoles);

		public void Dispose()
		{
			this.disposed = true;
		}

		public async Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			await this.administrationContext.Users.AddAsync(user, cancellationToken);
			await this.administrationContext.SaveChangesAsync(cancellationToken);

			return IdentityResult.Success;
		}

		public async Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			this.administrationContext.Attach(user);
			this.administrationContext.Update(user);

			await this.administrationContext.SaveChangesAsync(cancellationToken);

			return IdentityResult.Success;
		}

		public async Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			this.administrationContext.Users.Remove(user);
			await this.administrationContext.SaveChangesAsync(cancellationToken);

			return IdentityResult.Success;
		}

		public Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Users
						.Include(u => u.UserRoles)
						.SingleOrDefaultAsync(u => u.Id == userId, cancellationToken);
		}

		public Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Users
						.Include(u => u.UserRoles)
						.SingleOrDefaultAsync(u => u.NormalizedUserName == normalizedUserName, cancellationToken);
		}

		public Task<User> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Users
						.Include(u => u.UserRoles)
						.SingleOrDefaultAsync(u => u.NormalizedEmail == normalizedEmail, cancellationToken);
		}

		public Task<User> FindByUniqueIdAsync(string uniqueId, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Users
						.Include(u => u.UserRoles)
						.SingleOrDefaultAsync(u => u.UniqueId == uniqueId, cancellationToken);
		}

		public Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			return Task.FromResult(user.Id);
		}

		public Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			return Task.FromResult(user.UserName);
		}

		public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			return Task.FromResult(user.NormalizedUserName);
		}

		public Task<string> GetEmailAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			return Task.FromResult(user.Email);
		}

		public Task<string> GetNormalizedEmailAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			return Task.FromResult(user.NormalizedEmail);
		}

		public Task<bool> GetEmailConfirmedAsync(User user, CancellationToken cancellationToken = default)
		{
			throw new NotImplementedException();
		}

		public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			return Task.FromResult(user.PasswordHash);
		}

		public Task<string> GetSecurityStampAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			return Task.FromResult(user.SecurityStamp);
		}

		public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			return Task.FromResult(user.PasswordHash != null);
		}

		public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			user.UserName = userName;
			return Task.CompletedTask;
		}

		public Task SetNormalizedUserNameAsync(User user, string normalizedUserName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			user.NormalizedUserName = normalizedUserName;
			return Task.CompletedTask;
		}

		public Task SetEmailAsync(User user, string email, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			user.Email = email;
			return Task.CompletedTask;
		}

		public Task SetNormalizedEmailAsync(User user, string normalizedEmail, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			user.NormalizedEmail = normalizedEmail;
			return Task.CompletedTask;
		}

		public Task SetEmailConfirmedAsync(User user, bool confirmed, CancellationToken cancellationToken = default)
		{
			throw new NotImplementedException();
		}

		public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			user.PasswordHash = passwordHash;
			return Task.CompletedTask;
		}

		public Task SetSecurityStampAsync(User user, string stamp, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			if (stamp == null)
				throw new ArgumentNullException(nameof(stamp));

			user.SecurityStamp = stamp;
			return Task.CompletedTask;
		}

		public async Task AddToRoleAsync(User user, string normalizedRoleName, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			if (string.IsNullOrWhiteSpace(normalizedRoleName))
				throw new ArgumentException("Value cannot be null or empty.", nameof(normalizedRoleName));

			var roleEntity = await FindRoleAsync(normalizedRoleName, cancellationToken);
			if (roleEntity == null)
				throw new InvalidOperationException($"Role {normalizedRoleName} does not exist.");

			var userEntity = await FindByIdAsync(user.Id, cancellationToken);
			if (userEntity == null)
				throw new InvalidOperationException($"User {user.NormalizedUserName} does not exist.");

			userEntity.UserRoles.Add(new UserRole()
			{
				UserId = userEntity.Id,
				RoleId = roleEntity.Id
			});

			await this.administrationContext.SaveChangesAsync();
		}

		public async Task RemoveFromRoleAsync(User user, string normalizedRoleName, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			if (string.IsNullOrWhiteSpace(normalizedRoleName))
				throw new ArgumentException("Value cannot be null or empty.", nameof(normalizedRoleName));

			var roleEntity = await FindRoleAsync(normalizedRoleName, cancellationToken);
			if (roleEntity != null)
			{
				var userRole = roleEntity.UserRoles.SingleOrDefault(ur => ur.UserId == user.Id);
				if (userRole != null)
				{
					roleEntity.UserRoles.Remove(userRole);
				}
			}
		}

		public async Task<IList<string>> GetRolesAsync(User user, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			var userEntity = await FindByIdAsync(user.Id, cancellationToken);
			if (userEntity != null)
			{
				var rolesIds = userEntity.UserRoles.Select(ur => ur.RoleId).ToHashSet();
				var roles = this.administrationContext.Roles
								.Include(r => r.UserRoles)
								.Where(r => rolesIds.Contains(r.Id))
								.Select(r => r.Name);

				return roles.ToList();
			}

			return new List<string>();
		}

		public async Task<bool> IsInRoleAsync(User user, string normalizedRoleName, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException(nameof(user));

			if (string.IsNullOrWhiteSpace(normalizedRoleName))
				throw new ArgumentException("Value cannot be null or empty.", nameof(normalizedRoleName));

			var roleEntity = await FindRoleAsync(normalizedRoleName, cancellationToken);
			if (roleEntity != null)
				return roleEntity.UserRoles.Any(ur => ur.UserId == user.Id);

			return false;
		}

		public async Task<IList<User>> GetUsersInRoleAsync(string normalizedRoleName, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (string.IsNullOrWhiteSpace(normalizedRoleName))
				throw new ArgumentException("Value cannot be null or empty.", nameof(normalizedRoleName));

			var roleEntity = await FindRoleAsync(normalizedRoleName, cancellationToken);
			if (roleEntity != null)
			{
				var userIds = roleEntity.UserRoles.Select(ur => ur.UserId).ToHashSet();
				var users = this.administrationContext.Users
								.Include(u => u.UserRoles)
								.Where(u => userIds.Contains(u.Id));

				return users.ToList();
			}

			return new List<User>();
		}

		private Task<Role> FindRoleAsync(string normalizedRoleName, CancellationToken cancellationToken)
		{
			return this.administrationContext.Roles
						.Include(r => r.UserRoles)
						.SingleOrDefaultAsync(r => r.NormalizedName == normalizedRoleName, cancellationToken);
		}

		private void ThrowIfDisposed()
		{
			if (this.disposed)
				throw new ObjectDisposedException(GetType().Name);
		}
	}
}
