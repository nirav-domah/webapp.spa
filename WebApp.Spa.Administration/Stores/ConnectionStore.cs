﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Administration.Interfaces;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Stores
{
	public class ConnectionStore : IConnectionStore
	{
		private bool disposed;
		private readonly AdministrationContext administrationContext;

		public ConnectionStore(AdministrationContext administrationContext)
		{
			this.administrationContext = administrationContext;
		}

		public IQueryable<Connection> Connections => this.administrationContext.Connections;

		public void Dispose()
		{
			this.disposed = true;
		}

		public Task<Connection> FindByIdAsync(string connectionId, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Connections
					.SingleOrDefaultAsync(s => s.Id == connectionId, cancellationToken);
		}

		public Task<Connection> FindByNameAsync(string connectionName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Connections
					.SingleOrDefaultAsync(s => s.Name == connectionName, cancellationToken);
		}

		public async Task CreateAsync(Connection connection, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (connection == null)
				throw new ArgumentNullException(nameof(connection));

			await this.administrationContext.Connections.AddAsync(connection, cancellationToken);
			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task UpdateAsync(Connection connection, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (connection == null)
				throw new ArgumentNullException(nameof(connection));

			this.administrationContext.Attach(connection);
			this.administrationContext.Update(connection);

			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task RemoveRangeAsync(IEnumerable<Connection> connections, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (connections == null)
				throw new ArgumentNullException(nameof(connections));

			this.administrationContext.Connections.RemoveRange(connections);

			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		private void ThrowIfDisposed()
		{
			if (this.disposed)
				throw new ObjectDisposedException(GetType().Name);
		}
	}
}
