﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Administration.Interfaces;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Stores
{
	public class PageStore : IPageStore
	{
		private bool disposed;
		private readonly AdministrationContext administrationContext;

		public PageStore(AdministrationContext administrationContext)
		{
			this.administrationContext = administrationContext;
		}

		public IQueryable<Page> Pages => this.administrationContext.Pages
												.Include(p => p.PageRoles);

		public void Dispose()
		{
			this.disposed = true;
		}

		public Task<Page> FindByIdAsync(string pageId, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Pages
					.Include(p => p.PageRoles)
					.SingleOrDefaultAsync(p => p.Id == pageId, cancellationToken);
		}

		public Task<Page> FindByNameAsync(string pageName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Pages
					.Include(p => p.PageRoles)
					.SingleOrDefaultAsync(p => p.Name == pageName, cancellationToken);
		}

		public Task<Page> FindStartPageAsync(CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			return this.administrationContext.Pages
					.Include(p => p.PageRoles)
					.SingleOrDefaultAsync(p => p.IsStartPage, cancellationToken);
		}

		public async Task CreateAsync(Page page, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (page == null)
				throw new ArgumentNullException(nameof(page));

			await this.administrationContext.Pages.AddAsync(page, cancellationToken);
			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task UpdateAsync(Page page, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (page == null)
				throw new ArgumentNullException(nameof(page));

			this.administrationContext.Attach(page);
			this.administrationContext.Update(page);

			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task RemoveRangeAsync(IEnumerable<Page> pages, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			if (pages == null)
				throw new ArgumentNullException(nameof(pages));

			this.administrationContext.Pages.RemoveRange(pages);

			await this.administrationContext.SaveChangesAsync(cancellationToken);
		}

		public async Task AddRoleToAllPagesAsync(string normalizedRoleName, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			var roleEntity = await FindRoleAsync(normalizedRoleName, cancellationToken);
			if (roleEntity == null)
				throw new InvalidOperationException($"Role {normalizedRoleName} does not exist.");

			var pages = await this.administrationContext.Pages
									.Include(p => p.PageRoles)
									.ToArrayAsync();

			foreach (var page in pages)
			{
				if (!page.PageRoles.Any(pr => pr.RoleId == roleEntity.Id))
				{
					roleEntity.PageRoles.Add(new PageRole
					{
						PageId = page.Id,
						RoleId = roleEntity.Id
					});
				}
			}

			this.administrationContext.Entry(roleEntity).State = EntityState.Modified;
		}

		public async Task AddRoleToPageAsync(string normalizedRoleName, Page page, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			var roleEntity = await FindRoleAsync(normalizedRoleName, cancellationToken);
			if (roleEntity == null)
				throw new InvalidOperationException($"Role {normalizedRoleName} does not exist.");

			if (!page.PageRoles.Any(pr => pr.RoleId == roleEntity.Id))
			{
				roleEntity.PageRoles.Add(new PageRole
				{
					PageId = page.Id,
					RoleId = roleEntity.Id
				});

				this.administrationContext.Entry(roleEntity).State = EntityState.Modified;
			}
		}

		public async Task RemoveRoleFromPageAsync(string normalizedRoleName, Page page, CancellationToken cancellationToken = default)
		{
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			var roleEntity = await FindRoleAsync(normalizedRoleName, cancellationToken);
			if (roleEntity == null)
				throw new InvalidOperationException($"Role {normalizedRoleName} does not exist.");

			var pageRole = roleEntity.PageRoles.SingleOrDefault(pr => pr.PageId == page.Id);
			if (pageRole != null)
			{
				roleEntity.PageRoles.Remove(pageRole);

				this.administrationContext.Entry(roleEntity).State = EntityState.Modified;
			}
		}

		private Task<Role> FindRoleAsync(string normalizedRoleName, CancellationToken cancellationToken)
		{
			return this.administrationContext.Roles
						.Include(r => r.PageRoles)
						.SingleOrDefaultAsync(r => r.NormalizedName == normalizedRoleName, cancellationToken);
		}

		private void ThrowIfDisposed()
		{
			if (this.disposed)
				throw new ObjectDisposedException(GetType().Name);
		}
	}
}
