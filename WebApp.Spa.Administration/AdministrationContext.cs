﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration
{
	public class AdministrationContext : DbContext
	{
		public const string ConnectionStringName = "AdministrationDbConnection";

		private readonly ILookupNormalizer keyNormalizer;
		private readonly IPasswordHasher<User> passwordHasher;

		public AdministrationContext(DbContextOptions<AdministrationContext> options, ILookupNormalizer keyNormalizer, IPasswordHasher<User> passwordHasher)
			: base(options)
		{
			this.keyNormalizer = keyNormalizer;
			this.passwordHasher = passwordHasher;
		}

		public Application Application => Applications.Single();

		public DbSet<Application> Applications { get; set; }
		public DbSet<ApplicationUpdate> ApplicationUpdates { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<Page> Pages { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<AccessLookupEntry> AccessLookupEntries { get; set; }
		public DbSet<Connection> Connections { get; set; }
		public DbSet<Setting> Settings { get; set; }
		public DbSet<AuthenticationSetting> AuthenticationSettings { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			// User Role many to many
			modelBuilder.Entity<UserRole>()
				.HasKey(ur => new { ur.UserId, ur.RoleId });

			modelBuilder.Entity<UserRole>()
				.HasOne(ur => ur.User)
				.WithMany(u => u.UserRoles)
				.HasForeignKey(ur => ur.UserId);

			modelBuilder.Entity<UserRole>()
				.HasOne(ur => ur.Role)
				.WithMany(r => r.UserRoles)
				.HasForeignKey(ur => ur.RoleId);

			// Page Role many to many
			modelBuilder.Entity<PageRole>()
				.HasKey(pr => new { pr.PageId, pr.RoleId });

			modelBuilder.Entity<PageRole>()
				.HasOne(pr => pr.Page)
				.WithMany(p => p.PageRoles)
				.HasForeignKey(pr => pr.PageId);

			modelBuilder.Entity<PageRole>()
				.HasOne(pr => pr.Role)
				.WithMany(r => r.PageRoles)
				.HasForeignKey(pr => pr.RoleId);

			// Unique Index for User UserName
			modelBuilder.Entity<User>()
				.HasIndex(u => u.UserName)
				.IsUnique();

			// Unique Index for User Email
			modelBuilder.Entity<User>()
				.HasIndex(u => u.Email)
				.IsUnique();

			// Seed User Role
			var userRole = new Role(Role.DefaultRoleGuid, Role.DefaultRoleName)
			{
				NormalizedName = this.keyNormalizer.Normalize(Role.DefaultRoleName)
			};
			modelBuilder.Entity<Role>()
				.HasData(userRole);

			//----------------------Only for prototyping---------------------------

			// Seed Preview Role
			const string previewRoleName = "Preview";
			var previewRole = new Role(previewRoleName)
			{
				NormalizedName = this.keyNormalizer.Normalize(previewRoleName)
			};
			modelBuilder.Entity<Role>()
				.HasData(previewRole);

			// Seed Pages
			const string startPageName = "StartPage";
			string[] pageNames = new[]
			{
				"DataGridPage",
				"LabelPage",
				"LayoutGridPage",
				"LayoutTablePage",
				"LinkPage",
				"Page1",
				"PanelPage",
				"StartPage",
				"TextBoxPage"
			};

			var pages = pageNames.Select(n => new Page(Guid.NewGuid().ToString(), n, n == startPageName)).ToArray();
			modelBuilder.Entity<Page>()
				.HasData(pages);

			var startPage = pages.Single(p => p.IsStartPage);
			var pageRoles = pages.Select(p => new PageRole()
			{
				PageId = p.Id,
				RoleId = previewRole.Id
			})
				.Concat(new[] {
					new PageRole
					{
						PageId = startPage.Id,
						RoleId = userRole.Id
					}
				});

			modelBuilder.Entity<PageRole>()
				.HasData(pageRoles);

			// Seed OIDC Authentication Settings
			var oidcSettings = new[]
			{
				new AuthenticationSetting(AuthenticationSetting.RedirectUrl, "http://localhost:49764/callback"),
				new AuthenticationSetting(AuthenticationSetting.LogoutRedirectUrl, "http://localhost:49764/logout"),

				//new AuthenticationSetting(AuthenticationSetting.OidcProvider, "Okta"),
				//new AuthenticationSetting(AuthenticationSetting.AuthenticationAuthorityUri, "https://dev-33216961.okta.com/oauth2/default"),
				//new AuthenticationSetting(AuthenticationSetting.Tenant, null),
				//new AuthenticationSetting(AuthenticationSetting.ClientID, "0oa3dlr8bldrmJnOG5d7"),
				//new AuthenticationSetting(AuthenticationSetting.RoleClaimName, "groups")

				//new AuthenticationSetting(AuthenticationSetting.OidcProvider, "Google"),
				//new AuthenticationSetting(AuthenticationSetting.AuthenticationAuthorityUri, "https://accounts.google.com"),
				//new AuthenticationSetting(AuthenticationSetting.Tenant, null),
				//new AuthenticationSetting(AuthenticationSetting.ClientID, "13503462809-l2c3dte33nupdnp5h371t7hd7hts42hd.apps.googleusercontent.com"),
				//new AuthenticationSetting(AuthenticationSetting.RoleClaimName, null)

				//new AuthenticationSetting(AuthenticationSetting.OidcProvider, "Auth0"),
				//new AuthenticationSetting(AuthenticationSetting.AuthenticationAuthorityUri, "https://dev-ho47ravm.us.auth0.com"),
				//new AuthenticationSetting(AuthenticationSetting.Tenant, null),
				//new AuthenticationSetting(AuthenticationSetting.ClientID, "TFmbMyHXyr1lVrEjfU96BcgT3h9nL7mI"),
				//new AuthenticationSetting(AuthenticationSetting.RoleClaimName, "https://schemas.quickstarts.com/roles")

				//new AuthenticationSetting(AuthenticationSetting.OidcProvider, "AzureAD"),
				//new AuthenticationSetting(AuthenticationSetting.AuthenticationAuthorityUri, null),
				//new AuthenticationSetting(AuthenticationSetting.Tenant, "b5468f69-5e50-4262-a0fc-3d4f1d68a678"),
				//new AuthenticationSetting(AuthenticationSetting.ClientID, "0a7fd3bb-57f2-41d5-a57b-a637455f58c2"),
				//new AuthenticationSetting(AuthenticationSetting.RoleClaimName, null)

				new AuthenticationSetting(AuthenticationSetting.OidcProvider, "GenericProvider"),
				new AuthenticationSetting(AuthenticationSetting.AuthenticationAuthorityUri, "https://localhost:8443/StadiumTestIdentityServer"),
				new AuthenticationSetting(AuthenticationSetting.Tenant, null),
				new AuthenticationSetting(AuthenticationSetting.ClientID, "stadiumApp"),
				new AuthenticationSetting(AuthenticationSetting.ApiResourceName, "stadiumApp"),
				new AuthenticationSetting(AuthenticationSetting.ApiResourceSecret, "secret"),
				new AuthenticationSetting(AuthenticationSetting.RoleClaimName, ClaimTypes.Role),
				new AuthenticationSetting(AuthenticationSetting.Scopes, "openid profile email stadiumApp")
			};

			modelBuilder.Entity<AuthenticationSetting>()
				.HasData(oidcSettings);

			// seed admin user
			const string adminUserEmail = "admin@test.com";
			const string adminUserPassword = "admin";
			var adminUser = new User(adminUserEmail)
			{
				Email = adminUserEmail,
				IsAdministrator = true,
				PasswordHash = this.passwordHasher.HashPassword(null, adminUserPassword),
				NormalizedEmail = this.keyNormalizer.Normalize(adminUserEmail),
				NormalizedUserName = this.keyNormalizer.Normalize(adminUserEmail)
			};
			modelBuilder.Entity<User>()
				.HasData(adminUser);

			modelBuilder.Entity<UserRole>()
				.HasData(new UserRole
				{
					UserId = adminUser.Id,
					RoleId = userRole.Id
				});
		}
	}
}
