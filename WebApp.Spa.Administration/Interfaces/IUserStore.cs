﻿using Microsoft.AspNetCore.Identity;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Interfaces
{
	public interface IUserStore :
		IQueryableUserStore<User>,
		IUserEmailStore<User>,
		IUserPasswordStore<User>,
		IUserRoleStore<User>,
		IUserSecurityStampStore<User>
	{
		Task<User> FindByUniqueIdAsync(string uniqueId, CancellationToken cancellationToken = default);
	}
}
