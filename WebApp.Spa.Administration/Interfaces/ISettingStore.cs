﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Interfaces
{
	public interface ISettingStore : IDisposable
	{
		IQueryable<Setting> Settings { get; }

		Task<Setting> FindByIdAsync(string settingId, CancellationToken cancellationToken = default);

		Task<Setting> FindByNameAsync(string settingName, CancellationToken cancellationToken = default);

		Task CreateAsync(Setting setting, CancellationToken cancellationToken = default);

		Task UpdateAsync(Setting setting, CancellationToken cancellationToken = default);

		Task RemoveRangeAsync(IEnumerable<Setting> settings, CancellationToken cancellationToken = default);
	}
}
