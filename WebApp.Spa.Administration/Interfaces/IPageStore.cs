﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Interfaces
{
	public interface IPageStore : IDisposable
	{
		IQueryable<Page> Pages { get; }

		Task<Page> FindByIdAsync(string pageId, CancellationToken cancellationToken = default);

		Task<Page> FindByNameAsync(string pageName, CancellationToken cancellationToken = default);

		Task<Page> FindStartPageAsync(CancellationToken cancellationToken = default);

		Task CreateAsync(Page page, CancellationToken cancellationToken = default);

		Task UpdateAsync(Page page, CancellationToken cancellationToken = default);

		Task RemoveRangeAsync(IEnumerable<Page> pages, CancellationToken cancellationToken = default);

		Task AddRoleToAllPagesAsync(string normalizedRoleName, CancellationToken cancellationToken = default);

		Task AddRoleToPageAsync(string normalizedRoleName, Page page, CancellationToken cancellationToken = default);

		Task RemoveRoleFromPageAsync(string normalizedRoleName, Page page, CancellationToken cancellationToken = default);
	}
}
