﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Interfaces
{
	public interface IConnectionStore : IDisposable
	{
		IQueryable<Connection> Connections { get; }

		Task<Connection> FindByIdAsync(string connectionId, CancellationToken cancellationToken = default);

		Task<Connection> FindByNameAsync(string connectionName, CancellationToken cancellationToken = default);

		Task CreateAsync(Connection connection, CancellationToken cancellationToken = default);

		Task UpdateAsync(Connection connection, CancellationToken cancellationToken = default);

		Task RemoveRangeAsync(IEnumerable<Connection> connections, CancellationToken cancellationToken = default);
	}
}
