﻿using Microsoft.AspNetCore.Identity;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Administration.Interfaces
{
	public interface IRoleStore : IQueryableRoleStore<Role>, IRoleStore<Role>
	{ }
}
