﻿/********************************************************
 Name: Bradford Foxworth-Hill
 Email: Brad.Hill@acstechnologies.com
 Alt Email: Assiance@aol.com
 ********************************************************/

namespace WebDriverSEd.Extensions
{
	public static class StringExtensions
	{
		public static bool IsNullOrEmpty(this string target)
		{
			return string.IsNullOrEmpty(target);
		}

		public static string RemoveLineBreaks(this string theString)
		{
			return theString.Replace("\r", string.Empty).Replace("\n", " ").Replace("<strong>", string.Empty).Replace("</strong>", string.Empty);
		}
	}
}
