﻿/********************************************************
 Name: Bradford Foxworth-Hill
 Email: Brad.Hill@acstechnologies.com
 Alt Email: Assiance@aol.com
 ********************************************************/
using OpenQA.Selenium;
using System;

namespace WebDriverSEd.ElementTypes
{
	public class TableHeadSe : TableElements
	{
		private static readonly string columnTag = "th";

		public TableHeadSe(IWebDriver webDriver, By by)
			: base(webDriver, by)
		{
			InitializeRowsandCells(columnTag);
		}

		public TableHeadSe(IWebElement webElement, By by)
			: base(webElement, by)
		{
			InitializeRowsandCells(columnTag);
		}

		public TableHeadSe(IWebDriver webDriver, By by, Func<ElementSe, bool> predicate)
			: base(webDriver, by, predicate)
		{
			InitializeRowsandCells(columnTag);
		}

		public TableHeadSe(IWebElement webElement, By by, Func<ElementSe, bool> predicate)
			: base(webElement, by, predicate)
		{
			InitializeRowsandCells(columnTag);
		}

		public TableHeadSe(IWebElement body)
			: base(body, columnTag)
		{
		}

		public override string ElementTag
		{
			get { return "thead"; }
		}
	}
}
