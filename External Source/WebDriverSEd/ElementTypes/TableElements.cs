﻿/********************************************************
 Name: Bradford Foxworth-Hill
 Email: Brad.Hill@acstechnologies.com
 Alt Email: Assiance@aol.com
 ********************************************************/
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using WebDriverSEd.Entities.Args;
using WebDriverSEd.Extensions;

namespace WebDriverSEd.ElementTypes
{
	public abstract class TableElements : ElementSe
	{
		private readonly List<TableRowSe> _rows = new List<TableRowSe>();
		private readonly List<TableCellSe> _cells = new List<TableCellSe>();

		public TableElements(IWebDriver webDriver, By by)
			: base(webDriver, by)
		{
		}

		public TableElements(IWebElement webElement, By by)
			: base(webElement, by)
		{
		}

		public TableElements(IWebDriver webDriver, By by, Func<ElementSe, bool> predicate)
			: base(webDriver, by, predicate)
		{
		}

		public TableElements(IWebElement webElement, By by, Func<ElementSe, bool> predicate)
			: base(webElement, by, predicate)
		{
		}

		public TableElements(IWebElement body, string columnTag)
			: base(body)
		{
			var theRows = new TableRowSeCollection(WebElement, By.TagName(columnTag));

			foreach (var row in theRows)
			{
				var temp = new TableRowSe(row, columnTag);

				this._rows.Add(temp);
			}

			foreach (var row in this._rows)
			{
				foreach (var cell in row.Cells)
				{
					this._cells.Add(cell);
				}
			}
		}

		public override string ElementTag
		{
			get { return "tbody"; }
		}

		public List<TableRowSe> Rows
		{
			get
			{
				return this._rows;
			}
		}

		public List<TableCellSe> Cells
		{
			get
			{
				return this._cells;
			}
		}

		public TableRowSe FindRow(string key)
		{
			return Rows.Find(i => i.Text.RemoveLineBreaks().Contains(key));
		}

		public TableRowSe FindRow(string key, int keyColumn)
		{
			return Rows.Find(i => i.Cells[keyColumn].Text.RemoveLineBreaks().Contains(key));
		}

		public TableRowSe FindRow(FindRow findRow)
		{
			return Rows.Find(i => i.Cells[findRow.KeyColumn].Text.RemoveLineBreaks().Contains(findRow.Key));
		}

		public TableRowSe FindRow(Predicate<TableRowSe> predicate)
		{
			return Rows.Find(predicate);
		}

		public TableRowSe GetRow(int targetRow)
		{
			return Rows[targetRow];
		}

		public T GetTableElement<T>(FindRow findRow, string targetCellText) where T : ElementSe
		{
			var row = FindRow(findRow);
			var cell = row.Cells.Find(i => i.Text.Contains(targetCellText));
			string tag = new ElementSe(cell).ConvertTo<T>().ElementTag;
			var element = new ElementSe(cell, By.TagName(tag));
			return element.ConvertTo<T>();
		}

		public T GetTableElement<T>(FindRow findRow, int targetCell) where T : ElementSe
		{
			var row = FindRow(findRow);
			string tag = new ElementSe(row).ConvertTo<T>().ElementTag;
			var element = new ElementSe(row.Cells[targetCell], By.TagName(tag));
			return element.ConvertTo<T>();
		}

		public List<List<string>> GetRowText()
		{
			var tableValues = new List<List<string>>();

			foreach (var row in Rows)
			{
				if (row.Style.ToLower() != "none")
				{
					var rowValues = new List<string>();
					var cells = row.Cells;
					foreach (var cell in cells)
					{
						rowValues.Add(cell.Text.RemoveLineBreaks());
					}

					tableValues.Add(rowValues);
				}
			}

			return tableValues;
		}

		public List<string> GetCommaSeparatedTableRowText()
		{
			var tableValues = new List<string>();

			foreach (var row in Rows)
			{
				if (row.Style.ToLower() != "none")
				{
					var sb = new StringBuilder();
					foreach (var cell in row.Cells)
					{
						sb.AppendFormat("{0}, ", cell.Text);
					}

					string s = sb.ToString().Trim().Trim(',');
					tableValues.Add(s);
				}
			}

			return tableValues;
		}

		public List<string> GetAllValuesFromColumn(int columnIndex)
		{
			var values = new List<string>();

			foreach (var row in Rows)
			{
				if (row.Style.ToLower() != "none")
				{
					var cell = row.Cells[columnIndex];
					if (!string.IsNullOrEmpty(cell.Text))
					{
						values.Add(cell.Text);
					}
				}
			}

			return values;
		}

		public List<string> GetAllValuesFromColumn(int columnIndex, string className)
		{
			var values = new List<string>();

			foreach (var row in Rows)
			{
				if (row.Style.ToLower() != "none")
				{
					var cell = row.Cells[columnIndex];
					var element = cell.Div(e => e.ClassName == className).ConvertTo<ElementSe>();
					if (element.Exists)
					{
						string s = element.Text;
						if (!string.IsNullOrEmpty(s))
						{
							values.Add(s);
						}
					}
				}
			}

			return values;
		}

		protected void InitializeRowsandCells(string columnTag)
		{
			var theRows = new TableRowSeCollection(WebElement, By.TagName("tr"));
			foreach (var row in theRows)
			{
				this._rows.Add(new TableRowSe(row, columnTag));
			}

			foreach (var row in this._rows)
			{
				foreach (var cell in row.Cells)
				{
					this._cells.Add(cell);
				}
			}
		}
	}
}