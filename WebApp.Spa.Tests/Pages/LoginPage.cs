﻿using OpenQA.Selenium;
using WebApp.Spa.Tests.Fixture;
using WebDriverSEd.ElementTypes;

namespace WebApp.Spa.Tests.Pages
{
	public class LoginPage : AWebPage
	{
		public LoginPage(IWebDriver driver)
			: base(driver)
		{ }

		public override string PageName => "Login";

		public bool IsDisplayed
		{
			get
			{
				try
				{
					return LoginButton.Displayed;
				}
				catch (NoSuchElementException)
				{
					return false;
				}
			}
		}

		public string Email
		{
			get => EmailField.Value;
			set => EmailField.ClearFirstSendKeys(value);
		}

		public string Password
		{
			get => PasswordField.Value;
			set => PasswordField.ClearFirstSendKeys(value);
		}

		private TextFieldSe EmailField => Get<TextFieldSe>("email", fullID: true);

		private TextFieldSe PasswordField => Get<TextFieldSe>("password", fullID: true);

		private ButtonSe LoginButton => Get<ButtonSe>("login-button", fullID: true);

		public void Login(string email, string password)
		{
			Driver.WaitUntil(() => IsDisplayed);

			Email = email;
			Password = password;

			string url = Driver.Url;
			LoginButton.ClickAndWaitUntil(() => Driver.Url != url);

			Driver.WaitForPageToLoad();
		}
	}
}
