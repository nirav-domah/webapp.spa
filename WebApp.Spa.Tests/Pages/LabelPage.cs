﻿using OpenQA.Selenium;
using WebApp.Spa.Tests.Fixture;
using WebDriverSEd.ElementTypes;

namespace WebApp.Spa.Tests.Pages
{
	public class LabelPage : AWebPage
	{
		public LabelPage(IWebDriver driver)
			: base(driver)
		{ }

		public override string PageName => "LabelPage";

		public string ResultLabelText => ResultLabel.Text;

		public string TextLabelText => TextLabel.Text;

		public bool VisibleLabelVisible => IsVisible(() => VisibleLabel);

		private LabelSe ResultLabel => Get<LabelSe>("ResultLabel");

		private LabelSe TextLabel => Get<LabelSe>("TextLabel");

		private LabelSe VisibleLabel => Get<LabelSe>("VisibleLabel");

		private ButtonSe GetLabelTextButton => Get<ButtonSe>("GetLabelTextButton");

		private ButtonSe SetLabelTextButton => Get<ButtonSe>("SetLabelTextButton");

		private ButtonSe GetLabelVisibleButton => Get<ButtonSe>("GetLabelVisibleButton");

		private ButtonSe SetLabelVisibleButton => Get<ButtonSe>("SetLabelVisibleButton");

		public void ClickGetLabelTextButton()
		{
			GetLabelTextButton.ClickAndWait();
		}

		public void ClickSetLabelTextButton()
		{
			SetLabelTextButton.ClickAndWait();
		}

		public void ClickGetLabelVisibleButton()
		{
			GetLabelVisibleButton.ClickAndWait();
		}

		public void ClickSetLabelVisibleButton()
		{
			SetLabelVisibleButton.ClickAndWait();
		}
	}
}
