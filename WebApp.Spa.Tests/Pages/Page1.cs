﻿using OpenQA.Selenium;
using WebDriverSEd.ElementTypes;

namespace WebApp.Spa.Tests.Pages
{
	public class Page1 : AWebPage
	{
		public Page1(IWebDriver driver)
			: base(driver)
		{ }

		public override string PageName => "Page1";

		public string DefaultTemplateLoadLabelText => DefaultTemplateLoadLabel.Text;

		public string LoadLabelText => LoadLabel.Text;

		public string SettingsLabelText => SettingLabel.Text;

		private LabelSe DefaultTemplateLoadLabel => Get<LabelSe>("DefaultTemplate_LoadLabel", fullID: true);

		private LabelSe LoadLabel => Get<LabelSe>("LoadLabel");

		private LabelSe SettingLabel => Get<LabelSe>("SettingsLabel");
	}
}
