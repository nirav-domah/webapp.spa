﻿using OpenQA.Selenium;
using WebApp.Spa.Tests.Fixture;
using WebDriverSEd.ElementTypes;

namespace WebApp.Spa.Tests.Pages
{
	public class PageNotFoundErrorPage : AWebPage
	{
		public PageNotFoundErrorPage(IWebDriver driver)
			: base(driver)
		{ }

		public bool IsDisplayed => IsVisible(() => HeaderField);

		public string HeaderText => HeaderField.Text;

		private LabelSe HeaderField => Get<LabelSe>("error-page-header", fullID: true);

		private LinkSe BackToApplicationLink => Get<LinkSe>(By.CssSelector(".error-content-container .back-button"));

		public void ClickBackToApplicationButton()
		{
			BackToApplicationLink.ClickAndWait();
		}
	}
}
