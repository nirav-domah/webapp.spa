﻿using OpenQA.Selenium;
using WebApp.Spa.Tests.Fixture;
using WebDriverSEd.ElementTypes;

namespace WebApp.Spa.Tests.Pages
{
	public class TextBoxPage : AWebPage
	{
		public TextBoxPage(IWebDriver driver)
			: base(driver)
		{ }

		public override string PageName => "TextBoxPage";

		public string ResultLabelText => ResultLabel.Text;

		public string TextTextBoxText
		{
			get => TextTextBox.Value;
			set => TextTextBox.ClearFirstSendKeys(value);
		}

		private LabelSe ResultLabel => Get<LabelSe>("ResultLabel");

		private TextFieldSe TextTextBox => Get<TextFieldSe>("TextTextBox");

		private ButtonSe GetTextBoxTextButton => Get<ButtonSe>("GetTextBoxTextButton");

		private ButtonSe SetTextBoxTextButton => Get<ButtonSe>("SetTextBoxTextButton");

		public void ClickGetTextBoxTextButton()
		{
			GetTextBoxTextButton.ClickAndWait();
		}

		public void ClickSetLabelTextButton()
		{
			SetTextBoxTextButton.ClickAndWait();
		}
	}
}
