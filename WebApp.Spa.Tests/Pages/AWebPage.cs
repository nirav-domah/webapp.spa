﻿using OpenQA.Selenium;
using System;
using WebDriverSEd.ElementTypes;

namespace WebApp.Spa.Tests.Pages
{
	public abstract class AWebPage
	{
		protected AWebPage(IWebDriver driver)
		{
			Driver = driver;
		}

		public virtual string PageName
		{
			get
			{
				string typeName = GetType().Name;
				return typeName.Remove(typeName.Length - "Page".Length);
			}
		}

		protected IWebDriver Driver { get; }

		protected T Get<T>(string id, bool fullID = false)
			where T : ElementSe
		{
			return Get<T>(By.Id(ResolveControlID(id, fullID)));
		}

		protected T Get<T>(By by) where T : ElementSe
		{
			return (T)typeof(T).GetConstructor(new[] { typeof(IWebElement) })
								.Invoke(new[] { Driver.FindElement(by) });
		}

		protected bool IsVisible<T>(Func<T> getElement)
			where T : IWebElement
		{
			try
			{
				return getElement().Displayed;
			}
			catch (NoSuchElementException)
			{
				return false;
			}
			catch (StaleElementReferenceException)
			{
				return false;
			}
		}

		private string ResolveControlID(string controlID, bool fullID = false)
		{
			return fullID ? controlID : $"{PageName}_{controlID}";
		}
	}
}
