﻿using Microsoft.Extensions.Configuration;
using System;
using WebApp.Spa.Core.Enums;

namespace WebApp.Spa.Tests.Helpers
{
	public class ConfigHelper
	{
		private readonly IConfigurationSection configSection;

		private ConfigHelper(string configBasePath)
		{
			IConfiguration configuration = new ConfigurationBuilder()
											.SetBasePath(configBasePath)
											.AddJsonFile("appsettings.json")
											.Build();

			this.configSection = configuration.GetSection("Config");
		}

		public static ConfigHelper GetInstance(string configBasePath)
		{
			return new ConfigHelper(configBasePath);
		}

		public AuthenticationType GetAuthenticationType()
		{
			return Enum.TryParse(this.configSection["AuthenticationType"], out AuthenticationType authenticationType)
				? authenticationType
				: default;
		}
	}
}
