﻿using Microsoft.Web.Administration;
using System;
using System.Diagnostics;
using System.Linq;

namespace WebApp.Spa.Tests.Helpers
{
	public static class IISHelper
	{
		private const string Twenty57StadiumSite = "Twenty57 Stadium";
		private const string RelativePublishFolderPath = @"\bin\Release\net48\publish";

		public static void Publish(string applicationName, string applicationPath)
		{
			string publishFolderPath = $@"{applicationPath}{RelativePublishFolderPath}";

			using (var serverManager = new ServerManager())
			{
				var applicationPool = serverManager.ApplicationPools[applicationName];
				if (applicationPool != null)
				{
					if (applicationPool.State == ObjectState.Started)
						applicationPool.Stop();

					RunDotNetPublish(applicationPath);

					var site = serverManager.Sites[Twenty57StadiumSite];
					var application = site.Applications[$"/{applicationName}"];

					var virtualDirectory = application.VirtualDirectories.Single();
					if (virtualDirectory.PhysicalPath != publishFolderPath)
					{
						virtualDirectory.PhysicalPath = publishFolderPath;
						serverManager.CommitChanges();
					}

					applicationPool.Start();
				}
				else
				{
					RunDotNetPublish(applicationPath);

					applicationPool = serverManager.ApplicationPools.Add(applicationName);
					applicationPool.ManagedRuntimeVersion = string.Empty;
					applicationPool.Enable32BitAppOnWin64 = false;
					applicationPool.ProcessModel.IdentityType = ProcessModelIdentityType.ApplicationPoolIdentity;
					applicationPool.ProcessModel.LoadUserProfile = true;

					serverManager.CommitChanges();

					var site = serverManager.Sites[Twenty57StadiumSite];
					var application = site.Applications.Add($"/{applicationName}", $@"{applicationPath}{RelativePublishFolderPath}");
					application.ApplicationPoolName = applicationPool.Name;

					serverManager.CommitChanges();
				}
			}
		}

		private static void RunDotNetPublish(string applicationPath)
		{
			var process = new Process
			{
				StartInfo =
				{
					FileName = "dotnet",
					Arguments = $@"publish ""{applicationPath}"" --configuration Release -p:PublishProfile=FolderProfile",
					UseShellExecute = false
				}
			};

			process.Start();
			process.WaitForExit((int)TimeSpan.FromSeconds(20).TotalMilliseconds);
		}
	}
}
