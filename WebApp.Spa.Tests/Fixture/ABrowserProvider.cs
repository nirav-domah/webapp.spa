﻿using OpenQA.Selenium;
using System;
using System.Runtime.CompilerServices;
using System.Timers;

namespace WebApp.Spa.Tests.Fixture
{
	public abstract class ABrowserProvider
	{
		private IWebDriver browser;
		private readonly Timer checkIsBrowserClosedTimer;

		protected ABrowserProvider()
		{
			this.checkIsBrowserClosedTimer = new Timer(500);
			this.checkIsBrowserClosedTimer.Elapsed += (sender, e) => DisposeBrowserIfClosed();
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public IWebDriver GetBrowser()
		{
			DisposeBrowserIfClosed();

			if (this.browser == null)
			{
				try
				{
					this.browser = CreateBrowser();
					this.checkIsBrowserClosedTimer.Enabled = true;
				}
				finally
				{
					if (this.browser == null)
						Closed?.Invoke(this, EventArgs.Empty);
				}
			}

			return this.browser;
		}


		[MethodImpl(MethodImplOptions.Synchronized)]
		public void Close()
		{
			this.checkIsBrowserClosedTimer.Enabled = false;

			if (this.browser != null)
			{
				this.browser.Dispose();
				this.browser = null;
				Closed?.Invoke(this, EventArgs.Empty);
			}
		}

		public void Invalidate()
		{
			this.checkIsBrowserClosedTimer.Enabled = false;
			this.browser = null;
		}

		public abstract void Focus();

		protected abstract IWebDriver CreateBrowser();

		[MethodImpl(MethodImplOptions.Synchronized)]
		private void DisposeBrowserIfClosed()
		{
			if (IsBrowserClosed())
				Close();
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		private bool IsBrowserClosed()
		{
			if (this.browser == null)
				return true;

			try
			{
				this.browser.WindowHandles.ToString();
				return false;
			}
			catch (WebDriverException)
			{
				return true;
			}
			catch (InvalidOperationException)
			{
				return true;
			}
		}

		public event EventHandler Closed;
	}
}
