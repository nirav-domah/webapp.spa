﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using WebApp.Spa.Tests.Pages;

namespace WebApp.Spa.Tests.Fixture
{
	public static class WebDriverExtensions
	{
		public static void BrowseTo(this IWebDriver driver, string url)
		{
			driver.Navigate().GoToUrl(url);
			Thread.Sleep(TimeSpan.FromMilliseconds(200));

			driver.WaitForPageToLoad();
		}

		public static T BrowseTo<T>(this IWebDriver driver, string applicationUrl, string queryString = null, object[] additionalParameters = null)
			where T : AWebPage
		{
			object[] parameters = Enumerable.Repeat(driver, 1).Concat(additionalParameters ?? Enumerable.Empty<object>()).ToArray();
			var argTypes = Enumerable.Repeat(typeof(IWebDriver), 1).Concat(parameters.Skip(1).Select(p => p.GetType())).ToArray();
			var constructor = typeof(T).GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, argTypes, null);
			var instance = (T)constructor.Invoke(parameters);

			string urlQueryString = queryString;
			if (null != urlQueryString && !urlQueryString.TrimStart().StartsWith("?"))
				urlQueryString = "?" + urlQueryString.TrimStart();

			driver.BrowseTo(applicationUrl + "/" + instance.PageName + urlQueryString);

			return instance;
		}

		public static void WaitForPageToLoad(this IWebDriver driver)
		{
			driver.WaitUntil(driver.IsPageReady, TimeSpan.FromSeconds(30));
		}

		public static bool IsPageReady(this IWebDriver driver)
		{
			return driver.ExecuteScript<bool>("return document.readyState == 'complete';")
				   && (!driver.IsRunningScript());
		}

		public static bool IsRunningScript(this IWebDriver driver)
		{
			return driver.ExecuteScript<bool>("return document.getElementById('busy-indicator') == null || document.getElementById('busy-indicator').style.display !== 'none';");
		}

		//public static bool IsRunningAjaxCall(this IWebDriver driver)
		//{
		//	long activeConnections = driver.ExecuteScript<long>("return window.jQuery ? jQuery.active : 0;");
		//	return activeConnections != 0;
		//}

		public static void ScrollIntoView(this IWebDriver driver, IWebElement element)
		{
			//driver.WaitForAnimationsToComplete();
			driver.WaitForScrollToComplete();
			driver.ExecuteScript($"window.scroll(0, {element.Location.Y - (driver.Manage().Window.Size.Height / 2)})");
		}

		//		public static void WaitForAnimationsToComplete(this IWebDriver driver)
		//		{
		//			var executor = (IJavaScriptExecutor)driver;
		//			executor.ExecuteScript(@"
		//isAnimationRunning = false;

		//animatingElements = $(':animated').not('.toast');
		//if (animatingElements.length) {
		//	isAnimationRunning = true;
		//	animatingElements.promise().done(function () {
		//		isAnimationRunning = false;
		//	});
		//}");

		//			driver.WaitUntil(() =>
		//			{
		//				bool isAnimationRunning = (bool)executor.ExecuteScript("return isAnimationRunning;");
		//				return !isAnimationRunning;
		//			});
		//		}

		public static void WaitForScrollToComplete(this IWebDriver driver)
		{
			driver.ExecuteScript("lastYScrollPosition = window.pageYOffset;");
			Thread.Sleep(100);

			driver.WaitUntil(() =>
				!driver.ExecuteScript<bool>(@"
					var currentYScrollPosition = window.pageYOffset;
					var didScroll = currentYScrollPosition !== lastYScrollPosition;
					lastYScrollPosition = currentYScrollPosition;
					return didScroll;
				"));
		}

		public static void WaitUntil(this IWebDriver driver, Func<bool> condition)
		{
			driver.WaitUntil(condition, TimeSpan.Zero);
		}

		public static void WaitUntil(this IWebDriver driver, Func<bool> condition, TimeSpan timeout)
		{
			var wait = new WebDriverWait(driver, timeout == TimeSpan.Zero ? TimeSpan.FromSeconds(5) : timeout);
			wait.Until(d => condition());
		}

		public static void DeleteAllPageCookies(this IWebDriver driver)
		{
			driver.Manage().Cookies.DeleteAllCookies();
		}

		private static void ExecuteScript(this IWebDriver driver, string script, params object[] args)
		{
			driver.ExecuteScript<object>(script, args);
		}

		private static T ExecuteScript<T>(this IWebDriver driver, string script, params object[] args)
		{
			var executor = (IJavaScriptExecutor)driver;
			return (T)executor.ExecuteScript(script, args);
		}
	}
}
