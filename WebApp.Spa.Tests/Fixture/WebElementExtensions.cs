﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using WebDriverSEd.ElementTypes;

namespace WebApp.Spa.Tests.Fixture
{
	public static class WebElementExtensions
	{
		public static void ClickAndWait(this IWebElement element)
		{
			element.ClickAndWait(TimeSpan.Zero);
		}

		public static void ClickAndWait(this IWebElement element, TimeSpan timeout)
		{
			var driver = element.GetWebDriver();
			element.ClickAndWaitUntil(() => driver.IsPageReady(), timeout);
		}

		public static void ClickAndWaitUntil(this IWebElement element, Func<bool> function)
		{
			element.ClickAndWaitUntil(function, TimeSpan.Zero);
		}

		public static void ClickAndWaitUntil(this IWebElement element, Func<bool> function, TimeSpan timeout)
		{
			ScrollIntoViewAndClick(element);
			element.GetWebDriver().WaitUntil(function, timeout);
		}

		public static void ScrollIntoViewAndClick(this IWebElement element)
		{
			element.ScrollIntoView();
			element.Click();
		}

		public static void ScrollIntoView(this IWebElement element)
		{
			var driver = element.GetWebDriver();
			driver.ScrollIntoView(element);
		}

		public static IWebDriver GetWebDriver(this IWebElement element)
		{
			if (element is RemoteWebElement remoteWebElement)
				return remoteWebElement.WrappedDriver;

			var elementsWebDriverProperty = typeof(ElementSe).GetProperty(
				"ElementsWebDriver",
				System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public);

			return (IWebDriver)elementsWebDriverProperty.GetValue(element);
		}
	}
}
