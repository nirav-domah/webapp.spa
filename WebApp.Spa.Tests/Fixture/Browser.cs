﻿using Microsoft.Win32;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;

namespace WebApp.Spa.Tests.Fixture
{
	public class Browser
	{
		private static Browser instance;

		private BrowserProvider browserProvider = new BrowserProvider();

		private Browser()
		{ }

		public static Browser Instance => instance ?? (instance = new Browser());

		public IWebDriver WebDriver => this.browserProvider.GetBrowser();

		public IWebDriver Start()
		{
			IWebDriver browser = WebDriver;
			//browser.DismissAlertIfPresent();
			//browser.DeleteAllPageCookies();

			System.Collections.ObjectModel.ReadOnlyCollection<string> tabs = browser.WindowHandles;
			for (int i = 1; i < tabs.Count; ++i)
			{
				browser.SwitchTo().Window(tabs[i]);
				browser.Close();
			}
			browser.SwitchTo().Window(tabs[0]);

			return browser;
		}

		public void Close()
		{
			this.browserProvider.Close();
			KillOrphanedProcesses();
		}

		internal void WarmUp()
		{
			// Absorb potential InvalidOperationException ("session not created exception - from tab crashed") when instantiating 
			// ChromeDriver for the first time.
			try
			{
				WebDriver.Navigate().Refresh();
			}
			catch (InvalidOperationException)
			{ }
			catch (WebDriverException)
			{ }
		}

		internal void Invalidate()
		{
			this.browserProvider.Invalidate();
			this.browserProvider = new BrowserProvider();
		}


		private static void KillOrphanedProcesses()
		{
			foreach (Process process in Process.GetProcessesByName("chromedriver"))
			{
				try
				{
					process.Kill();
				}
				catch (Win32Exception)
				{ }
				catch (InvalidOperationException)
				{ }
			}
		}


		private class BrowserProvider : ABrowserProvider
		{
			public override void Focus()
			{ }

			protected override IWebDriver CreateBrowser()
			{
				ChromeOptions chromeOptions = new ChromeOptions();
				chromeOptions.AddArgument("disable-site-isolation-trials");
				chromeOptions.AddUserProfilePreference("profile.default_content_setting_values.automatic_downloads", 1);
				chromeOptions.AddUserProfilePreference("credentials_enable_service", false);
				chromeOptions.AddUserProfilePreference("profile.password_manager_enabled", false);
				chromeOptions.SetLoggingPreference(LogType.Browser, LogLevel.All);
				chromeOptions.AddAdditionalCapability("useAutomationExtension", false);

				if (GetExePath() is string chromeExePath)
					chromeOptions.BinaryLocation = chromeExePath;

				return new ChromeDriver(ChromeDriverService.CreateDefaultService(), chromeOptions, TimeSpan.FromMinutes(5));
			}

			private string GetExePath()
			{
				using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe"))
				{
					if (key != null && key.GetValue(null).ToString() is string exePath && File.Exists(exePath))
						return exePath;

					return null;
				}
			}
		}
	}
}
