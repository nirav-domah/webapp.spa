﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.IO;
using WebApp.Spa.Core.Enums;
using WebApp.Spa.Tests.Fixture;
using WebApp.Spa.Tests.Helpers;
using WebApp.Spa.Tests.Pages;

namespace WebApp.Spa.Tests
{
	[TestFixture(Category = "Web")]
	public class TestApplication
	{
		private const string ApplicationUrl = "https://localhost/WebAppSpaTemplate";
		private const string Email = "admin@test.com";
		private const string Password = "admin";

		private IWebDriver browser;

		[OneTimeSetUp]
		public void SetUpFixture()
		{
			string webAppPath = Path.GetFullPath(Path.Combine(
				AppDomain.CurrentDomain.BaseDirectory,
				@"..\..\..\WebApp.Spa.Template"
			));
			IISHelper.Publish("WebAppSpaTemplate", webAppPath);
			this.browser = Browser.Instance.Start();

			if (ConfigHelper.GetInstance(webAppPath).GetAuthenticationType() == AuthenticationType.Cookie)
			{
				Login(Email, Password);
			}
		}

		[Test]
		public void PageNotFoundError()
		{
			this.browser.BrowseTo($"{ApplicationUrl}/nonexistantpage");
			var page = new PageNotFoundErrorPage(this.browser);

			Assert.IsTrue(page.IsDisplayed);
			Assert.AreEqual("404 - This page does not exist", page.HeaderText);

			page.ClickBackToApplicationButton();

			Assert.AreEqual($"{ApplicationUrl}/StartPage", this.browser.Url);
		}

		[Test]
		public void Settings()
		{
			var startPage = this.browser.BrowseTo<StartPage>(ApplicationUrl);

			Assert.AreEqual("Setting Value 1", startPage.SettingsLabelText);

			var page1 = this.browser.BrowseTo<Page1>(ApplicationUrl);

			Assert.AreEqual("Setting Value 2", page1.SettingsLabelText);
		}

		[Test]
		public void DocumentLoadEvent()
		{
			var startPage = this.browser.BrowseTo<StartPage>(ApplicationUrl);

			Assert.AreEqual("DefaultTemplate loaded", startPage.DefaultTemplateLoadLabelText);
			Assert.AreEqual("StartPage loaded", startPage.LoadLabelText);

			var page1 = this.browser.BrowseTo<Page1>(ApplicationUrl);

			Assert.AreEqual("DefaultTemplate loaded", page1.DefaultTemplateLoadLabelText);
			Assert.AreEqual("Page1 loaded", page1.LoadLabelText);
		}

		[Test]
		public void LabelText()
		{
			var page = this.browser.BrowseTo<LabelPage>(ApplicationUrl);

			Assert.AreEqual("initial text", page.TextLabelText);
			Assert.AreEqual(string.Empty, page.ResultLabelText);

			page.ClickGetLabelTextButton();

			Assert.AreEqual("initial text", page.ResultLabelText);

			page.ClickSetLabelTextButton();

			Assert.AreEqual("set text", page.TextLabelText);

			page.ClickGetLabelTextButton();

			Assert.AreEqual("set text", page.ResultLabelText);
		}

		[Test]
		public void LabelVisible()
		{
			var page = this.browser.BrowseTo<LabelPage>(ApplicationUrl);

			Assert.IsFalse(page.VisibleLabelVisible);
			Assert.AreEqual(string.Empty, page.ResultLabelText);

			page.ClickGetLabelVisibleButton();

			Assert.AreEqual("false", page.ResultLabelText);

			page.ClickSetLabelVisibleButton();

			Assert.IsTrue(page.VisibleLabelVisible);

			page.ClickGetLabelVisibleButton();

			Assert.AreEqual("true", page.ResultLabelText);
		}

		[Test]
		public void TextBoxText()
		{
			var page = this.browser.BrowseTo<TextBoxPage>(ApplicationUrl);

			Assert.AreEqual("initial text", page.TextTextBoxText);

			page.TextTextBoxText = "entered text";

			Assert.AreEqual(string.Empty, page.ResultLabelText);

			page.ClickGetTextBoxTextButton();

			Assert.AreEqual("entered text", page.ResultLabelText);

			page.ClickSetLabelTextButton();

			Assert.AreEqual("set text", page.TextTextBoxText);

			page.ClickGetTextBoxTextButton();

			Assert.AreEqual("set text", page.ResultLabelText);
		}

		private void Login(string email, string password)
		{
			this.browser.DeleteAllPageCookies();

			this.browser.BrowseTo(ApplicationUrl);
			var loginPage = new LoginPage(this.browser);
			loginPage.Login(email, password);
		}
	}
}
