﻿using NUnit.Framework;
using WebApp.Spa.Tests.Fixture;

namespace WebApp.Spa.Tests
{
	[SetUpFixture]
	public class GlobalSetup
	{
		[OneTimeSetUp]
		public void RunBeforeAnyTests()
		{
			Browser.Instance.Close();
		}

		[OneTimeTearDown]
		public void RunAfterAnyTests()
		{
			Browser.Instance.Close();
		}
	}
}
