# Solution Setup

- [Download and install Node.js](https://nodejs.org/en/download/) (you can leave *Automatically install necessary tools* unchecked)
- In Command Prompt, run the following to install Vue:
``npm install -g @vue/cli``
-  In Visual Studio 2019, under *Tools > Get Tools and Features*, ensure the following workloads are installed:
  - Node.js development
  - ASP.net and web development
- [Download and install the ASP.Net Core Module for IIS hosting](https://dotnet.microsoft.com/download/dotnet-core/thank-you/runtime-aspnetcore-2.2.8-windows-hosting-bundle-installer)

# Running Web App

- In Visual Studio 2019, set run to *Web.App.Spa.Template* instead of *IIS Express*
- Run the solution (the first build might take a few minutes to download npm dependencies)