﻿using System;
using System.IO;

namespace WebApp.Spa.Core
{
	public class ContextStream : Stream
	{
		private readonly Stream wrappedStream;
		private readonly IDisposable context;

		public ContextStream(Stream wrappedStream, IDisposable context)
		{
			this.wrappedStream = wrappedStream;
			this.context = context;
		}

		public override bool CanRead => this.wrappedStream.CanRead;

		public override bool CanSeek => this.wrappedStream.CanSeek;

		public override bool CanWrite => this.wrappedStream.CanWrite;

		public override long Length => this.wrappedStream.Length;

		public override long Position
		{
			get { return this.wrappedStream.Position; }
			set { this.wrappedStream.Position = value; }
		}

		public override void Flush()
		{
			this.wrappedStream.Flush();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			return this.wrappedStream.Read(buffer, offset, count);
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.wrappedStream.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			this.wrappedStream.SetLength(value);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			this.wrappedStream.Write(buffer, offset, count);
		}

		protected override void Dispose(bool disposing)
		{
			this.context?.Dispose();
			this.wrappedStream.Dispose();

			base.Dispose(disposing);
		}
	}
}
