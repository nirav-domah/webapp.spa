﻿using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Data.Common;

namespace WebApp.Spa.Core.Services.Database
{
	public class CommandFactory
	{
		public DbCommand Create(DbConnection connection, string commandText, CommandType commandType, int? timeout = null)
		{
			var command = connection.CreateCommand();
			if (command is OracleCommand oracleCommand)
				oracleCommand.BindByName = true;

			command.CommandText = commandText;
			command.CommandType = commandType;
			command.CommandTimeout = timeout ?? 0;
			return command;
		}
	}
}
