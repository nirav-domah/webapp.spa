﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Spa.Core.Extensions;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.Configuration;

namespace WebApp.Spa.Core.Services.Database
{
	public class Database : IDatabase
	{
		private readonly bool isPreview;
		private IDebugLoggerClient debugLogger;

		public Database()
		{
			var config = AppSettingsHelper.GetInstance().GetAppSettings().Config;
			this.isPreview = config.Preview;
		}

		public void EnableDebugLogger(IDebugLoggerClient debugLoggerClient)
		{
			this.debugLogger = debugLoggerClient;
		}

		public async Task<IQueryResult> ExecuteQuery(
			DbConnection connection,
			string connectionString,
			string commandText,
			CommandType commandType,
			int? commandTimeout,
			DbParameter[] parameters)
		{
			var result = new QueryResult();

			connection.ConnectionString = connectionString;

			var command = new CommandFactory().Create(connection, commandText, commandType, commandTimeout);

			if (parameters != null && parameters.Any())
				command.Parameters.AddRange(parameters);

			if (this.debugLogger != null)
				await LogCommand(command);

			try
			{
				if (connection.State != ConnectionState.Open)
					connection.Open();

				ExecuteQuery(command, out object[][] resultSet, out int numberOfRowsAffected);
				result.ResultSet = resultSet;
				result.NumberOfRowsAffected = numberOfRowsAffected;
				result.OutputParameters = parameters.Where(p => p.Direction == ParameterDirection.Output).ToArray();
			}
			catch (FileNotFoundException fileNotFoundException) when (fileNotFoundException.FileName.StartsWith("Microsoft.SqlServer.Types"))
			{
				throw new Exception("Unsupported column datatype. The SqlHierarchyId, SqlGeography & SqlGeometry datatypes are not supported.");
			}
			finally
			{
				connection?.Close();
			}

			if (this.debugLogger != null)
				await LogResult(result);

			return result;
		}

		private async Task LogCommand(DbCommand command)
		{
			if (this.isPreview)
				await this.debugLogger.Log($"Connection String: {command.Connection.ConnectionString}");

			await this.debugLogger.Log($"Command Type: {command.CommandType}");

			var inputParameters = command.Parameters
											.Cast<DbParameter>()
											.Where(p => p.Direction == ParameterDirection.Input);

			if (inputParameters.Any())
			{
				await this.debugLogger.LogGroupInfo("Input Parameters", async () =>
				{
					foreach (var inputParameter in inputParameters)
					{
						await this.debugLogger.LogGroupInfo(inputParameter.ParameterName, async () =>
						{
							await this.debugLogger.Log($"Value: {inputParameter.Value}");
							await this.debugLogger.Log($"Type: {inputParameter.DbType}");
						});
					}
				});
			}
		}

		private async Task LogResult(QueryResult result)
		{
			bool hasResultSet = result.NumberOfRowsAffected < 0 || (result != null && result.ResultSet.Any());

			await this.debugLogger.LogGroupInfo("Result", async () =>
			{
				if (hasResultSet)
				{
					await this.debugLogger.Log($"{result.ResultSet.Length} row(s) returned.");
				}
				else if (result.NumberOfRowsAffected >= 0 || !result.OutputParameters.Any())
				{
					await this.debugLogger.Log($"{result.NumberOfRowsAffected} row(s) affected.");
				}

				if (result.OutputParameters.Any())
				{
					await this.debugLogger.LogGroupInfo("Output Parameters", async () =>
					{
						foreach (var outputParameter in result.OutputParameters)
						{
							await this.debugLogger.LogGroupInfo(outputParameter.ParameterName, async () =>
							{
								await this.debugLogger.Log($"Value: {outputParameter.Value}");
								await this.debugLogger.Log($"Type: {outputParameter.DbType}");
							});
						}
					});
				}
			});
		}

		private void ExecuteQuery(DbCommand command, out object[][] resultSet, out int numberOfRowsAffected)
		{
			using (var reader = command.ExecuteReader())
			{
				numberOfRowsAffected = reader.RecordsAffected;

				var resultRows = new List<object[]>();
				while (reader.Read())
				{
					object[] row = new object[reader.FieldCount];
					reader.GetValues(row);

					resultRows.Add(row);
				}

				resultSet = resultRows.ToArray();
			}
		}
	}
}
