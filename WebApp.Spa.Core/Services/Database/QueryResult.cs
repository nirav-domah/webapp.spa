﻿using System.Collections.Generic;
using System.Data.Common;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Core.Services.Database
{
	public class QueryResult : IQueryResult
	{
		public object[][] ResultSet { get; set; }

		public IList<DbParameter> OutputParameters { get; set; } = new List<DbParameter>();

		public int NumberOfRowsAffected { get; set; } = -1;
	}
}
