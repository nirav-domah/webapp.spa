﻿using Microsoft.ClearScript;
using Microsoft.ClearScript.V8;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Core.Services.CodeInterpreter
{
	public class CodeInterpreter : ICodeInterpreter
	{
		public string Evaluate(string expression)
		{
			using (var javaScriptEngine = new V8ScriptEngine())
			{
				javaScriptEngine.Execute(
$@"function toString(value) {{
	if (value == null)
		return value;

	return typeof value === 'object' && !(value instanceof Date) ? JSON.stringify(value) : String(value);
}}
");
				javaScriptEngine.Execute($"var expressionResult = toString({expression});");

				var result = javaScriptEngine.Script.expressionResult;
				return result is Undefined ? null : result;
			}
		}
	}
}
