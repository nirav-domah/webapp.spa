﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Enums;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Core.Services.OAuth.ProviderSettings
{
	public class OidcSettingsProvider
	{
		private readonly IDictionary<string, string> authenticationSettings;

		[ActivatorUtilitiesConstructor]
		public OidcSettingsProvider(IAuthenticationSettingStore authenticationSettingStore)
			: this(authenticationSettingStore.AuthenticationSettings.ToDictionary(s => s.Name, s => s.Value))
		{ }

		private OidcSettingsProvider(IDictionary<string, string> authenticationSettings)
		{
			this.authenticationSettings = authenticationSettings;

			if (this.authenticationSettings.TryGetValue(AuthenticationSetting.OidcProvider, out string oidcProviderString)
				&& Enum.TryParse<OidcProvider>(oidcProviderString, out var oidcProvider))
			{
				switch (oidcProvider)
				{
					case OidcProvider.Okta: Settings = new OktaSettings(this.authenticationSettings); break;
					case OidcProvider.Auth0: Settings = new Auth0Settings(this.authenticationSettings); break;
					case OidcProvider.Google: Settings = new GoogleSettings(this.authenticationSettings); break;
					case OidcProvider.AzureAD: Settings = new AzureADSettings(this.authenticationSettings); break;
					case OidcProvider.GenericProvider: Settings = new GenericProviderSettings(this.authenticationSettings); break;
					default: throw new Exception($"Unsupported OIDC provider: {oidcProvider}");
				};
			}
			else
			{
				Settings = new GenericProviderSettings(this.authenticationSettings);
			}
		}

		public GenericProviderSettings Settings { get; }

		public static OidcSettingsProvider CreateInstance(IDictionary<string, string> authenticationSettings)
		{
			return new OidcSettingsProvider(authenticationSettings);
		}
	}
}
