﻿using System.Collections.Generic;
using WebApp.Spa.Core.Enums;

namespace WebApp.Spa.Core.Services.OAuth.ProviderSettings
{
	internal class OktaSettings : GenericProviderSettings
	{
		private const string OAuth2 = "oauth2";
		private const string DefaultAuthorizationServerId = "default";

		public OktaSettings(IDictionary<string, string> authenticationSettings)
			: base(authenticationSettings)
		{ }

		public override OidcProvider? OidcProvider => Enums.OidcProvider.Okta;

		public override string Audience => $"api://{DefaultAuthorizationServerId}";

		public override string Authority
		{
			get
			{
				string authority = base.Authority;

				if (!string.IsNullOrEmpty(authority) && !authority.Contains($"/{OAuth2}/"))
					return UrlHelper.Combine(authority, OAuth2, DefaultAuthorizationServerId);

				return authority;
			}
		}

		public override string Scopes => DefaultScopes;
	}
}
