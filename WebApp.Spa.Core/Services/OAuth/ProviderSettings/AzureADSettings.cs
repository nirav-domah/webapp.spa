﻿using System.Collections.Generic;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Enums;

namespace WebApp.Spa.Core.Services.OAuth.ProviderSettings
{
	internal class AzureADSettings : GenericProviderSettings
	{
		private readonly IDictionary<string, string> authenticationSettings;

		public AzureADSettings(IDictionary<string, string> authenticationSettings)
			: base(authenticationSettings)
		{
			this.authenticationSettings = authenticationSettings;
		}

		public override OidcProvider? OidcProvider => Enums.OidcProvider.AzureAD;

		public override string Audience => ClientId;

		public override string Authority => UrlHelper.Combine("https://login.microsoftonline.com", Tenant, "v2.0");

		public override string Tenant => this.authenticationSettings[AuthenticationSetting.Tenant];

		public override string RoleClaimName => "roles";

		public override string Scopes => DefaultScopes;
	}
}
