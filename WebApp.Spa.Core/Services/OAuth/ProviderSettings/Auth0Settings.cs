﻿using System.Collections.Generic;
using WebApp.Spa.Core.Enums;

namespace WebApp.Spa.Core.Services.OAuth.ProviderSettings
{
	internal class Auth0Settings : GenericProviderSettings
	{
		public Auth0Settings(IDictionary<string, string> authenticationSettings)
			: base(authenticationSettings)
		{ }

		public override OidcProvider? OidcProvider => Enums.OidcProvider.Auth0;

		public override string Audience => UrlHelper.Combine(Authority, "api", "v2") + "/";

		public override string EndSessionEndpoint
		{
			get
			{
				string url = UrlHelper.Combine(Authority, "v2", "logout");
				return UrlHelper.AddQueryString(url, new Dictionary<string, string>
				{
					{ "client_id", ClientId },
					{ "returnTo", LogoutRedirectUrl }
				});
			}
		}

		public override string Scopes => DefaultScopes;
	}
}
