﻿using System.Collections.Generic;
using WebApp.Spa.Core.Enums;

namespace WebApp.Spa.Core.Services.OAuth.ProviderSettings
{
	internal class GoogleSettings : GenericProviderSettings
	{
		public GoogleSettings(IDictionary<string, string> authenticationSettings)
			: base(authenticationSettings)
		{ }

		public override OidcProvider? OidcProvider => Enums.OidcProvider.Google;

		public override string Audience => ClientId;

		public override string Authority => "https://accounts.google.com";

		public override string RoleClaimName => null;

		public override string ResponseType => "token id_token";

		public override string EndSessionEndpoint => LogoutRedirectUrl;

		public override string Scopes => DefaultScopes;
	}
}
