﻿using System.Collections.Generic;
using WebApp.Spa.Core.Entities;
using WebApp.Spa.Core.Enums;

namespace WebApp.Spa.Core.Services.OAuth.ProviderSettings
{
	public class GenericProviderSettings
	{
		private readonly IDictionary<string, string> authenticationSettings;
		protected const string DefaultScopes = "openid profile email";

		public GenericProviderSettings(IDictionary<string, string> authenticationSettings)
		{
			this.authenticationSettings = authenticationSettings;
		}

		public virtual OidcProvider? OidcProvider { get; }

		public virtual string Audience => this.authenticationSettings.TryGetValue(AuthenticationSetting.Audience, out string audience) ? audience : null;

		public virtual string Authority
		{
			get
			{
				string authority = this.authenticationSettings[AuthenticationSetting.AuthenticationAuthorityUri];

				return UrlHelper.EnsureHttpProtocol(authority, secure: true);
			}
		}

		public virtual string Tenant => null;

		public virtual string RoleClaimName => this.authenticationSettings[AuthenticationSetting.RoleClaimName];

		public virtual string ResponseType => "code";

		public virtual string EndSessionEndpoint => null;

		public virtual string Scopes => this.authenticationSettings.TryGetValue(AuthenticationSetting.Scopes, out string storedScopes) ? storedScopes : null;

		public string RedirectUrl => this.authenticationSettings[AuthenticationSetting.RedirectUrl];

		public string LogoutRedirectUrl => this.authenticationSettings[AuthenticationSetting.LogoutRedirectUrl];

		public string ClientId => this.authenticationSettings[AuthenticationSetting.ClientID];

		public string ApiResourceName => this.authenticationSettings.TryGetValue(AuthenticationSetting.ApiResourceName, out string storedAPiResourceName) ? storedAPiResourceName : null;

		public string ApiResourceSecret => this.authenticationSettings.TryGetValue(AuthenticationSetting.ApiResourceSecret, out string storedApiResourceSecret) ? storedApiResourceSecret : null;
	}
}
