﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Text;
using WebApp.Spa.Core.Entities.FileTokens;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.Configuration;

namespace WebApp.Spa.Core.Services.ValueResolver
{
	public class ValueResolver : IValueResolver
	{
		private readonly Config config;
		private readonly IFilesService filesService;

		public ValueResolver(IOptionsSnapshot<Config> configOptions, IFilesService filesService)
		{
			this.config = configOptions.Value;
			this.filesService = filesService;
		}

		public T ResolveValue<T>(object value)
		{
			var targetType = typeof(T).IsGenericType && (typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>))
								? typeof(T).GetGenericArguments()[0]
								: typeof(T);

			if ((value == null) || (value.Equals(string.Empty) && (targetType != typeof(string))))
				return default;

			if (value is AFileToken fileToken)
			{
				return targetType == typeof(string)
						? (T)(object)this.filesService.ReadText(fileToken)
						: (T)(object)this.filesService.ReadBytes(fileToken);
			}

			if (value is string valueAsString)
			{
				if (targetType == typeof(byte[]))
					return (T)(object)Encoding.UTF8.GetBytes(valueAsString);

				if (targetType == typeof(Guid))
					return (T)(object)Guid.Parse(valueAsString);

				if (targetType == typeof(DateTime)
					&& DateTime.TryParseExact(
						valueAsString,
						this.config.DateFormat,
						CultureInfo.InvariantCulture,
						DateTimeStyles.None,
						out var date)
					)
				{
					return (T)(object)date;
				}
			}

			if (value is JObject jObject)
			{
				string jsonString = JsonConvert.SerializeObject(jObject);

				if (targetType == typeof(string))
					return (T)(object)jsonString;

				if (targetType == typeof(byte[]))
					return (T)(object)Encoding.UTF8.GetBytes(jsonString);

				return JsonConvert.DeserializeObject<T>(jsonString);
			}

			return (T)Convert.ChangeType(value, targetType);
		}

		public T ResolveParameter<T>(JObject parametersContainer, string parameterName, bool resolveFileTokenValue = true)
		{
			if (!parametersContainer.ContainsKey(parameterName))
				return default;

			if (parametersContainer[parameterName].Type == JTokenType.Object)
			{
				return resolveFileTokenValue
					? ResolveValue<T>(ResolveParameter(parametersContainer.Value<JObject>(parameterName)))
					: (T)ResolveParameter(parametersContainer.Value<JObject>(parameterName));
			}
			
			return ResolveValue<T>(((JValue)parametersContainer[parameterName]).Value);
		}

		private object ResolveParameter(object value)
		{
			if (value is JObject jObject
				&& this.filesService.TryParseFileToken(jObject, out var fileToken))
			{
				return fileToken;
			}

			if (value is JValue jValue)
				return jValue.Value;

			return value;
		}
	}
}
