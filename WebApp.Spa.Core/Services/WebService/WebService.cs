﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebApp.Spa.Core.Entities.FileTokens;
using WebApp.Spa.Core.Extensions;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.Configuration;

namespace WebApp.Spa.Core.Services.WebService
{
	public class WebService : IWebService
	{
		public enum Authentication { Anonymous, Basic, ApiKey, Digest, Windows };
		public enum KeyLocation { Header, QueryString, Cookie };
		public enum HttpMethod { GET, POST, PUT, DELETE, HEAD };

		public enum ResponseContentType
		{
			[Description("text/plain")]
			Text,
			[Description("application/json")]
			Json,
			[Description("application/xml")]
			Xml,
			[Description("application/octet-stream")]
			OctetStream
		};

		private readonly bool isPreview;
		private readonly IValueResolver valueResolver;
		private readonly IFilesService filesService;
		private IDebugLoggerClient debugLogger;

		public WebService(IValueResolver valueResolver, IFilesService filesService)
		{
			var config = AppSettingsHelper.GetInstance().GetAppSettings().Config;
			this.isPreview = config.Preview;
			this.valueResolver = valueResolver;
			this.filesService = filesService;
		}

		public void EnableDebugLogger(IDebugLoggerClient debugLoggerClient)
		{
			this.debugLogger = debugLoggerClient;
		}

		public async Task<object> Execute(
			string url,
			HttpMethod httpMethod,
			List<KeyValuePair<string, string>> queryStrings,
			List<KeyValuePair<string, string>> headers,
			List<MultipartParameter> multipartParameters,
			string bodyContentType,
			object body,
			List<KeyValuePair<string, string>> cookies,
			ResponseContentType responseContentType,
			TimeSpan timeout,
			Authentication authentication,
			string username,
			string password,
			string domain = null,
			string proxyHost = null,
			int proxyPort = 80,
			bool bypassProxyOnLocal = false
			)
		{
			var requestMessage = ConstructRequestMessage(
									url,
									httpMethod,
									queryStrings,
									headers,
									multipartParameters,
									bodyContentType,
									body);

			var webProxy = ConstructWebProxy(proxyHost, proxyPort, bypassProxyOnLocal);

			var httpClient = ConstructHttpClient(
								requestMessage.RequestUri,
								timeout,
								authentication,
								username,
								password,
								domain,
								webProxy,
								cookies);

			if (this.debugLogger != null)
			{
				await LogAuthenticationDetails(authentication, username, password, domain);
				await LogRequestDetails(requestMessage, cookies, webProxy);
			}

			object responseContent = CallWebService(httpClient, requestMessage, EnumHelper.GetValueDescription(responseContentType), out string responseMediaType, out int statusCode, out string reasonPhrase, out bool isSuccess);

			if (this.debugLogger != null)
			{
				await LogResponseDetails(responseContent?.ToString(), responseMediaType, statusCode, reasonPhrase);
			}

			if (!isSuccess)
				throw new Exception(responseContent?.ToString());

			return responseContent is string stringResponseContent
				? ParseResponse(stringResponseContent, responseContentType)
				: responseContent;
		}

		private HttpRequestMessage ConstructRequestMessage(
			string url,
			HttpMethod httpMethod,
			List<KeyValuePair<string, string>> queryStrings,
			List<KeyValuePair<string, string>> headers,
			List<MultipartParameter> multipartParameters,
			string bodyContentType,
			object body
			)
		{
			var fullUrl = ConstructUrl(url, queryStrings);

			var requestMessage = new HttpRequestMessage
			{
				Method = TranslateMethod(httpMethod),
				RequestUri = fullUrl,
				Content = GetBodyContent(body, bodyContentType, multipartParameters)
			};

			PopulateHeaders(requestMessage, headers);

			return requestMessage;
		}

		private Uri ConstructUrl(string baseUrl, List<KeyValuePair<string, string>> queryStringParameters)
		{
			string encodedBaseUrl = Uri.EscapeUriString(baseUrl);

			var encodedQueryStringParameters = new List<string>();
			foreach (var parameterItem in queryStringParameters)
			{
				if (parameterItem.Key != string.Empty && parameterItem.Value != null)
					encodedQueryStringParameters.Add($"{WebUtility.UrlEncode(parameterItem.Key)}={WebUtility.UrlEncode(parameterItem.Value)}");
			}

			if (encodedQueryStringParameters.Count > 0)
			{
				encodedBaseUrl += encodedBaseUrl.Contains("?") ? "&" : "?";
				encodedBaseUrl += string.Join("&", encodedQueryStringParameters.ToArray());
			}

			return new Uri(encodedBaseUrl);
		}

		private WebProxy ConstructWebProxy(string proxyHost, int proxyPort, bool bypassProxyOnLocal)
		{
			if (string.IsNullOrEmpty(proxyHost))
				return null;

			return new WebProxy(proxyHost, proxyPort)
			{
				BypassProxyOnLocal = bypassProxyOnLocal
			};
		}

		private HttpClient ConstructHttpClient(
			Uri fullUrl,
			TimeSpan timeout,
			Authentication authentication,
			string username,
			string password,
			string domain,
			WebProxy webProxy,
			List<KeyValuePair<string, string>> cookies)
		{
			var handler = new WebRequestHandler();
			var httpClient = new HttpClient(handler) { Timeout = timeout };

			var credentials = ConstructCredentials(authentication, username, password, domain, new Uri(fullUrl.GetLeftPart(UriPartial.Authority)));
			handler.Credentials = credentials;

			if (authentication == Authentication.Basic)
			{
				string authenticationHeader = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}"));
				httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authenticationHeader);
			}

			if (webProxy != null)
			{
				webProxy.Credentials = credentials;
				handler.Proxy = webProxy;
			}

			if (cookies.Count() > 0)
			{
				var cookieContainer = new CookieContainer();
				foreach (var cookie in cookies.Where(c => c.Value != null))
					cookieContainer.Add(new Cookie(cookie.Key, cookie.Value) { Domain = fullUrl.Host });

				handler.UseCookies = true;
				handler.CookieContainer = cookieContainer;
			}

			return httpClient;
		}

		private object CallWebService(HttpClient client, HttpRequestMessage requestMessage, string responseMediaType, out string receivedResponseMediaType, out int statusCode, out string reasonPhrase, out bool isSuccess)
		{
			bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
			{
				return true;
			}

			try
			{
				ServicePointManager.ServerCertificateValidationCallback += ValidateServerCertificate;
				var sendTask = client.SendAsync(requestMessage);

				sendTask.Wait();

				var responseMessage = sendTask.Result;
				statusCode = (int)responseMessage.StatusCode;
				reasonPhrase = responseMessage.ReasonPhrase;
				isSuccess = responseMessage.IsSuccessStatusCode;
				receivedResponseMediaType = responseMessage.Content?.Headers.ContentType?.MediaType;

				if (responseMessage.Content != null)
				{
					QueryPropertiesToFixLazyLoadingIssue(responseMessage.Content.Headers);

					if (responseMediaType != EnumHelper.GetValueDescription(ResponseContentType.Text)
						&& responseMediaType != receivedResponseMediaType)
					{
						throw new Exception($@"Invalid response format: Expected ""{responseMediaType}"" but got ""{receivedResponseMediaType ?? "null"}"".");
					}

					object result;
					if (responseMediaType == EnumHelper.GetValueDescription(ResponseContentType.OctetStream))
					{
						var readByteArrayTask = sendTask.Result.Content.ReadAsByteArrayAsync();
						readByteArrayTask.Wait();

						result = readByteArrayTask.Result;
					}
					else
					{
						var readStringTask = sendTask.Result.Content.ReadAsStringAsync();
						readStringTask.Wait();

						result = readStringTask.Result;
					}

					return !isSuccess && result is string stringResult
							? GetErrorText(stringResult, receivedResponseMediaType)
							: result;
				}

				return string.Empty;
			}
			catch (AggregateException ex) when (ex.InnerException is TaskCanceledException)
			{
				throw new TimeoutException();
			}
			finally
			{
				ServicePointManager.ServerCertificateValidationCallback -= ValidateServerCertificate;
			}
		}

		private async Task LogAuthenticationDetails(Authentication authentication, string username, string password, string domain)
		{
			await this.debugLogger.LogGroupInfo("Authentication", async () =>
			{
				await this.debugLogger.Log($"Type: {authentication}");

				if (authentication == Authentication.Digest)
				{
					await this.debugLogger.Log($"Domain: {domain}");
				}

				if (authentication == Authentication.Basic || authentication == Authentication.Digest)
				{
					await this.debugLogger.Log($"Username: {username}");
					if (this.isPreview)
						await this.debugLogger.Log($"Password: {password}");
				}
			});
		}

		private async Task LogRequestDetails(HttpRequestMessage requestMessage, List<KeyValuePair<string, string>> cookies, WebProxy webProxy)
		{
			await this.debugLogger.LogGroupInfo("Request", async () =>
			{
				await this.debugLogger.Log($"URL: {requestMessage.RequestUri}");
				await this.debugLogger.Log($"HTTP Method: {requestMessage.Method.Method}");

				if (requestMessage.Headers.Any())
				{
					await this.debugLogger.LogGroupInfo("Headers", async () =>
					{
						foreach (var header in requestMessage.Headers)
							await this.debugLogger.Log($"{header.Key}: {string.Join(", ", header.Value)}");
					});
				}

				if (cookies.Any())
				{
					await this.debugLogger.LogGroupInfo("Cookies", async () =>
					{
						foreach (var cookie in cookies)
							await this.debugLogger.Log($"{cookie.Key}: {cookie.Value}");
					});
				}

				if (requestMessage.Method.Method != "GET" && requestMessage.Method.Method != "HEAD")
				{
					string contentMediaType = requestMessage.Content?.Headers.ContentType.MediaType;
					await this.debugLogger.Log($"Body Content Type: {contentMediaType}");

					if (contentMediaType == "multipart/form-data")
					{
						var multipartProvider = await requestMessage.Content.ReadAsMultipartAsync();
						await this.debugLogger.LogGroupInfo("Multipart Form Data", async () =>
						{
							for (int i = 0; i < multipartProvider.Contents.Count; i++)
							{
								var multipartContent = multipartProvider.Contents[i];
								await this.debugLogger.LogGroupInfo($"Content[{i}]", async () =>
								{
									await this.debugLogger.Log($"Content Type: {multipartContent.Headers.ContentType.MediaType}");

									if (!string.IsNullOrEmpty(multipartContent.Headers.ContentDisposition?.FileName))
										await this.debugLogger.Log($"File Name: {multipartContent.Headers.ContentDisposition.FileName}");

									string content = await multipartContent.ReadAsStringAsync();
									await this.debugLogger.Log($"Body: {content.Truncate(500)}");
								});
							}
						});
					}
					else
					{
						string content = requestMessage.Content == null ? null : await requestMessage.Content.ReadAsStringAsync();
						await this.debugLogger.Log($"Body: {content.Truncate(500)}");
					}
				}

				if (webProxy != null)
				{
					await this.debugLogger.LogGroupInfo("Web Proxy", async () =>
					{
						await this.debugLogger.Log($"Host: {webProxy.Address.Host}");
						await this.debugLogger.Log($"Port: {webProxy.Address.Port}");
						await this.debugLogger.Log($"Bypass On Local: {webProxy.BypassProxyOnLocal}");
					});
				}
			});
		}

		private async Task LogResponseDetails(string responseContent, string responseMediaType, int statusCode, string reasonPhrase)
		{
			await this.debugLogger.LogGroupInfo("Response", async () =>
			{
				await this.debugLogger.Log($"Status Code: {statusCode}");
				await this.debugLogger.Log($"Reason Phrase: {reasonPhrase}");
				await this.debugLogger.Log($"Media Type: {responseMediaType}");
				await this.debugLogger.Log($"Content: {responseContent.Truncate(500)}");
			});
		}

		private ICredentials ConstructCredentials(Authentication authentication, string username, string password, string domain, Uri uriPrefix = null)
		{
			switch (authentication)
			{
				case Authentication.Digest:
					{
						var credential = new NetworkCredential(username, password, domain);
						var credentialCache = new CredentialCache
						{
							{ uriPrefix, "Digest", credential }
						};
						return credentialCache;
					}
				case Authentication.Windows: return CredentialCache.DefaultCredentials;
				case Authentication.Anonymous:
				case Authentication.Basic:
				case Authentication.ApiKey:
				default: return null;
			}
		}

		private System.Net.Http.HttpMethod TranslateMethod(HttpMethod method)
		{
			switch (method)
			{
				case HttpMethod.DELETE: return System.Net.Http.HttpMethod.Delete;
				case HttpMethod.GET: return System.Net.Http.HttpMethod.Get;
				case HttpMethod.HEAD: return System.Net.Http.HttpMethod.Head;
				case HttpMethod.POST: return System.Net.Http.HttpMethod.Post;
				case HttpMethod.PUT: return System.Net.Http.HttpMethod.Put;
				default:
					throw new Exception($"Unsupported method: {method}.");
			}
		}

		private HttpContent GetBodyContent(object body, string bodyContentType, List<MultipartParameter> multipartParameters)
		{
			if (body == null && multipartParameters.Count == 0)
				return null;

			if (bodyContentType == "application/octet-stream")
			{
				byte[] binaryData = this.valueResolver.ResolveValue<byte[]>(body);
				var byteArrayContent = new ByteArrayContent(binaryData);
				byteArrayContent.Headers.ContentType = new MediaTypeHeaderValue(bodyContentType);
				return byteArrayContent;
			}

			if (bodyContentType == "application/json")
			{
				object bodyContent = body is AFileToken fileToken
									? this.filesService.ReadText(fileToken)
									: body;

				string jsonData = JsonConvert.SerializeObject(bodyContent);
				return new StringContent(jsonData, Encoding.UTF8, bodyContentType);
			}

			if (bodyContentType == "multipart/form-data")
			{
				var multipartFormDataContent = new MultipartFormDataContent(Guid.NewGuid().ToString());

				foreach (var parameter in multipartParameters)
				{
					byte[] binaryData = this.valueResolver.ResolveValue<byte[]>(parameter.Value);

					var content = new ByteArrayContent(binaryData);
					content.Headers.ContentType = new MediaTypeHeaderValue(parameter.ContentType);

					if (string.IsNullOrWhiteSpace(parameter.FileName))
						multipartFormDataContent.Add(content, parameter.Name);
					else
						multipartFormDataContent.Add(content, parameter.Name, parameter.FileName);
				}

				return multipartFormDataContent;
			}

			string stringData = this.valueResolver.ResolveValue<string>(body);
			return new StringContent(stringData, Encoding.UTF8, bodyContentType);
		}

		private void PopulateHeaders(HttpRequestMessage requestMessage, List<KeyValuePair<string, string>> headers)
		{
			foreach (var header in headers.Where(h => h.Key != string.Empty && h.Value != null))
			{
				requestMessage.Headers.TryAddWithoutValidation(header.Key, header.Value);
			}
		}

		private void QueryPropertiesToFixLazyLoadingIssue(HttpContentHeaders httpContentHeaders)
		{
			try
			{
				foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(httpContentHeaders))
					property.GetValue(httpContentHeaders);
			}
			catch { }
		}

		private string GetErrorText(string responseText, string receivedResponseMediaType)
		{
			if (receivedResponseMediaType == EnumHelper.GetValueDescription(MediaTypeNames.Text.Html))
				return ExtractTextContentFromHtml(responseText);

			try
			{
				var parsedResponse = JsonConvert.DeserializeObject(responseText) as JObject;
				var messageProperty = parsedResponse?.Property("Message");
				return messageProperty != null
					? ((JValue)messageProperty.Value).Value.ToString()
					: responseText;
			}
			catch (JsonException)
			{
				return responseText;
			}
		}

		private string ExtractTextContentFromHtml(string html)
		{
			if (string.IsNullOrWhiteSpace(html))
				return string.Empty;

			string[] excludeTags = { "title", "style", "script" };

			var document = new HtmlDocument();
			document.LoadHtml(html);

			var textContent = document.DocumentNode.SelectNodes("//text()")?
				.Where(node => !excludeTags.Contains(node.ParentNode.Name) && !string.IsNullOrWhiteSpace(node.InnerText))
				.Select(node => node.InnerText);

			if (textContent != null && textContent.Count() > 0)
				return string.Join(string.Empty, textContent);

			return html;
		}

		private object ParseResponse(string responseContent, ResponseContentType responseContentType)
		{
			switch (responseContentType)
			{
				case ResponseContentType.Json: return JsonConvert.DeserializeObject(responseContent);
				case ResponseContentType.Xml: return XDocument.Parse(responseContent);
				default: return responseContent;
			}
		}

		public class MultipartParameter
		{
			public string Name { get; set; }
			public string ContentType { get; set; }
			public string FileName { get; set; }
			public object Value { get; set; }
		}
	}
}
