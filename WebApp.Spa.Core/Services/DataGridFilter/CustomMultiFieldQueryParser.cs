﻿using Lucene.Net.Analysis;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using System;
using System.Collections.Generic;

namespace WebApp.Spa.Core.Services.DataGridFilter
{
	public class CustomMultiFieldQueryParser : MultiFieldQueryParser
	{
		private readonly string dateFormat;

		public CustomMultiFieldQueryParser(
			Lucene.Net.Util.Version matchVersion,
			string[] fields,
			Analyzer analyzer,
			string dateFormat)
			: base(matchVersion, fields, analyzer)
		{
			this.dateFormat = dateFormat;
		}

		protected override Query GetRangeQuery(string field, string part1, string part2, bool inclusive)
		{
			if (field == null)
			{
				var clauses = new List<BooleanClause>();
				for (int i = 0; i < this.fields.Length; i++)
				{
					clauses.Add(new BooleanClause(GetRangeQuery(this.fields[i], part1, part2, inclusive), Occur.SHOULD));
				}
				return GetBooleanQuery(clauses, true);
			}

			if (LowercaseExpandedTerms)
			{
				part1 = part1.ToLower();
				part2 = part2.ToLower();
			}

			if (DateTime.TryParseExact(part1, this.dateFormat, Locale, System.Globalization.DateTimeStyles.None, out var dateTime1)
				&& DateTime.TryParseExact(part2, this.dateFormat, Locale, System.Globalization.DateTimeStyles.None, out var dateTime2))
			{
				if (inclusive)
				{
					var calendar = Locale.Calendar;
					dateTime2 = calendar.AddHours(dateTime2, 23);
					dateTime2 = calendar.AddMinutes(dateTime2, 59);
					dateTime2 = calendar.AddSeconds(dateTime2, 59);
					dateTime2 = calendar.AddMilliseconds(dateTime2, 999);
				}

				part1 = dateTime1.ToString(this.dateFormat);
				part2 = dateTime2.ToString(this.dateFormat);
			}

			return NewRangeQuery(field, part1, part2, inclusive);
		}
	}
}
