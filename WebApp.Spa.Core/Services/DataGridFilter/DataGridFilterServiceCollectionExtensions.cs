﻿using Microsoft.Extensions.DependencyInjection;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Core.Services.DataGridFilter
{
	public static class DataGridFilterServiceCollectionExtensions
	{
		public static IServiceCollection AddDataGridFilter(this IServiceCollection services)
		{
			services.AddTransient<IDataGridFilter, DataGridFilter>()
					.AddTransient<IDataGridDisplayText, DataGridDisplayText>();

			return services;
		}
	}
}
