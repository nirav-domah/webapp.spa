﻿using Microsoft.Extensions.Options;
using System;
using System.Collections;
using System.Globalization;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.Configuration;

namespace WebApp.Spa.Core.Services.DataGridFilter
{
	public class DataGridDisplayText : IDataGridDisplayText
	{
		private readonly string dateFormat;
		private readonly string timeFormat;
		private readonly DataGridDisplayFormats dataGridDisplayFormats;

		public DataGridDisplayText(IOptionsSnapshot<Config> configOptions)
		{
			this.dateFormat = configOptions.Value.DateFormat;
			this.timeFormat = configOptions.Value.TimeFormat;
			this.dataGridDisplayFormats = configOptions.Value.DataGridDisplayFormats;
		}

		public string ToDisplayText(object value)
		{
			if (value == null)
				return string.Empty;

			if ((value is IEnumerable) && (!(value is string)))
				return this.dataGridDisplayFormats.ArrayFormat;

			if (value is bool boolean)
				return boolean.GetHashCode().ToString(this.dataGridDisplayFormats.BooleanFormat, null).Trim();

			if (IsNumeric(value) && !(value is string))
			{
				var numberFormatInfo = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
				numberFormatInfo.NumberGroupSeparator = this.dataGridDisplayFormats.NumberSeparator;
				numberFormatInfo.NumberDecimalSeparator = this.dataGridDisplayFormats.DecimalSeparator;

				string numericFormatString = $"###,###,###,##0.{new string('#', this.dataGridDisplayFormats.MaximumFractionDigits)}";

				return Convert.ToDecimal(value).ToString(numericFormatString, numberFormatInfo);
			}

			if (value is DateTime dateTime)
				return dateTime.ToString(this.dateFormat, CultureInfo.InvariantCulture);

			if (value is TimeSpan timeSpan)
				return timeSpan.ToString(this.timeFormat, CultureInfo.InvariantCulture);

			return value.ToString();
		}

		public bool IsNumeric(object value)
		{
			switch (Type.GetTypeCode(value.GetType()))
			{
				case TypeCode.Int16:
				case TypeCode.UInt16:
				case TypeCode.Int32:
				case TypeCode.UInt32:
				case TypeCode.Int64:
				case TypeCode.UInt64:
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
					return true;
			}

			return double.TryParse(value.ToString(), out _);
		}
	}
}
