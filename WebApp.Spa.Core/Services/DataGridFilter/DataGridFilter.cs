﻿using Lucene.Net.Analysis;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using WebApp.Spa.Core.Interfaces;
using WebApp.Spa.Core.Services.Configuration;

namespace WebApp.Spa.Core.Services.DataGridFilter
{
	public class DataGridFilter : IDataGridFilter
	{
		private readonly string dateFormat;
		private readonly DataGridDisplayFormats dataGridDisplayFormats;
		private readonly IDataGridDisplayText dataGridDisplayText;

		private Query searchQuery;
		private IDictionary<string, IEnumerable<string>> columnHeaderToNamesMap;
		private Func<int, string, object> getValueFunc;

		public DataGridFilter(IOptionsSnapshot<Config> configOptions, IDataGridDisplayText dataGridDisplayText)
		{
			this.dateFormat = configOptions.Value.DateFormat;
			this.dataGridDisplayFormats = configOptions.Value.DataGridDisplayFormats;
			this.dataGridDisplayText = dataGridDisplayText;
		}

		public void Initialise(string searchString, IDictionary<string, string> columnNameHeaderMap, Func<int, string, object> getValueFunc)
		{
			this.columnHeaderToNamesMap = columnNameHeaderMap
											.GroupBy(kvp => kvp.Value.ToLower())
											.ToDictionary(g => g.Key, g => g.Select(kvp => kvp.Key));

			this.getValueFunc = getValueFunc;

			string[] columnHeaders = this.columnHeaderToNamesMap.Keys
										.Select(h => h.ToLower())
										.ToArray();

			this.searchQuery = CreateSearchQuery(searchString, columnHeaders);
		}

		public bool MeetsFilterCriteria(int rowIndex)
		{
			if (this.searchQuery == null)
				return true;

			return Interpret(this.searchQuery, rowIndex);
		}

		private bool Interpret(Query query, int rowIndex)
		{
			switch (query)
			{
				case TermQuery q: return Apply(q, rowIndex);
				case BooleanQuery q: return Apply(q, rowIndex);
				case WildcardQuery q: return Apply(q, rowIndex);
				case PhraseQuery q: return Apply(q, rowIndex);
				case PrefixQuery q: return Apply(q, rowIndex);
				case TermRangeQuery q: return Apply(q, rowIndex);
				default: throw new Exception("Unhandled query type: " + query.GetType().FullName);
			}
		}

		/*
		 *	Example: <Value1>
		 */
		private bool Apply(TermQuery query, int rowIndex)
		{
			return Or(
				GetValues(rowIndex, query.Term.Field),
				v =>
				{
					if (v is DateTime dateTimeValue)
					{
						bool didParse = DateTime.TryParseExact(query.Term.Text, this.dateFormat, null, DateTimeStyles.None, out var searchValue);
						return didParse && searchValue.Date.Equals(dateTimeValue.Date);
					}

					return this.dataGridDisplayText.ToDisplayText(v).IndexOf(query.Term.Text, StringComparison.InvariantCultureIgnoreCase) != -1;
				});
		}

		/*
		 *	Example: 
		 *		<Value1> <Value2> ...
		 *		<Value1> AND <Value2> ...
		 *		<Value1> OR <Value2> ...
		 *		<Value1> AND <Value2> OR <Value3>...
		 */
		private bool Apply(BooleanQuery query, int rowIndex)
		{
			bool? finalResult = null;
			foreach (var booleanClause in query.Clauses)
			{
				bool result = Interpret(booleanClause.Query, rowIndex);
				if (booleanClause.Occur == Occur.MUST_NOT)
					result = !result;

				if (!finalResult.HasValue)
				{
					finalResult = result;
					continue;
				}

				finalResult = booleanClause.IsRequired
					? result && finalResult.Value
					: result || finalResult.Value;
			}

			return finalResult ?? false;
		}

		/*
		 *	Example: FirstName:N*cy
		 */
		private bool Apply(WildcardQuery query, int rowIndex)
		{
			string regexPattern = query.Term.Text
				.Replace("?", ".")
				.Replace("*", ".*")
				.Replace("^", "\\^")
				.Replace("$", "\\$");

			return Or(
				GetValues(rowIndex, query.Term.Field),
				v => Regex.IsMatch(v.ToString(), $"^{regexPattern}$", RegexOptions.IgnoreCase));
		}

		/*
		 *	Example: "Andrew Fuller"
		 */
		private bool Apply(PhraseQuery query, int rowIndex)
		{
			var terms = query.GetTerms();
			if (terms.Length == 0)
				return false;

			return Or(
				GetValues(rowIndex, terms[0].Field),
				v =>
				{
					string valueString = v.ToString();
					int nextStartIndex = 0;
					foreach (var term in terms)
					{
						nextStartIndex = valueString.IndexOf(term.Text, nextStartIndex, StringComparison.CurrentCultureIgnoreCase);
						if (nextStartIndex == -1)
							return false;
					}

					return true;
				});
		}

		/*
		 *	Example: FirstName:Nan*
		 */
		private bool Apply(PrefixQuery query, int rowIndex)
		{
			return Or(
				GetValues(rowIndex, query.Prefix.Field),
				v =>
				{
					string valueString = v.ToString();
					return !string.IsNullOrEmpty(valueString) && valueString.StartsWith(query.Prefix.Text, StringComparison.CurrentCultureIgnoreCase);
				});
		}

		/*
		 *	Example: Age:[18 TO 25]
		 */
		private bool Apply(TermRangeQuery query, int rowIndex)
		{
			return Or(
				GetValues(rowIndex, query.Field),
				v =>
				{
					if (!TryParseRange(v, query, out var lowerTerm, out var upperTerm))
						return false;

					if (this.dataGridDisplayText.IsNumeric(v))
					{
						if (!(v is decimal))
							v = Convert.ToDouble(v);
					}
					else if (!(v is DateTime))
					{
						v = v.ToString().ToLower();
					}

					bool lowerResult = query.IncludesLower ? lowerTerm.CompareTo(v) <= 0 : lowerTerm.CompareTo(v) < 0;
					bool upperResult = query.IncludesUpper ? upperTerm.CompareTo(v) >= 0 : upperTerm.CompareTo(v) > 0;

					return lowerResult && upperResult;
				});
		}

		private IEnumerable<object> GetValues(int rowIndex, string columnHeader)
		{
			return this.columnHeaderToNamesMap.TryGetValue(columnHeader.ToLower(), out var columnNames)
				? columnNames.Select(n =>
				{
					var result = this.getValueFunc(rowIndex, n);
					return result;
				})
				: Enumerable.Empty<object>();
		}

		private bool Or(IEnumerable<object> values, Func<object, bool> isMatch)
		{
			return values.Select(isMatch).Aggregate(seed: false, func: (a, b) => a || b);
		}

		private bool TryParseRange(object value, TermRangeQuery query, out dynamic lowerTerm, out dynamic upperTerm)
		{
			switch (value)
			{
				case DateTime _:
					return TryParseDateRange(query, out lowerTerm, out upperTerm);
				case decimal _:
					return TryParseDecimalRange(query, out lowerTerm, out upperTerm);
				case object o when this.dataGridDisplayText.IsNumeric(o):
					return TryParseDoubleRange(query, out lowerTerm, out upperTerm);
				case bool _:
					{
						lowerTerm = upperTerm = null;
						return false;
					}
				default:
					{
						lowerTerm = query.LowerTerm.ToLower();
						upperTerm = query.UpperTerm.ToLower();
						return true;
					}
			}
		}

		private bool TryParseDateRange(TermRangeQuery query, out dynamic lowerTerm, out dynamic upperTerm)
		{
			if (DateTime.TryParseExact(query.LowerTerm, this.dateFormat, null, DateTimeStyles.None, out var lowerDate)
				&& DateTime.TryParseExact(query.UpperTerm, this.dateFormat, null, DateTimeStyles.None, out var upperDate))
			{
				lowerTerm = lowerDate;
				upperTerm = upperDate;
				return true;
			}

			lowerTerm = upperTerm = null;
			return false;
		}

		private bool TryParseDecimalRange(TermRangeQuery query, out dynamic lowerTerm, out dynamic upperTerm)
		{
			if (decimal.TryParse(query.LowerTerm, out decimal lowerValue) && decimal.TryParse(query.UpperTerm, out decimal upperValue))
			{
				lowerTerm = lowerValue;
				upperTerm = upperValue;
				return true;
			}

			lowerTerm = upperTerm = null;
			return false;
		}

		private bool TryParseDoubleRange(TermRangeQuery query, out dynamic lowerTerm, out dynamic upperTerm)
		{
			if (double.TryParse(query.LowerTerm, out double lowerValue) && double.TryParse(query.UpperTerm, out double upperValue))
			{
				lowerTerm = lowerValue;
				upperTerm = upperValue;
				return true;
			}

			lowerTerm = upperTerm = null;
			return false;
		}

		private Query CreateSearchQuery(string searchString, string[] columnHeaders)
		{
			var queryParser = new CustomMultiFieldQueryParser(
				Lucene.Net.Util.Version.LUCENE_30,
				columnHeaders,
				new WhitespaceAnalyzer(),
				this.dateFormat
			)
			{
				AllowLeadingWildcard = true,
				DefaultOperator = QueryParser.Operator.AND
			};

			string processedSearchString = ParenthesizeNOTTerms(searchString);

			return queryParser.Parse(processedSearchString);
		}

		private string ParenthesizeNOTTerms(string searchString)
		{
			return Regex.Replace(
				searchString,
				@"NOT (\w+|\""[^\""]+\"")(?![^(]*\))",
				s => $"({s})");
		}
	}
}
