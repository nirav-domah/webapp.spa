﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using WebApp.Spa.Core.Enums;

namespace WebApp.Spa.Core.Services.Configuration
{
	public class Config
	{
		public const string ConfigSectionName = "Config";
		public const int DefaultSessionStateTimeout = 20;

		public string WebAppId { get; set; }

		public string WebAppName { get; set; }

		public string SpaDevHostUrl { get; set; }

		public string ApplicationManagerUrl { get; set; }

		[JsonConverter(typeof(StringEnumConverter))]
		public AuthenticationType AuthenticationType { get; set; }

		public OAuth OAuth { get; set; }

		public int SessionStateTimeout { get; set; }

		public string[] SessionVariableNames { get; set; }

		public bool Debug { get; set; }

		public bool Preview { get; set; }

		public string DateFormat { get; set; }

		public string TimeFormat { get; set; }

		public DataGridDisplayFormats DataGridDisplayFormats { get; set; }
	}
}
