﻿using System.Collections.Generic;

namespace WebApp.Spa.Core.Services.Configuration
{
	public class AppSettings
	{
		public object Logging { get; set; }

		public string AllowedHosts { get; set; }

		public IDictionary<string, string> ConnectionStrings { get; set; }

		public Config Config { get; set; }
	}
}
