﻿namespace WebApp.Spa.Core.Services.Configuration
{
	public class OAuth
	{
		public string Authority { get; set; }

		public string Audience { get; set; }

		public string ClientId { get; set; }

		public string ApiResourceName { get; set; }

		public string ApiResourceSecret { get; set; }

		public string Scopes { get; set; }
	}
}
