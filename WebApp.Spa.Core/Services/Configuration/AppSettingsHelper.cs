﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace WebApp.Spa.Core.Services.Configuration
{
	public class AppSettingsHelper
	{
		private readonly string appSettingsPath;

		private AppSettingsHelper()
		{
			this.appSettingsPath = Path.Combine(AppContext.BaseDirectory, "appsettings.json");
		}

		public static AppSettingsHelper GetInstance()
		{
			return new AppSettingsHelper();
		}

		public static void ExecuteAndSave(Action<AppSettings> action)
		{
			var appSettingsHelper = new AppSettingsHelper();
			var appSettings = appSettingsHelper.GetAppSettings();

			action(appSettings);

			appSettingsHelper.SaveAppSettings(appSettings);
		}

		public AppSettings GetAppSettings()
		{
			string appSettingsContent = File.ReadAllText(this.appSettingsPath);
			return JsonConvert.DeserializeObject<AppSettings>(appSettingsContent);
		}

		private void SaveAppSettings(AppSettings appSettings)
		{
			string newAppSettingsContent = JsonConvert.SerializeObject(appSettings, Formatting.Indented);
			File.WriteAllText(this.appSettingsPath, newAppSettingsContent);
		}
	}
}
