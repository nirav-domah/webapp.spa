﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;

namespace WebApp.Spa.Core.Services.Configuration
{
	public class WebConfigHelper
	{
		// language=regex
		private const string ValidCustomHeaderNamePattern = @"^[^\s@()=[\]{};:"",<>\\\/?]*$";

		private readonly string webConfigPath;
		private XDocument webConfig;

		private WebConfigHelper()
		{
			this.webConfigPath = Path.Combine(AppContext.BaseDirectory, "web.config");
		}

		private XDocument WebConfig
		{
			get
			{
				if (this.webConfig == null)
					this.webConfig = XDocument.Load(this.webConfigPath, LoadOptions.PreserveWhitespace);

				return this.webConfig;
			}
		}

		public static WebConfigHelper GetInstance()
		{
			return new WebConfigHelper();
		}

		public static void ExecuteAndSave(Action<WebConfigHelper> action)
		{
			var webConfigHelper = new WebConfigHelper();
			action(webConfigHelper);
			webConfigHelper.Save();
		}

		public bool FileExists()
		{
			return File.Exists(this.webConfigPath);
		}

		public uint GetMaxAllowedContentLength()
		{
			string maxAllowedContentLength = WebConfig.XPathSelectElement("/configuration/system.webServer/security/requestFiltering/requestLimits")
														.Attribute("maxAllowedContentLength")
														.Value;

			return uint.Parse(maxAllowedContentLength);
		}

		public void SetMaxAllowedContentLength(int maxAllowedContentLengthInKB)
		{
			ValidateMaxAllowedContentLength(maxAllowedContentLengthInKB);
			uint maxAllowedContentLengthInBytes = (uint)(maxAllowedContentLengthInKB * 1024L);

			WebConfig.XPathSelectElement("/configuration/system.webServer/security/requestFiltering/requestLimits")
						.SetAttributeValue("maxAllowedContentLength", maxAllowedContentLengthInBytes.ToString());
		}

		public IDictionary<string, string> GetCustomHeaders()
		{
			var customHeaders = new Dictionary<string, string>();

			var addElements = WebConfig.XPathSelectElements("/configuration/system.webServer/httpProtocol/customHeaders/add");

			foreach (var addElement in addElements)
			{
				customHeaders.Add(addElement.Attribute("name").Value, addElement.Attribute("value").Value);
			}

			return customHeaders;
		}

		public void AddCustomHeader(string name, string value)
		{
			ValidateCustomHeaderName(name);
			ValidateUniqueCustomHeader(name, GetCustomHeaders().Keys);

			var customHeadersElement = WebConfig.XPathSelectElement("/configuration/system.webServer/httpProtocol/customHeaders");

			customHeadersElement.Add(
				new XElement("add",
					new XAttribute("name", name),
					new XAttribute("value", value)
				)
			);
		}

		public void SetCustomHeader(string name, string newName, string newValue)
		{
			ValidateCustomHeaderName(newName);
			ValidateCustomHeaderExists(name, GetCustomHeaders().Keys);

			if (newName != name)
				ValidateUniqueCustomHeader(newName, GetCustomHeaders().Keys);

			var addElement = WebConfig.XPathSelectElements($"/configuration/system.webServer/httpProtocol/customHeaders/add")
										.Single(e => e.Attribute("name").Value == name);

			addElement.SetAttributeValue("name", newName);
			addElement.SetAttributeValue("value", newValue);
		}

		public void RemoveCustomHeader(string name)
		{
			ValidateCustomHeaderExists(name, GetCustomHeaders().Keys);

			var addElement = WebConfig.XPathSelectElements($"/configuration/system.webServer/httpProtocol/customHeaders/add")
										.SingleOrDefault(e => e.Attribute("name").Value == name);

			addElement?.Remove();
		}

		private void Save()
		{
			WebConfig.Save(this.webConfigPath);
		}

		private void ValidateCustomHeaderName(string name)
		{
			if (!Regex.IsMatch(name, ValidCustomHeaderNamePattern))
				throw new Exception("Name contains invalid character.");
		}

		private void ValidateUniqueCustomHeader(string name, IEnumerable<string> existingNames)
		{
			if (existingNames.Contains(name))
				throw new Exception("Duplicated header.");
		}

		private void ValidateCustomHeaderExists(string name, IEnumerable<string> existingNames)
		{
			if (!existingNames.Contains(name))
				throw new Exception("Header not found.");
		}

		private void ValidateMaxAllowedContentLength(int maxAllowedContentLengthInKB)
		{
			if (maxAllowedContentLengthInKB <= 0 || maxAllowedContentLengthInKB >= 4096 * 1024)
				throw new Exception("Invalid MaxAllowedContentLength value.");
		}
	}
}
