﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WebApp.Spa.Core.Services.Configuration
{
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class DataGridDisplayFormats
	{
		public string BooleanFormat { get; set; }

		public string ArrayFormat { get; set; }

		public string NumberSeparator { get; set; }

		public string DecimalSeparator { get; set; }

		public int MaximumFractionDigits { get; set; }
	}
}
