﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApp.Spa.Core.Extensions
{
	public static class ExceptionExtensions
	{
		public static string GetDisplayMessage(this Exception exception)
		{
			return exception.GetDisplayMessage($"{System.Environment.NewLine}{System.Environment.NewLine}Details: ");
		}

		public static string GetDisplayMessage(this Exception exception, string exceptionSeparator)
		{
			var unrolledExceptionMessages = exception.Unroll().Select(e => e.Message);

			bool removePeriods = !exceptionSeparator.StartsWith(System.Environment.NewLine);
			if (removePeriods)
			{
				string[] exceptionsArray = unrolledExceptionMessages.ToArray();
				unrolledExceptionMessages = exceptionsArray
					.Take(exceptionsArray.Length - 1).Select(e => e.TrimEnd('.'))
					.Concat(Enumerable.Repeat(exceptionsArray[exceptionsArray.Length - 1], 1));
			}

			return string.Join(exceptionSeparator, unrolledExceptionMessages);
		}

		public static IEnumerable<Exception> Unroll(this Exception exception)
		{
			while (exception != null)
			{
				if (exception is AggregateException aggregateException)
				{
					foreach (var innerException in aggregateException.Flatten().InnerExceptions)
						yield return innerException;
				}
				else
				{
					yield return exception;
				}

				exception = exception.InnerException;
			}
		}
	}
}
