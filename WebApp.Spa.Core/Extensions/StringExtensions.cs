﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Spa.Core.Extensions
{
	public static class StringExtensions
	{
		public static string Truncate(this string myString, int length)
		{
			return myString != null && myString.Length > length
					? myString.Substring(0, length) + "..."
					: myString;
		}
	}
}
