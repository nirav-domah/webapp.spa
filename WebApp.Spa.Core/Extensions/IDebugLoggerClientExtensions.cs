﻿using System;
using System.Threading.Tasks;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Core.Extensions
{
	public static class IDebugLoggerClientExtensions
	{
		public static async Task LogGroupInfo(this IDebugLoggerClient debugLoggerClient, string groupName, Func<Task> logInfo)
		{
			try
			{
				await debugLoggerClient.GroupCollapsed(groupName);
				await logInfo();
			}
			finally
			{
				await debugLoggerClient.GroupEnd();
			}
		}
	}
}
