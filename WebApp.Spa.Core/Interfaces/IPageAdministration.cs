﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IPageAdministration
	{
		Task<IEnumerable<PageInfo>> GetPagesAsync();

		Task EditPageAsync(string id, IEnumerable<string> roles);
	}
}
