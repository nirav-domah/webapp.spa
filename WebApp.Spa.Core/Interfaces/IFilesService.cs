﻿using Newtonsoft.Json.Linq;
using System.IO;
using WebApp.Spa.Core.Entities.FileTokens;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IFilesService
	{
		bool TryParseFileToken(JObject fileTokenObject, out AFileToken fileToken);

		bool TryParseFileToken(string fileTokenJson, out AFileToken fileToken);

		SessionFileToken CreateFileToken(Stream stream, string fileName = null);

		SessionFileToken CreateFileToken(byte[] bytes);

		Stream GetInputStream(AFileToken fileToken);

		bool Exists(AFileToken fileToken);

		string ReadText(AFileToken fileToken);

		byte[] ReadBytes(AFileToken fileToken);

		void Write(AFileToken fileToken, byte[] data);

		void Delete(AFileToken fileToken);

		void CheckFileExists(AFileToken fileToken);
	}
}
