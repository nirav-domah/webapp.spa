﻿using System;

namespace WebApp.Spa.Core.Interfaces
{
	public interface ISessionFilesJanitor : IDisposable
	{
		void RefreshTimeout(string sessionId);

		void ClearSession(string sessionId);
	}
}
