﻿using System.Collections.Generic;
using System.Data.Common;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IQueryResult
	{
		object[][] ResultSet { get; }

		IList<DbParameter> OutputParameters { get; }

		int NumberOfRowsAffected { get; }
	}
}
