﻿namespace WebApp.Spa.Core.Interfaces
{
	public interface IFolderPathService
	{
		string WebAppPath { get; }

		string WebRootPath { get; }

		string AppSettingsPath { get; }

		string WebConfigPath { get; }

		string UpdatesFolderPath { get; }

		string SessionsFolderPath { get; }

		string SessionIdFolderPath { get; }
	}
}
