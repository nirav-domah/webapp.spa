﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IAuthenticationSettingStore : IDisposable
	{
		IQueryable<AuthenticationSetting> AuthenticationSettings { get; }

		Task<AuthenticationSetting> FindByIdAsync(string authenticationSettingId, CancellationToken cancellationToken = default);

		Task<AuthenticationSetting> FindByNameAsync(string authenticationSettingName, CancellationToken cancellationToken = default);

		Task CreateAsync(AuthenticationSetting authenticationSetting, CancellationToken cancellationToken = default);

		Task UpdateAsync(AuthenticationSetting authenticationSetting, CancellationToken cancellationToken = default);

		Task UpdateAllAsync(IEnumerable<AuthenticationSetting> authenticationSettings, CancellationToken cancellationToken = default);

		Task RemoveRangeAsync(IEnumerable<AuthenticationSetting> authenticationSettings, CancellationToken cancellationToken = default);
	}
}
