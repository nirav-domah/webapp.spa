﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IUserAdministration
	{
		Task<bool> EmailExistsAsync(string email);

		Task<IEnumerable<UserInfo>> GetUsersAsync();

		Task<UserInfo> GetUserByIdAsync(string id);

		Task<UserInfo> GetUserByEmailAsync(string email);

		bool TryGetUserByEmail(string email, out UserInfo userInfo);

		bool TryGetUserByUniqueId(string uniqueId, out UserInfo userInfo);

		Task AddUserAsync(string email, string name, string password, bool isAdministrator, IEnumerable<string> roles, string uniqueId = null);

		Task EditUserAsync(string id, string email = null, string name = null, bool? isAdministrator = null, string password = null, IEnumerable<string> roles = null, string uniqueId = null);

		Task EditIsAdministrator(string id, bool isAdministrator);

		Task DeleteUserAsync(string id);
	}
}
