﻿using Newtonsoft.Json.Linq;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IValueResolver
	{
		T ResolveValue<T>(object value);

		T ResolveParameter<T>(JObject parametersContainer, string parameterName, bool resolveFileTokenValue = true);
	}
}
