﻿using System.Threading.Tasks;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IDebugLoggerClient
	{
		Task Log(string message);

		Task GroupCollapsed(string groupName);

		Task GroupEnd();

		Task Error(string message);
	}
}
