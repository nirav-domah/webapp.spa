﻿using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IDatabase
	{
		void EnableDebugLogger(IDebugLoggerClient debugLoggerClient);

		Task<IQueryResult> ExecuteQuery(
			DbConnection connection,
			string connectionString,
			string commandText,
			CommandType commandType,
			int? commandTimeout,
			DbParameter[] parameters);
	}
}
