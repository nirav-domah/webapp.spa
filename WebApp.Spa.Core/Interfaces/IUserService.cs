﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IUserService
	{
		UserClaims GetLoggedInUserClaims();

		Task<UserClaims> GetLoggedInUserClaimsAsync();

		Task<UserClaims> GetUserClaimsAsync(User user, IEnumerable<string> externalRoles = null);
	}
}
