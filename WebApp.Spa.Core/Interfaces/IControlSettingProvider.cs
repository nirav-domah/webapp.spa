﻿namespace WebApp.Spa.Core.Interfaces
{
	public interface IControlPropertyValueProvider
	{
		T GetPropertyValue<T>(string controlId, string propertyName);
	}
}
