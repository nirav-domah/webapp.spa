﻿namespace WebApp.Spa.Core.Interfaces
{
	public interface IDataGridDisplayText
	{
		string ToDisplayText(object value);

		bool IsNumeric(object value);
	}
}
