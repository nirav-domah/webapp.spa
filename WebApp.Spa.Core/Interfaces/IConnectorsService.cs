﻿namespace WebApp.Spa.Core.Interfaces
{
	public interface IConnectorsService
	{
		string GetConnectorName(string connectionId);

		string GetConnectionString(string connectionId);

		bool TryResolveConnectionString(string connectionString, out string resolvedConnectionString, out string errorMessage);
	}
}
