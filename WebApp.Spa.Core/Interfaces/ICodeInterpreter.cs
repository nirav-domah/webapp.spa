﻿namespace WebApp.Spa.Core.Interfaces
{
	public interface ICodeInterpreter
	{
		string Evaluate(string expression);
	}
}
