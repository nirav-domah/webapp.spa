﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Spa.Core.Entities;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IRoleAdministration
	{
		Task<IEnumerable<RoleInfo>> GetRolesAsync();

		Task AddRoleAsync(string name, IEnumerable<string> pages);

		Task EditRoleAsync(string id, string name, IEnumerable<string> pages);

		Task DeleteRoleAsync(string id);
	}
}
