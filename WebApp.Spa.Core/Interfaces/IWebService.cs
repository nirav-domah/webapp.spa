﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static WebApp.Spa.Core.Services.WebService.WebService;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IWebService
	{
		void EnableDebugLogger(IDebugLoggerClient debugLoggerClient);

		Task<object> Execute(
			string url,
			HttpMethod httpMethod,
			List<KeyValuePair<string, string>> queryStrings,
			List<KeyValuePair<string, string>> headers,
			List<MultipartParameter> multipartParameters,
			string bodyContentType,
			object body,
			List<KeyValuePair<string, string>> cookies,
			ResponseContentType responseContentType,
			TimeSpan timeout,
			Authentication authentication,
			string username,
			string password,
			string domain = null,
			string proxyHost = null,
			int proxyPort = 80,
			bool bypassProxyOnLocal = false);
	}
}
