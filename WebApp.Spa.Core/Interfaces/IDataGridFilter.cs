﻿using System;
using System.Collections.Generic;

namespace WebApp.Spa.Core.Interfaces
{
	public interface IDataGridFilter
	{
		void Initialise(string searchString, IDictionary<string, string> columnNameHeaderMap, Func<int, string, object> getValueFunc);

		bool MeetsFilterCriteria(int rowIndex);
	}
}
