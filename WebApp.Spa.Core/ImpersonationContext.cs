﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;

namespace WebApp.Spa.Core
{
	public class ImpersonationContext : IDisposable
	{
		private const int LOGON32_LOGON_INTERACTIVE = 2;
		private const int LOGON32_LOGON_NEW_CREDENTIALS = 9;
		private const int LOGON32_PROVIDER_DEFAULT = 0;

		private readonly SafeTokenHandle handle;
		private readonly WindowsImpersonationContext context;

		public ImpersonationContext(string domain, string username, string password, bool impersonateOverNetwork = false, string customErrorMessage = null)
		{
			int logonMode = impersonateOverNetwork ?
				LOGON32_LOGON_NEW_CREDENTIALS : LOGON32_LOGON_INTERACTIVE;

			bool ok = LogonUser(username, domain, password,
						   logonMode, LOGON32_PROVIDER_DEFAULT, out this.handle);

			if (!ok)
			{
				if (string.IsNullOrEmpty(customErrorMessage))
				{
					int errorCode = Marshal.GetLastWin32Error();
					throw new ApplicationException($"Could not impersonate user with Username={username} and Domain={domain}. LogonUser returned error code {errorCode}.");
				}
				else
				{
					throw new ApplicationException(customErrorMessage);
				}
			}

			this.context = WindowsIdentity.Impersonate(this.handle.DangerousGetHandle());
		}

		public void Dispose()
		{
			this.context.Dispose();
			this.handle.Dispose();
		}

		[DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
		private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);

		public sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
		{
			private SafeTokenHandle()
				: base(true) { }

			[DllImport("kernel32.dll")]
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			[SuppressUnmanagedCodeSecurity]
			[return: MarshalAs(UnmanagedType.Bool)]
			private static extern bool CloseHandle(IntPtr handle);

			protected override bool ReleaseHandle()
			{
				return CloseHandle(this.handle);
			}
		}
	}
}
