﻿using System.Collections.Generic;

namespace WebApp.Spa.Core.Entities
{
	public class RoleInfo
	{
		public Role Role { get; set; }

		public IEnumerable<string> Pages { get; set; }
	}
}
