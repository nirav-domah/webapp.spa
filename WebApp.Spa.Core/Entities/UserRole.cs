﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Spa.Core.Entities
{
	public class UserRole
	{
		[Required]
		public string UserId { get; set; }

		public User User { get; set; }

		[Required]
		public string RoleId { get; set; }

		public Role Role { get; set; }
	}
}
