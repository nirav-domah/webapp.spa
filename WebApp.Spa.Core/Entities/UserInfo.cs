﻿using System.Collections.Generic;

namespace WebApp.Spa.Core.Entities
{
	public class UserInfo
	{
		public User User { get; set; }

		public IEnumerable<string> Roles { get; set; }
	}
}
