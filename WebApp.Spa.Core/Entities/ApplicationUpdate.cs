﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Spa.Core.Entities
{
	public class ApplicationUpdate
	{
		public ApplicationUpdate(string userId, string username, string designerVersion)
		{
			UserId = userId;
			Username = username;
			DesignerVersion = designerVersion;
		}

		[Required]
		public string Id { get; private set; } = Guid.NewGuid().ToString();

		public string UserId { get; private set; }

		public string Username { get; private set; }

		[Required]
		public string DesignerVersion { get; private set; } = string.Empty;

		public DateTime DateTime { get; private set; } = DateTime.Now;
	}
}
