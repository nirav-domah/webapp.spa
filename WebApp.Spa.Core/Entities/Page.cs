﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Spa.Core.Entities
{
	public class Page
	{
		public Page(string id, string name, bool isStartPage)
		{
			Id = id;
			Name = name;
			IsStartPage = isStartPage;
		}

		[Required]
		public string Id { get; private set; }

		[Required]
		public string Name { get; set; }

		public bool IsStartPage { get; set; }

		public ICollection<AccessLookupEntry> AccessLookupEntries { get; } = new List<AccessLookupEntry>();

		public ICollection<PageRole> PageRoles { get; } = new List<PageRole>();
	}
}
