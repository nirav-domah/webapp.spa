﻿using Newtonsoft.Json;
using System;
using System.IO;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Core.Entities.FileTokens
{
	public class FileSystemConnectorToken : AFileToken
	{
		private IConnectorsService connectorsService;

		private string connectionString;
		private string connectorName;

		public FileSystemConnectorToken(string connectorID, string filePath)
		{
			if (string.IsNullOrEmpty(filePath))
				throw new Exception("A FileName was expected.");

			ConnectorID = connectorID;
			FilePath = filePath.TrimStart(Path.DirectorySeparatorChar);
		}

		public string ConnectorID { get; }

		public string FilePath { get; }

		public void InjectConnectorsService(IConnectorsService connectorsService)
		{
			this.connectorsService = connectorsService;
		}

		public override ImpersonationContext GetImpersonationContext()
		{
			CheckConnectorsService();

			string connectionString = GetCachedConnectionString();
			string connectorName = GetCachedConnectorName();

			dynamic connection = JsonConvert.DeserializeObject(connectionString);
			string user = connection?.User;

			if (string.IsNullOrEmpty(user))
				return null;

			string domain = user.Substring(0, user.LastIndexOf("\\"));
			string userName = user.Substring(user.LastIndexOf("\\") + 1);

			string targetDomain = domain == "." ? System.Environment.MachineName : domain;
			bool isTargetUserLoggedIn = targetDomain.Equals(System.Environment.UserDomainName, StringComparison.InvariantCultureIgnoreCase)
										&& userName.Equals(System.Environment.UserName, StringComparison.InvariantCultureIgnoreCase);

			if (isTargetUserLoggedIn)
				return null;

			string password = connection.Password;
			bool isNetworkPath = connection.Path.ToString().StartsWith("\\\\");
			string customErrorMessage = $"Access Denied: FileSystem [{connectorName}] could not connect due to invalid credentials.";

			return new ImpersonationContext(domain, userName, password, isNetworkPath, customErrorMessage);
		}

		public override string GetFullPath()
		{
			CheckConnectorsService();

			string connectionString = GetCachedConnectionString();
			string connectorName = GetCachedConnectorName();

			dynamic connection = JsonConvert.DeserializeObject(connectionString);
			string rootPath = connection?.Path;

			using (GetImpersonationContext())
			{
				if (!Directory.Exists(rootPath))
					throw new Exception($"Could not find a part of the path '[{connectorName}]\\{FilePath}'");
			}

			string fullPath = Path.GetFullPath(Path.Combine(rootPath, FilePath));

			if (!fullPath.StartsWith(rootPath))
				throw new Exception($"The path is inaccessible as it is not inside of [{connectorName}].");

			return fullPath;
		}

		private void CheckConnectorsService()
		{
			if (this.connectorsService == null)
				throw new Exception("The ConnectorsService was not registered.");
		}

		private string GetCachedConnectionString()
		{
			if (string.IsNullOrEmpty(this.connectionString))
				this.connectionString = this.connectorsService.GetConnectionString(ConnectorID);

			return this.connectionString;
		}

		private string GetCachedConnectorName()
		{
			if (string.IsNullOrEmpty(this.connectorName))
				this.connectorName = this.connectorsService.GetConnectorName(ConnectorID);

			return this.connectorName;
		}
	}
}
