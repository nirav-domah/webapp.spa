﻿using Newtonsoft.Json;

namespace WebApp.Spa.Core.Entities.FileTokens
{
	public abstract class AFileToken
	{
		[JsonProperty("$type")]
		private string SerializationType => GetType().AssemblyQualifiedName;

		public virtual ImpersonationContext GetImpersonationContext() => null;

		public abstract string GetFullPath();
	}
}
