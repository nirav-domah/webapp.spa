﻿using System.IO;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Core.Entities.FileTokens
{
	public class SessionFileToken : AFileToken
	{
		private IFolderPathService folderPathService;

		public SessionFileToken(string fileID, string fileName)
		{
			FileID = fileID;
			FileName = fileName;
		}

		public string FileID { get; }

		public string FileName { get; }

		public void InjectFolderPathService(IFolderPathService folderPathService)
		{
			this.folderPathService = folderPathService;
		}

		public override string GetFullPath()
		{
			return Path.Combine(this.folderPathService.SessionIdFolderPath, FileID);
		}
	}
}
