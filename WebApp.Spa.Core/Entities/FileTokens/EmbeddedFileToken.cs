﻿using System.IO;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Core.Entities.FileTokens
{
	public class EmbeddedFileToken : AFileToken
	{
		private IFolderPathService folderPathService;

		public EmbeddedFileToken(string relativePath)
		{
			RelativePath = relativePath;
		}

		public string RelativePath { get; }

		public void InjectFolderPathService(IFolderPathService folderPathService)
		{
			this.folderPathService = folderPathService;
		}

		public override string GetFullPath()
		{
			return Path.Combine(this.folderPathService.WebRootPath, RelativePath);
		}
	}
}
