﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Spa.Core.Entities
{
	public class Application
	{
		public Application(string fileGuid, string name)
		{
			FileGuid = fileGuid;
			Name = name;
		}

		[Key]
		[Required]
		public string FileGuid { get; private set; }

		[Required]
		public string Name { get; set; }

		public string WebApiKey { get; set; }
	}
}
