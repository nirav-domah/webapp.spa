﻿using System.Collections.Generic;

namespace WebApp.Spa.Core.Entities
{
	public class PageInfo
	{
		public Page Page { get; set; }

		public IEnumerable<string> Roles { get; set; }
	}
}
