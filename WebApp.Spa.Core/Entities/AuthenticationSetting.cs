﻿using Newtonsoft.Json;
using System;

namespace WebApp.Spa.Core.Entities
{
	public class AuthenticationSetting : ASettingBase
	{
		[JsonIgnore]
		public const string OidcProvider = "OidcProvider";
		[JsonIgnore]
		public const string AuthenticationAuthorityUri = "AuthenticationAuthorityUri";
		[JsonIgnore]
		public const string RedirectUrl = "RedirectUrl";
		[JsonIgnore]
		public const string LogoutRedirectUrl = "LogoutRedirectUrl";
		[JsonIgnore]
		public const string ClientID = "ClientID";
		[JsonIgnore]
		public const string ApiResourceName = "ApiResourceName";
		[JsonIgnore]
		public const string ApiResourceSecret = "ApiResourceSecret";
		[JsonIgnore]
		public const string RoleClaimName = "RoleClaimName";
		[JsonIgnore]
		public const string Tenant = "Tenant";
		[JsonIgnore]
		public const string Audience = "Audience";
		[JsonIgnore]
		public const string Scopes = "Scopes";

		public AuthenticationSetting(string name, string value)
			: base(Guid.NewGuid().ToString(), name, value)
		{
			Value = value;
		}

		public string Value { get; set; } = string.Empty;
	}
}
