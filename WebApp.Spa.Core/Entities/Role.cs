﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Spa.Core.Entities
{
	public class Role
	{
		public static readonly string DefaultRoleGuid = Guid.Empty.ToString();
		public static readonly string DefaultRoleName = "User";

		public Role(string name)
		{
			Name = name;
		}

		public Role(string id, string name)
			: this(name)
		{
			Id = id;
		}

		[Required]
		public string Id { get; private set; } = Guid.NewGuid().ToString();

		[Required]
		public string Name { get; set; }

		public string NormalizedName { get; set; }

		public ICollection<UserRole> UserRoles { get; } = new List<UserRole>();

		public ICollection<PageRole> PageRoles { get; } = new List<PageRole>();

		[NotMapped]
		public bool IsDefault => Id == DefaultRoleGuid;
	}
}
