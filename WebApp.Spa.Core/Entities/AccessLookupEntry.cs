﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Spa.Core.Entities
{
	public class AccessLookupEntry
	{
		public AccessLookupEntry(string id, string description)
		{
			Id = id;
			Description = description;
		}

		[Required]
		public string Id { get; private set; }

		public string Description { get; set; }

		[Required]
		public Page Page { get; set; }
	}
}
