﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Spa.Core.Entities
{
	public class User
	{
		public User(string userName)
		{
			UserName = userName;
		}

		[Required]
		public string Id { get; private set; } = Guid.NewGuid().ToString();

		public string UserName { get; set; }

		public string Name { get; set; }

		public string Email { get; set; }

		public string PasswordHash { get; set; }

		public string SecurityStamp { get; set; } = Guid.NewGuid().ToString();

		public bool IsAdministrator { get; set; }

		public string NormalizedUserName { get; set; }

		public string NormalizedEmail { get; set; }

		public ICollection<UserRole> UserRoles { get; } = new List<UserRole>();

		public string UniqueId { get; set; }
	}
}
