﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Spa.Core.Entities
{
	public class PageRole
	{
		[Required]
		public string PageId { get; set; }

		public Page Page { get; set; }

		[Required]
		public string RoleId { get; set; }

		public Role Role { get; set; }
	}
}
