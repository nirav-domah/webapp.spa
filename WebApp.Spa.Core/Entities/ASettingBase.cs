﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Spa.Core.Entities
{
	public abstract class ASettingBase
	{
		public ASettingBase(string id, string name, string defaultValue)
		{
			Id = id;
			Name = name;
			DefaultValue = defaultValue;
		}

		[Required]
		public string Id { get; private set; }

		[Required]
		public string Name { get; set; }

		public string DefaultValue { get; set; } = string.Empty;
	}
}
