﻿namespace WebApp.Spa.Core.Entities
{
	public class Setting : ASettingBase
	{
		public Setting(string id, string name, string value)
			: base(id, name, value)
		{
			Value = value;
		}

		public string Value { get; set; } = string.Empty;
	}
}
