﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WebApp.Spa.Core.Entities
{
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class UserClaims
	{
		public string Id { get; set; }

		public string Email { get; set; }

		public string Username { get; set; }

		public string Name { get; set; }

		public bool IsAdministrator { get; set; }

		public string[] AccessiblePages { get; set; }

		public string[] Roles { get; set; }
	}
}
