﻿namespace WebApp.Spa.Core.Entities
{
	public class Connection : ASettingBase
	{
		public Connection(string id, string name, string connectionString)
			: base(id, name, connectionString)
		{
			ConnectionString = connectionString;
		}

		public string ConnectionString { get; set; } = string.Empty;
	}
}
