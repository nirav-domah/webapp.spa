﻿using Microsoft.Win32;
using System.Runtime.CompilerServices;

namespace WebApp.Spa.Core
{
	public class RegistryKeys
	{
		private const string ServerRegistryKeyName = @"SOFTWARE\Twenty57\Stadium Server";
		private static RegistryKeys instance;

		private RegistryKeys()
		{ }

		public static RegistryKeys Instance => instance ?? (instance = new RegistryKeys());

		public string ApiKey
		{
			get => GetValue();
			set => SetValue(value);
		}

		public string InstallDir => GetValue();

		public string Port => GetValue();

		private string GetValue([CallerMemberName] string name = null)
		{
			using (var localMachineKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
			using (var key = localMachineKey.OpenSubKey(ServerRegistryKeyName))
			{
				return key?.GetValue(name)?.ToString();
			}
		}

		private void SetValue(string value, [CallerMemberName] string name = null)
		{
			using (var localMachineKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
			using (var key = localMachineKey.CreateSubKey(ServerRegistryKeyName, writable: true))
			{
				key.SetValue(name, value, RegistryValueKind.String);
			}
		}
	}
}
