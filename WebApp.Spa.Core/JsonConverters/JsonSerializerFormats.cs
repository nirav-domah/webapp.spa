﻿namespace WebApp.Spa.Core.JsonConverters
{
	public static class JsonSerializerFormats
	{
		public const string DateTime = @"yyyy-MM-dd\THH:mm:ss";

		public const string TimeSpan = @"hh\:mm\:ss\.fff";
	}
}
