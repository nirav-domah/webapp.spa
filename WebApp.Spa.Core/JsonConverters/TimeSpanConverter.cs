﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace WebApp.Spa.Core.JsonConverters
{
	public class TimeSpanConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(TimeSpan);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.Value == null)
				return null;

			string stringValue = reader.Value.ToString();
			if (TimeSpan.TryParseExact(
				stringValue,
				JsonSerializerFormats.TimeSpan,
				CultureInfo.InvariantCulture,
				out var parsedTimeSpan))
			{
				return parsedTimeSpan;
			}

			return null;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var timeSpan = (TimeSpan)value;
			writer.WriteValue(timeSpan.ToString(JsonSerializerFormats.TimeSpan, CultureInfo.InvariantCulture));
		}
	}
}
