﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace WebApp.Spa.Core.JsonConverters
{
	public class DateTimeConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DateTime);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.Value == null)
				return null;

			string stringValue = reader.Value.ToString();
			if (DateTime.TryParseExact(
				stringValue,
				JsonSerializerFormats.DateTime,
				CultureInfo.InvariantCulture,
				DateTimeStyles.None,
				out var parsedDateTime))
			{
				return parsedDateTime;
			}

			return null;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var dateTime = (DateTime)value;
			writer.WriteValue(dateTime.ToString(JsonSerializerFormats.DateTime, CultureInfo.InvariantCulture));
		}
	}
}
