﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using WebApp.Spa.Core.Interfaces;

namespace WebApp.Spa.Core.JsonConverters
{
	public class ByteArrayConverter : JsonConverter
	{
		private const int FileTokenThreshold = 100;

		private readonly IFilesService filesService;

		public ByteArrayConverter(IFilesService filesService)
		{
			this.filesService = filesService;
		}

		public override bool CanRead => true;

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(byte[]);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null)
				return null;

			var token = JToken.Load(reader);
			if (token == null)
				return null;

			if (token.Type == JTokenType.Array)
			{
				var bytes = new List<byte>();
				foreach (var child in token.Children())
					bytes.Add(child.ToObject<byte>());

				return bytes.ToArray();
			}

			throw new JsonReaderException("Unknown byte array format.");
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			byte[] bytes = (byte[])value;

			if (bytes.Length > FileTokenThreshold)
			{
				var fileToken = this.filesService.CreateFileToken(bytes);
				serializer.Serialize(writer, fileToken);
			}
			else
			{
				writer.WriteStartArray();
				foreach (byte @byte in bytes)
					writer.WriteValue(@byte);
				writer.WriteEndArray();
			}
		}
	}
}
