﻿namespace WebApp.Spa.Core.Enums
{
	public enum AuthenticationType
	{
		Anonymous,
		Cookie,
		Windows,
		OAuth
	}
}
