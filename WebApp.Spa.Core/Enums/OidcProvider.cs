﻿namespace WebApp.Spa.Core.Enums
{
	public enum OidcProvider
	{
		Auth0,
		AzureAD,
		Google,
		Okta,
		GenericProvider
	}
}
