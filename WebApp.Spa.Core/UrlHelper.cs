﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;

namespace WebApp.Spa.Core
{
	public static class UrlHelper
	{
		public static string Combine(params string[] parts)
		{
			return string.Join("/", parts
				.Select(part => part?.Trim().Trim('/'))
				.Where(part => !string.IsNullOrWhiteSpace(part)));
		}

		public static string AddQueryString(string url, IDictionary<string, string> queryString)
		{
			bool hasQueryParameter = url.IndexOf('?') != -1;
			var stringBuilder = new StringBuilder();
			stringBuilder.Append(url);

			foreach (var parameter in queryString)
			{
				stringBuilder.Append(hasQueryParameter ? '&' : '?');
				stringBuilder.Append(UrlEncoder.Default.Encode(parameter.Key));
				stringBuilder.Append('=');
				stringBuilder.Append(UrlEncoder.Default.Encode(parameter.Value));

				hasQueryParameter = true;
			}

			return stringBuilder.ToString();
		}

		public static bool StartsWithScheme(string url)
		{
			return UriSchemes.Any(s => url.StartsWith(s + "://", StringComparison.InvariantCultureIgnoreCase));
		}

		public static string EnsureHttpProtocol(string url, bool secure = false)
		{
			return !url.StartsWith("http://") && !url.StartsWith("https://")
					? secure ? "https://" + url : "http://" + url
					: url;
		}

		public static bool IsValidUrl(string baseUrl, out string invalidUrlMessage)
		{
			invalidUrlMessage = null;

			if (!Uri.TryCreate(baseUrl, UriKind.Absolute, out var uri))
			{
				baseUrl = EnsureHttpProtocol(baseUrl);

				if (!Uri.TryCreate(baseUrl, UriKind.Absolute, out uri))
				{
					invalidUrlMessage = "Invalid Base URL.";
					return false;
				}
			}
			else if (uri.Scheme != Uri.UriSchemeHttp && uri.Scheme != Uri.UriSchemeHttps)
			{
				invalidUrlMessage = "Invalid Base URL. Only http:// or https:// schemes are allowed.";
				return false;
			}

			var hostType = Uri.CheckHostName(uri.Host);

			bool hasValidDomain = Regex.Match(uri.Host, @"\.[a-zA-Z]{2,}$").Success
				|| uri.Host == "localhost"
				|| hostType == UriHostNameType.IPv4
				|| hostType == UriHostNameType.IPv6;

			if (hostType == UriHostNameType.Unknown || !hasValidDomain)
			{
				invalidUrlMessage = "Invalid Base URL host.";
				return false;
			}

			return true;
		}

		private static readonly string[] UriSchemes = new string[]
		{
			Uri.UriSchemeFile,
			Uri.UriSchemeFtp,
			Uri.UriSchemeGopher,
			Uri.UriSchemeHttp,
			Uri.UriSchemeHttps,
			Uri.UriSchemeMailto,
			Uri.UriSchemeNetPipe,
			Uri.UriSchemeNetTcp,
			Uri.UriSchemeNews,
			Uri.UriSchemeNntp
		};
	}
}
